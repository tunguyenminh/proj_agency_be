import {
  Controller,
  Get,
  Headers,
  Param,
  Query,
  UseGuards,
} from '@nestjs/common';
import { AppService } from './app.service';
import { BaseException, Errors } from './constants/error.constant';
import {
  getProvinces,
  getDistrictsByProvince,
  getWardsByDistrict,
  getWards,
  getDistricts,
} from './utils/location.utils';
import subVn from 'sub-vi';
import { PrismaService } from 'prisma/prisma.service';
import phoneCode from 'country-codes-list';
import { I18n, I18nContext } from 'nestjs-i18n';
import { I18nCustomService } from './i18n/i18n.service';
import { ApiBearerAuth } from '@nestjs/swagger';
import {
  AccountBalanceTransactionStatus,
  AccountBalanceTransactionType,
  AdvertisingAccountType,
  BMShareRequestStatus,
  MoneyTransactionLogStatus,
  MoneyTransactionLogType,
  UserRole,
} from '@prisma/client';
import { Roles } from './auth/decorators/roles.decorator';
import { JwtAuthGuard } from './auth/guards/jwt-auth.guard';
import { RolesGuard } from './auth/guards/roles.guard';
import { User } from './auth/decorators/user.decorator';
import { IUserJwt } from './auth/strategies/jwt.strategy';
import { getDates } from './utils/common.utils';
import moment from 'moment';
import { DashboardFilter } from './base/filterOption.dto';
import { WithdrawTarget } from './constants/enum.constant';
@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
    private readonly prismaService: PrismaService,
    private readonly i18n: I18nCustomService,
  ) { }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @Get('/dashboard')
  async dashBoard(@Query() options: DashboardFilter, @User() user: IUserJwt) {
    let where = {};
    if (user.role == UserRole.CUSTOMER) {
      where = {
        userId: user.data.id,
        // advertisingAccount: {
        //   userId: user.data.id,
        // },
      };
    }

    if ((user.role === UserRole.ADMIN || user.role === UserRole.EMPLOYEE) && options.userId) {
      where = {
        userId: options.userId
      };
    }
    const moneyTopupToAdsAccount = await this.prismaService.accountBalanceTransaction.count({
      where: {
        ...where,
        type: AccountBalanceTransactionType.DEPOSIT,
        status: AccountBalanceTransactionStatus.PENDING,
      }
    })
    const moneyRefundFromAdsToWallet = await this.prismaService.accountBalanceTransaction.count({
      where: {
        ...where,
        type: AccountBalanceTransactionType.WITHDRAWAL,
        status: AccountBalanceTransactionStatus.PENDING,
        target: WithdrawTarget.WALLET
      }
    })
    const moneyWithdrawalTransactionPending = await this.prismaService.accountBalanceTransaction.count({
      where: {
        ...where,
        type: AccountBalanceTransactionType.WITHDRAWAL,
        status: AccountBalanceTransactionStatus.PENDING,
        target: WithdrawTarget.BANK
      }
    })
    // const moneyWithdrawalTransactionPending =
    //   await this.prismaService.moneyTransactionLog.count({
    //     where: {
    //       ...where,
    //       type: AccountBalanceTransactionType.WITHDRAWAL,
    //       status: AccountBalanceTransactionStatus.PENDING,
    //     },
    //   });
    const moneyDepositTransactionPending =
      await this.prismaService.accountBalanceTransaction.count({
        where: {
          ...where,
          type: AccountBalanceTransactionType.WALLET_DEPOSIT,
          status: AccountBalanceTransactionStatus.PENDING,
        },
      });
    const accountFBActive = await this.prismaService.advertisingAccount.count({
      where: { ...where, type: AdvertisingAccountType.FACEBOOK },
    });
    const accountGoogleActive =
      await this.prismaService.advertisingAccount.count({
        where: { ...where, type: AdvertisingAccountType.GOOGLE },
      });
    const accountTiktokActive =
      await this.prismaService.advertisingAccount.count({
        where: { ...where, type: AdvertisingAccountType.TIKTOK },
      });
    const totalAccountActive =
      await this.prismaService.advertisingAccount.count({
        where: { ...where },
      });
    const totalWaitingForBMShare =
      await this.prismaService.bMShareRequest.count({
        where: {
          advertisingAccount: {
            userId:
              user.role == UserRole.CUSTOMER ? user.data.id : options.userId,
          },
          status: BMShareRequestStatus.PENDING,
        },
      });

    const sevenDaysAgo = new Date();
    sevenDaysAgo.setDate(sevenDaysAgo.getDate() - 7);
    const currentDate = new Date();

    let deposit: Array<any>
    let withdrawal: Array<any>
    if ((user.role == UserRole.ADMIN || user.role == UserRole.EMPLOYEE)) {
      if (options.userId) {
        deposit = await this.prismaService.$queryRaw`
    SELECT DATE(created_At) as day, SUM(amount) as totalAmount
    FROM account_balance_transaction
    WHERE type = ${AccountBalanceTransactionType.DEPOSIT
          } AND created_at >= ${sevenDaysAgo.toISOString()} AND created_at <= ${currentDate.toISOString()} AND
      user_id = ${options.userId}
    GROUP BY day
    ORDER BY day DESC
  `;

        withdrawal = await this.prismaService.$queryRaw`
      SELECT DATE(created_at) AS day, SUM(amount) as totalAmount
      FROM account_balance_transaction
      WHERE type = ${MoneyTransactionLogType.WITHDRAWAL
          } AND created_at >= ${sevenDaysAgo.toISOString()} AND created_at <= ${currentDate.toISOString()} AND
      user_id = ${user.data.id}
      GROUP BY day
      ORDER BY day DESC
      `;
      }
      else {
        deposit = await this.prismaService.$queryRaw`
        SELECT DATE(created_At) as day, SUM(amount) as totalAmount
        FROM account_balance_transaction
        WHERE type = ${AccountBalanceTransactionType.DEPOSIT
          } AND created_at >= ${sevenDaysAgo.toISOString()} AND created_at <= ${currentDate.toISOString()}
        GROUP BY day
        ORDER BY day DESC
      `;

        withdrawal = await this.prismaService.$queryRaw`
          SELECT DATE(created_at) AS day, SUM(amount) as totalAmount
          FROM account_balance_transaction
          WHERE type = ${AccountBalanceTransactionType.WITHDRAWAL
          } AND created_at >= ${sevenDaysAgo.toISOString()} AND created_at <= ${currentDate.toISOString()} 
          GROUP BY day
          ORDER BY day DESC
          `;
      }
    }

    if (user.role == UserRole.CUSTOMER) {
      deposit = await this.prismaService.$queryRaw`
      SELECT DATE(created_At) as day, SUM(amount) as totalAmount
      FROM account_balance_transaction
      WHERE type = ${AccountBalanceTransactionType.DEPOSIT
        } AND created_at >= ${sevenDaysAgo.toISOString()} AND created_at <= ${currentDate.toISOString()} AND
        user_id = ${user.data.id}
      GROUP BY day
      ORDER BY day DESC
    `;

      withdrawal = await this.prismaService.$queryRaw`
        SELECT DATE(created_at) AS day, SUM(amount) as totalAmount
        FROM account_balance_transaction
        WHERE type = ${AccountBalanceTransactionType.WITHDRAWAL
        } AND created_at >= ${sevenDaysAgo.toISOString()} AND created_at <= ${currentDate.toISOString()} AND
        user_id = ${options.userId}
        GROUP BY day
        ORDER BY day DESC
        `;
    }

    const dateArray = getDates(sevenDaysAgo, currentDate);

    const _withdrawal = dateArray.map((date) => {
      const filteredItem = withdrawal.find(
        (item) =>
          new Date(item.day).toDateString() === new Date(date).toDateString(),
      );
      if (filteredItem) {
        return { date: new Date(date), totalAmount: filteredItem.totalAmount };
      } else {
        return { date: new Date(date), totalAmount: 0 };
      }
    });

    const _deposit = dateArray.map((date) => {
      const filteredItem = deposit.find(
        (item) =>
          new Date(item.day).toDateString() === new Date(date).toDateString(),
      );
      if (filteredItem) {
        return { date: new Date(date), totalAmount: filteredItem.totalAmount };
      } else {
        return { date: new Date(date), totalAmount: 0 };
      }
    });

    return {
      chart: {
        deposit: _deposit,
        withdrawal: _withdrawal,
      },
      moneyWithdrawalTransactionPending: moneyRefundFromAdsToWallet,
      moneyDepositTransactionPending: moneyTopupToAdsAccount,
      totalWaitingForChanging: moneyDepositTransactionPending,
      totalWaitingForRefund: moneyWithdrawalTransactionPending,
      accountFBActive,
      accountGoogleActive,
      accountTiktokActive,
      totalAccountActive,
      totalWaitingForBMShare
    };
  }

  @Get()
  async getHello(@Headers() headers: any) {
    // const test = await i18n.t('translation.welcome.abc')
    const test = await this.i18n.t('common-message.test-fall-back');
    throw new BaseException(
      Errors.BAD_REQUEST(await this.i18n.t('common-message.test-fall-back')),
    );
    return { message: test };
  }

  @Get('province-codes')
  async getProvinceCodes() {
    try {
      return getProvinces();
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  @Get(':provinceCode/district-by-province')
  getDistrictsByProvince(@Param('provinceCode') param: string) {
    try {
      return getDistrictsByProvince(param);
    } catch (error) {
      return error;
    }
  }

  @Get(':districtCode/wards-by-district')
  getWardsByDistrict(@Param('districtCode') param: string) {
    try {
      return getWardsByDistrict(param);
    } catch (error) {
      return error;
    }
  }

  @Get('get-phone-code')
  getPhoneCode() {
    try {
      const phoneCodes = phoneCode.all();
      return phoneCodes.map((x) => {
        return {
          countryNameEn: x.countryNameEn,
          countryCode: x.countryCode,
          countryCallingCode: x.countryCallingCode,
          flag: x.flag,
        };
      });
    } catch (error) {
      return error;
    }
  }
}
