import { Injectable } from '@nestjs/common';
import { AccountBalanceTransactionStatus, AdvertisingAccountType, AgencyServiceType, Prisma } from '@prisma/client';
import axios from 'axios';
import { PrismaService } from 'prisma/prisma.service';
import { BaseException, Errors } from 'src/constants/error.constant';
import { uniOpenApi } from 'src/constants/openApi/Uni.constant';
import { AgencyBalanceService } from '../agency-balance/agency-balance.service';

@Injectable()
export class AccountBalanceTransactionService {
  constructor(
    private readonly prismaService: PrismaService,
  ) { }

  async create(args: Prisma.AccountBalanceTransactionCreateArgs) {
    return this.prismaService.accountBalanceTransaction.create(args)
  }

  findAll(args: Prisma.AccountBalanceTransactionFindManyArgs) {
    return this.prismaService.accountBalanceTransaction.findMany(args)
  }

  findOne(args: Prisma.AccountBalanceTransactionFindFirstArgs) {
    return this.prismaService.accountBalanceTransaction.findFirst(args)
  }

  // C1
  async update(id: number, args: Prisma.AccountBalanceTransactionUncheckedUpdateInput) {
    return this.prismaService.accountBalanceTransaction.update({ where: { id }, data: args })
  }

  count(args: Prisma.AccountBalanceTransactionCountArgs) {
    return this.prismaService.accountBalanceTransaction.count(args)
  }
}
