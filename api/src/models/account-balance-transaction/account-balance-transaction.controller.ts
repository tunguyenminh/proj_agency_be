import { Body, Controller, Get, Param, Patch, Post, Query, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import {
  AccountBalanceTransactionStatus,
  AccountBalanceTransactionType,
  AdvertisingAccountStatus,
  AdvertisingAccountType,
  AgencyBalanceLogsType,
  AgencyServiceType,
  Prisma,
  UserRole,
  UserStatus,
} from '@prisma/client';
import moment from 'moment';
import { Roles } from 'src/auth/decorators/roles.decorator';
import { User as UserDecorator } from 'src/auth/decorators/user.decorator';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { RolesGuard } from 'src/auth/guards/roles.guard';
import { IUserJwt } from 'src/auth/strategies/jwt.strategy';
import { CONFIGURATION_TYPE, NotificationTitle, NotificationType, NotificationUserTitle, UNICode, WalletFlowStatus } from 'src/constants/common.constant';
import { Language, SortOrder, WithdrawTarget } from 'src/constants/enum.constant';
import { BaseException, Errors } from 'src/constants/error.constant';
import { NodeMailerService } from 'src/core/node-mailer/node-mailer.service';
import { funcListPaging } from 'src/helpers/list-paging';
import { HKService, IAdHKDepoistSubmit } from 'src/services/hongkong/hongkong.service';
import { IRechargeApplication, UniService } from 'src/services/uni/uni.service';
import { exportToExcel, tranformAdsType } from 'src/utils/common.utils';
import { AdvertisingAccountService } from '../advertising_account/advertising_account.service';
import { AgencyBalanceLogsService } from '../agency-balance-logs/agency-balance-logs.service';
import { AgencyBalanceService } from '../agency-balance/agency-balance.service';
import { ConfigurationService } from '../configuration/configuration.service';
import { NotificationLogsService } from '../notification-log/notification-log.service';
import { UserService } from '../user/user.service';
import { AccountBalanceTransactionService } from './account-balance-transaction.service';
import { FilterAccountBalanceTransactionDto } from './dto/account-balance-transaction.dto';
import { CalculatorWalletRechargeDto, CreateAccountBalanceDto, CreateWalletRechargeDto } from './dto/create-account-balance-transaction.dto';
import { AccountBalanceTransactionChangeStatus, UpdateAccountBalanceTransactionDto } from './dto/update-account-balance-transaction.dto';

@ApiTags('Account Balance')
@Controller('account-balance')
export class AccountBalanceTransactionController {
  constructor(
    private readonly accountBalanceTransactionService: AccountBalanceTransactionService,
    private readonly advertisingAccountService: AdvertisingAccountService,
    private readonly userService: UserService,
    private readonly notificationLogsService: NotificationLogsService,
    private readonly nodeMailer: NodeMailerService,
    private readonly agencyBalanceService: AgencyBalanceService,
    private readonly agencyBalanceLogsService: AgencyBalanceLogsService,
    private readonly configurationService: ConfigurationService,
    private readonly uniService: UniService,
    private readonly hkService: HKService,
  ) {}

  async autoCallAgencyLv1(
    amount: number,
    idAccount: string,
    type: AdvertisingAccountType,
    agencyService: AgencyServiceType,
    updatedById: number,
    isAuto: boolean = false,
  ) {
    /**
     * @description
     * Tích hợp với hệ thống C1 để nạp tiền vào tài khoản quảng cáo
     * ___________________________ START ___________________________
     */
    const createArgs: { updatedById: number; status: AccountBalanceTransactionStatus; error: any; traceId?: string } = {
      updatedById,
      status: AccountBalanceTransactionStatus.PENDING,
      error: '',
      traceId: null,
    };

    const agencyWallet = await this.agencyBalanceService.findOne({ where: { type: agencyService } });

    if (agencyWallet.balance >= amount) {
      switch (agencyService) {
        case AgencyServiceType.UNI:
          // check số dư tài khoản C1 của nhà cup cấp có đủ tiền không (Nếu có API hỗ trợ)
          // nạp tiền từ tài khoản C1
          const dataUni: IRechargeApplication = {
            taskNumber: new Date().toISOString(),
            mediaAccountId: idAccount,
            mediaPlatform: tranformAdsType(type),
            amount: amount.toString(),
            dailyBudget: 1,
          };
          const response = await this.uniService.rechargeApplication(dataUni);
          console.log('Body UNI: ', dataUni);
          console.log('Response UNI: ', response.data);
          if (response.data.code === UNICode.SUCCESS) {
            createArgs.updatedById = 1;
            createArgs.status = AccountBalanceTransactionStatus.PENDING_APPROVAL;
            createArgs.traceId = response.data.data.taskId;
          } else {
            createArgs.error = response.data.message;
          }
          break;
        case AgencyServiceType.HONGKONG:
          const platform = tranformAdsType(type);
          // currently only support for FB
          if (platform === 1) {
            const bodyHK: IAdHKDepoistSubmit = {
              args: [
                {
                  list: [
                    {
                      faceAdId: idAccount,
                      money: amount,
                    },
                  ],
                  platform,
                },
              ],
            };
            const responsehk = await this.hkService.adDepositSubmit(bodyHK);
            console.log('Body HONGKONG: ', JSON.stringify(bodyHK));
            console.log('Response HONGKONG: ', responsehk.data);
            if (!responsehk.data.code) {
              createArgs.updatedById = 1;
              createArgs.status = AccountBalanceTransactionStatus.PENDING_APPROVAL;
              createArgs.traceId = responsehk.data.data?.[0]?.applyId || null;
            } else {
              createArgs.error = responsehk.data.message;
            }
          } else {
            createArgs.status = AccountBalanceTransactionStatus.PENDING;
          }
          break;
        case AgencyServiceType.VN:
        default:
          createArgs.updatedById = 1;
          createArgs.status = isAuto ? AccountBalanceTransactionStatus.PENDING : AccountBalanceTransactionStatus.PENDING_APPROVAL;
          createArgs.error = '';
          break;
      }
    } else {
      createArgs.error = `Wallet ${agencyWallet.type} has insufficient balance`;
    }
    return createArgs;
    /**
     * @description
     * Tích hợp với hệ thống C1 để nạp tiền vào tài khoản quảng cáo
     * ___________________________ END ___________________________
     */
  }

  @ApiBearerAuth()
  @Roles(UserRole.ADMIN, UserRole.CUSTOMER, UserRole.EMPLOYEE)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Get()
  async findAll(@UserDecorator() user: IUserJwt, @Query() options: FilterAccountBalanceTransactionDto) {
    let whereInput: Prisma.AccountBalanceTransactionFindManyArgs;

    const sortField = options?.sortField || 'createdAt';
    const sortOrder = options?.sortOrder || SortOrder.DESC;
    let where: Prisma.AccountBalanceTransactionWhereInput = {};
    where.AND = [] as Prisma.AccountBalanceTransactionWhereInput[];

    // if (user.role === UserRole.CUSTOMER) {
    //   where = {
    //     ...where,
    //     advertisingAccount: {
    //       userId: user.data.id,
    //     },
    //   };
    // }

    if (user.role === UserRole.CUSTOMER) {
      where.AND.push({
        userId: user.data.id,
      });
    }

    if (options.textSearch) {
      const OR: Prisma.AccountBalanceTransactionWhereInput[] = [];
      OR.push({ advertisingAccount: { title: { contains: options.textSearch } } });

      if (!options.walletFlow && !options.walletFlowStatus) {
        OR.push({ user: { email: { contains: options.textSearch } } });
      }
      where.AND.push({ OR });
    }

    if (options.accountType) {
      where = {
        ...where,
        // @ts-ignore
        advertisingAccount: {
          ...where?.advertisingAccount,
          type: {
            in: options.accountType,
          },
        },
      };
    }

    if (options.from) {
      where = {
        ...where,
        createdAt: {
          gte: moment(options.from).startOf('day').toDate(),
          lte: moment(options.to).endOf('day').toDate(),
        },
      };
    }

    if (options.status) {
      where = {
        ...where,
        status: { in: options.status },
      };
    }

    if (options.type) {
      where = {
        ...where,
        type: options.type,
      };
    }

    if (options.walletFlow && !options.walletFlowStatus) {
      where = {
        ...where,
        OR: [
          { type: AccountBalanceTransactionType.DEPOSIT, advertisingAccount: { type: AdvertisingAccountType.GOOGLE } },
          {
            type: AccountBalanceTransactionType.WITHDRAWAL,
            status: AccountBalanceTransactionStatus.ADDED,
            target: WithdrawTarget.WALLET,
            advertisingAccount: { type: AdvertisingAccountType.GOOGLE },
          },
          { type: AccountBalanceTransactionType.DEPOSIT, advertisingAccount: { type: AdvertisingAccountType.FACEBOOK } },
          {
            type: AccountBalanceTransactionType.WITHDRAWAL,
            status: AccountBalanceTransactionStatus.ADDED,
            target: WithdrawTarget.WALLET,
            advertisingAccount: { type: AdvertisingAccountType.FACEBOOK },
          },
          { type: AccountBalanceTransactionType.WALLET_DEPOSIT, status: AccountBalanceTransactionStatus.ADDED },
          { type: AccountBalanceTransactionType.ACCOUNT_REQUEST, AdvertisingAccountRequest: { type: AdvertisingAccountType.GOOGLE } },
          { type: AccountBalanceTransactionType.ACCOUNT_REQUEST, AdvertisingAccountRequest: { type: AdvertisingAccountType.FACEBOOK } },
          { type: AccountBalanceTransactionType.DENINED_DEPOSIT, advertisingAccount: { type: AdvertisingAccountType.FACEBOOK } },
          { type: AccountBalanceTransactionType.DENINED_WITHDRAWAL, advertisingAccount: { type: AdvertisingAccountType.FACEBOOK } },
          { type: AccountBalanceTransactionType.DENINED_ACCOUNT_REQUEST, AdvertisingAccountRequest: { type: AdvertisingAccountType.FACEBOOK } },
          { type: AccountBalanceTransactionType.DENINED_DEPOSIT, advertisingAccount: { type: AdvertisingAccountType.GOOGLE } },
          { type: AccountBalanceTransactionType.DENINED_WITHDRAWAL, advertisingAccount: { type: AdvertisingAccountType.GOOGLE } },
          { type: AccountBalanceTransactionType.DENINED_ACCOUNT_REQUEST, AdvertisingAccountRequest: { type: AdvertisingAccountType.GOOGLE } },
        ],
      };
    }

    if (options.walletFlowStatus) {
      // where.AND.OR = []
      const filterFlow = options.walletFlowStatus.map((WFS: WalletFlowStatus) => {
        if (WFS === WalletFlowStatus.WALLET_DEPOSIT) {
          return { type: WFS, status: AccountBalanceTransactionStatus.ADDED };
        }
        const [adsAccountType, transactionType] = WFS.split('-') as [AdvertisingAccountType, AccountBalanceTransactionType];
        const conditional = { type: transactionType };
        switch (transactionType) {
          case AccountBalanceTransactionType.WITHDRAWAL:
            return {
              ...conditional,
              status: AccountBalanceTransactionStatus.ADDED,
              target: WithdrawTarget.WALLET,
              advertisingAccount: { type: adsAccountType },
            };
          case WalletFlowStatus.ACCOUNT_REQUEST:
          case WalletFlowStatus.DENINED_ACCOUNT_REQUEST:
            return { ...conditional, AdvertisingAccountRequest: { type: adsAccountType } };
          default:
            return { ...conditional, advertisingAccount: { type: adsAccountType } };
        }
      });
      console.log('filterFlow', JSON.stringify(filterFlow));

      // @ts-ignore
      where.AND.push({ OR: filterFlow });
    }

    whereInput = {
      where,
      orderBy: {
        [sortField]: sortOrder,
      },
      include: {
        advertisingAccount: true,
        AdvertisingAccountRequest: true,
        user: true,
        updatedBy: true,
      },
    };

    return await funcListPaging(this.accountBalanceTransactionService, whereInput, options?.page, options?.perPage);
  }

  @ApiBearerAuth()
  @Roles(UserRole.ADMIN, UserRole.CUSTOMER, UserRole.EMPLOYEE)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Get('export-excel')
  async get(@UserDecorator() user: IUserJwt, @Query() options: FilterAccountBalanceTransactionDto) {
    let whereInput: Prisma.AccountBalanceTransactionFindManyArgs;

    const sortField = options?.sortField || 'createdAt';
    const sortOrder = options?.sortOrder || SortOrder.DESC;
    let where: Prisma.AccountBalanceTransactionWhereInput = {};
    where.AND = [];

    if (user?.role === UserRole.CUSTOMER) {
      where = {
        ...where,
        userId: user.data.id,
      };
    }

    if (options.textSearch) {
      // @ts-ignore
      where.AND.push({
        OR: [
          {
            advertisingAccount: {
              user: { lastName: { contains: options.textSearch } },
            },
          },
          {
            advertisingAccount: {
              user: { firstName: { contains: options.textSearch } },
            },
          },
          {
            advertisingAccount: {
              user: { email: { contains: options.textSearch } },
            },
          },
        ],
      });
    }

    if (options.accountType) {
      where = {
        ...where,
        // @ts-ignore
        advertisingAccount: {
          ...where?.advertisingAccount,
          type: {
            in: options.accountType,
          },
        },
      };
    }

    if (options.from) {
      where = {
        ...where,
        createdAt: {
          gte: moment(options.from).startOf('day').toDate(),
          lte: moment(options.to).endOf('day').toDate(),
        },
      };
    }

    if (options.status) {
      where = {
        ...where,
        status: { in: options.status },
      };
    }

    if (options.type) {
      where = {
        ...where,
        type: options.type,
      };
    }

    if (options.walletFlow && !options.walletFlowStatus) {
      where = {
        ...where,
        OR: [
          { type: AccountBalanceTransactionType.DEPOSIT, advertisingAccount: { type: AdvertisingAccountType.GOOGLE } },
          {
            type: AccountBalanceTransactionType.WITHDRAWAL,
            status: AccountBalanceTransactionStatus.ADDED,
            target: WithdrawTarget.WALLET,
            advertisingAccount: { type: AdvertisingAccountType.GOOGLE },
          },
          { type: AccountBalanceTransactionType.DEPOSIT, advertisingAccount: { type: AdvertisingAccountType.FACEBOOK } },
          {
            type: AccountBalanceTransactionType.WITHDRAWAL,
            status: AccountBalanceTransactionStatus.ADDED,
            target: WithdrawTarget.WALLET,
            advertisingAccount: { type: AdvertisingAccountType.FACEBOOK },
          },
          { type: AccountBalanceTransactionType.WALLET_DEPOSIT, status: AccountBalanceTransactionStatus.ADDED },
          { type: AccountBalanceTransactionType.ACCOUNT_REQUEST, AdvertisingAccountRequest: { type: AdvertisingAccountType.GOOGLE } },
          { type: AccountBalanceTransactionType.ACCOUNT_REQUEST, AdvertisingAccountRequest: { type: AdvertisingAccountType.FACEBOOK } },
          { type: AccountBalanceTransactionType.DENINED_DEPOSIT, advertisingAccount: { type: AdvertisingAccountType.FACEBOOK } },
          { type: AccountBalanceTransactionType.DENINED_WITHDRAWAL, advertisingAccount: { type: AdvertisingAccountType.FACEBOOK } },
          { type: AccountBalanceTransactionType.DENINED_ACCOUNT_REQUEST, AdvertisingAccountRequest: { type: AdvertisingAccountType.FACEBOOK } },
          { type: AccountBalanceTransactionType.DENINED_DEPOSIT, advertisingAccount: { type: AdvertisingAccountType.GOOGLE } },
          { type: AccountBalanceTransactionType.DENINED_WITHDRAWAL, advertisingAccount: { type: AdvertisingAccountType.GOOGLE } },
          { type: AccountBalanceTransactionType.DENINED_ACCOUNT_REQUEST, AdvertisingAccountRequest: { type: AdvertisingAccountType.GOOGLE } },
        ],
      };
    }

    if (options.walletFlowStatus) {
      // where.AND.OR = []
      const filterFlow = options.walletFlowStatus.map((WFS: WalletFlowStatus) => {
        if (WFS === WalletFlowStatus.WALLET_DEPOSIT) {
          return { type: WFS, status: AccountBalanceTransactionStatus.ADDED };
        }
        const [adsAccountType, transactionType] = WFS.split('-') as [AdvertisingAccountType, AccountBalanceTransactionType];
        const conditional = { type: transactionType };
        switch (transactionType) {
          case AccountBalanceTransactionType.WITHDRAWAL:
            return {
              ...conditional,
              status: AccountBalanceTransactionStatus.ADDED,
              target: WithdrawTarget.WALLET,
              advertisingAccount: { type: adsAccountType },
            };
          case WalletFlowStatus.ACCOUNT_REQUEST:
          case WalletFlowStatus.DENINED_ACCOUNT_REQUEST:
            return { ...conditional, AdvertisingAccountRequest: { type: adsAccountType } };
          default:
            return { ...conditional, advertisingAccount: { type: adsAccountType } };
        }
      });
      // @ts-ignore
      where.AND.push({ OR: filterFlow });
    }

    console.log({ where });

    whereInput = {
      where,
      orderBy: {
        [sortField]: sortOrder,
      },
      include: {
        advertisingAccount: {
          include: { user: true },
        },
        AdvertisingAccountRequest: true,
      },
    };

    const data = await this.accountBalanceTransactionService.findAll(whereInput);

    let _data = [];
    let headers = ['Loại', 'ID Case', 'Thời gian tạo', 'Số Tiền'];
    switch (options.type) {
      case AccountBalanceTransactionType.DEPOSIT:
        headers = ['Id tài khoản', 'Tên tài khoản', 'Tên khách hàng', 'Số Tiền', 'Tổng chi phí', 'Loại tài khoản', 'Thời gian tạo', 'Trạng thái'];
        if (options.lang === Language.EN) {
          headers = ['Account ID', 'Account Name', 'Customer Name', 'Amount', 'Total Cost', 'Account Type', 'Creation Time', 'Status'];
        }
        _data = data.map((x: any) => {
          return {
            advertisingAccountId: x.advertisingAccountId,
            title: x.advertisingAccount.title,
            email: x.advertisingAccount.user.email,
            amount: x.amount,
            totalCost: x.totalCost,
            type: x.advertisingAccount.type,
            createdAt: x.createdAt,
            status: x.status,
          };
        });
        break;
      case AccountBalanceTransactionType.WITHDRAWAL:
        headers = ['ID Case', 'Tên tài khoản', 'ID tài khoản', 'Số tiền nhập', 'Số tiền nhận', 'Loại tài khoản', 'Phương thức', 'Thời gian tạo', 'Trạng thái'];
        if (options.lang === Language.EN) {
          headers = ['ID Case', 'Account Name', 'Account ID', 'Input Amount', 'Received Amount', 'Account Type', 'Method', 'Creation Time', 'Status'];
        }
        _data = data.map((x: any) => {
          return {
            idCase: x.id,
            adsAccountName: x.advertisingAccount.title,
            adsIdAccountName: x.advertisingAccount.idAccount,
            amount: x.amount,
            actualyAmount: x.refundReceived,
            type: x.advertisingAccount.type,
            refundMethod: x.target,
            createdAt: x.createdAt,
            status: x.advertisingAccount.status,
          };
        });
        break;
      default:
        headers = ['Loại', 'ID Case', 'Thời gian tạo', 'Số Tiền'];
        if (options.lang === Language.EN) {
          headers = ['Type', 'ID Case', 'Creation Time', 'Amount'];
        }
        _data = data.map((x) => {
          let type = x.type;
          if (x?.advertisingAccountId) {
            // @ts-ignore
            type = `${x.advertisingAccount.type}_${x.type}`;
          }
          if (x?.advertisingAccountRequestId) {
            // @ts-ignore
            type = `${x.AdvertisingAccountRequest.type}_${x.type}`;
          }

          const calculatorAmount = () => {
            switch (x.type) {
              case AccountBalanceTransactionType.WITHDRAWAL:
                return `+${x.refundReceived}`;
              case AccountBalanceTransactionType.WALLET_DEPOSIT:
                return `+${x.amount}`;
              case AccountBalanceTransactionType.DENINED_ACCOUNT_REQUEST:
              case AccountBalanceTransactionType.DENINED_DEPOSIT:
                return `+${x.totalCost}`;
              case AccountBalanceTransactionType.DEPOSIT:
              case AccountBalanceTransactionType.ACCOUNT_REQUEST:
              default:
                return `-${x.totalCost}`;
            }
          };

          const typeToRowType = (type: string) => {
            // Replace all instances of "WITHDRAWAL" with "REFUND"
            let updatedString = type.replace(/WITHDRAWAL/g, 'REFUND');
            // Replace all instances of "DEPOSIT" with "TOP UP"
            updatedString = updatedString.replace(/DEPOSIT/g, 'TOP-UP');
            return updatedString;
          };

          let amount = calculatorAmount();
          if (Math.abs(Number(amount)) === 0) {
            amount = '0';
          }
          // @ts-ignore
          const idCase = x.advertisingAccountRequestId ? x.AdvertisingAccountRequest.id : x.id;
          return { type: typeToRowType(type), idCase, createdAt: x.createdAt, amount };
        });
        break;
    }
    let name = '';
    switch (options.type) {
      case AccountBalanceTransactionType.DEPOSIT:
        name = 'topup';
        break;
      case AccountBalanceTransactionType.WALLET_DEPOSIT:
        name = 'walletRecharge';
        break;
      case AccountBalanceTransactionType.WITHDRAWAL:
        name = 'refunded';
        break;
      default:
        name = 'walletFlow';
        break;
    }

    const fileName = await exportToExcel(_data, headers, name);

    return {
      uri: `excels/${fileName}`,
    };
  }

  @ApiBearerAuth()
  @Roles(UserRole.ADMIN, UserRole.CUSTOMER, UserRole.EMPLOYEE)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Get(':id')
  async findOne(@Param('id') id: number) {
    return this.accountBalanceTransactionService.findOne({
      where: { id },
      include: { advertisingAccount: true },
    });
  }

  @ApiBearerAuth()
  @Roles(UserRole.ADMIN, UserRole.CUSTOMER)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Patch(':id')
  async update(@Param('id') id: number, @UserDecorator() user: IUserJwt, @Body() body: UpdateAccountBalanceTransactionDto) {
    let updateArgs: Prisma.AccountBalanceTransactionUncheckedUpdateInput = {
      ...body,
      updatedById: user.data.id,
    };

    return this.accountBalanceTransactionService.update(id, updateArgs);
  }

  @ApiBearerAuth()
  @Roles(UserRole.CUSTOMER)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Post('/wallet-recharge')
  async createWalletRecharge(@UserDecorator() user: IUserJwt, @Body() body: CreateWalletRechargeDto) {
    const userFound = await this.userService.findOne({
      where: { id: user.data.id, status: { not: UserStatus.DELETED } },
    });
    if (!userFound) throw new BaseException(Errors.BAD_REQUEST('User not found'));
    if (userFound.status === UserStatus.BANNED) throw new BaseException(Errors.BAD_REQUEST('User has been banned'));

    const config = await this.configurationService.findOne({
      where: { settingCode: body.method },
    });
    if (!config) throw new BaseException(Errors.BAD_REQUEST('Method not found'));

    // TODO Calculate fee
    let TOTAL_AMOUNT = body.amount + (body.amount * config.settingFee) / 100;
    if (config.type === CONFIGURATION_TYPE.RATIO) {
      TOTAL_AMOUNT = body.amount * config.settingFee;
    }

    const createArgs: Prisma.AccountBalanceTransactionUncheckedCreateInput = {
      ...body,
      userId: user.data.id,
      methodDetail: config.settingValue,
      totalPaid: TOTAL_AMOUNT,
      type: AccountBalanceTransactionType.WALLET_DEPOSIT,
      target: WithdrawTarget.WALLET,
    };
    const accountBalanceTransactionLog = await this.accountBalanceTransactionService.create({
      data: createArgs,
    });
    await this.notificationLogsService.create({
      data: {
        title: NotificationTitle.MONEY_TRANSACTION,
        accountBalanceTransactionId: accountBalanceTransactionLog.id,
        type: NotificationType.MONEY_TRANSACTION,
      },
    });
    return accountBalanceTransactionLog;
  }

  @ApiBearerAuth()
  @Roles(UserRole.CUSTOMER)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Post('/calulate-wallet-recharge')
  async calulateTotalPaid(@UserDecorator() user: IUserJwt, @Body() body: CalculatorWalletRechargeDto) {
    const userFound = await this.userService.findOne({
      where: { id: user.data.id, status: { not: UserStatus.DELETED } },
    });
    if (!userFound) {
      throw new BaseException(Errors.BAD_REQUEST('User not found'));
    }
    if (userFound.status === UserStatus.BANNED) {
      throw new BaseException(Errors.BAD_REQUEST('User has been banned'));
    }
    const config = await this.configurationService.findOne({
      where: { settingCode: body.method },
    });

    if (!config) {
      throw new BaseException(Errors.BAD_REQUEST('Method not found'));
    }

    // TODO Calculate fee
    let TOTAL_AMOUNT = body.amount + (body.amount * config.settingFee) / 100;
    if (config.type === CONFIGURATION_TYPE.RATIO) {
      TOTAL_AMOUNT = body.amount * config.settingFee;
    }

    return { totalPaid: TOTAL_AMOUNT };
  }

  // Create Account Balance Deposit
  @ApiBearerAuth()
  @Roles(UserRole.CUSTOMER, UserRole.EMPLOYEE)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Post('account-balance-topup')
  async createAccountBalanceTopup(@UserDecorator() user: IUserJwt, @Body() body: Array<CreateAccountBalanceDto>) {
    const userFound = await this.userService.findOne({
      where: { id: user.data.id, status: { not: UserStatus.DELETED } },
    });
    if (!userFound) throw new BaseException(Errors.BAD_REQUEST('User not found'));
    if (userFound.status === UserStatus.BANNED) throw new BaseException(Errors.BAD_REQUEST('User has been banned'));

    const accountBalanceTransaction = [];
    for (let i = 0; i < body.length; i++) {
      const element = body[i];
      const advertisingAccountId = element.advertisingAccountId;
      const totalCost = element.amount + (element.amount * (userFound.depositAdsFee || 0)) / 100;

      let createArgs: Prisma.AccountBalanceTransactionUncheckedCreateInput = {
        ...element,
        advertisingAccountId,
        userId: userFound.id,
        target: element?.target || '',
        imageWithdraw: element?.imageWithdraw || '',
        totalCost,
        type: AccountBalanceTransactionType.DEPOSIT,
      };

      const advertisingAccount = await this.advertisingAccountService.findOne({
        where: { id: advertisingAccountId, status: { not: AdvertisingAccountStatus.DELETED } },
      });
      if (!advertisingAccount) throw new BaseException(Errors.BAD_REQUEST('Account is not found'));

      switch (advertisingAccount.status) {
        case AdvertisingAccountStatus.DEACTIVE:
        case AdvertisingAccountStatus.PENDING:
          throw new BaseException(Errors.BAD_REQUEST('Account is not active'));
        case AdvertisingAccountStatus.REFUNDED:
          throw new BaseException(Errors.BAD_REQUEST('Account is refunded'));
        default:
          break;
      }
      // DEPOSIT
      // if (element.type == AccountBalanceTransactionType.DEPOSIT) {
      if (userFound.balance < totalCost) throw new BaseException(Errors.BAD_REQUEST(`Your balance is not enough`));

      await this.userService.update(userFound.id, { balance: { decrement: totalCost } });
      // update on Agency Lv1
      /**
       * @description
       * Tích hợp với hệ thống C1 để nạp tiền vào tài khoản quảng cáo
       * ___________________________ START ___________________________
       */
      // Lấy số dư ví tại database local (Do C1 chưa hỗ trợ lấy số dư ví)
      const agencyWallet = await this.agencyBalanceService.findOne({ where: { type: advertisingAccount.agencyService } });
      console.log(agencyWallet.balance);

      if (agencyWallet.balance >= element.amount) {
        // check loại uni hay hk. tạm để uni
        const { updatedById, status, traceId } = await this.autoCallAgencyLv1(
          element.amount,
          advertisingAccount.idAccount,
          advertisingAccount.type,
          advertisingAccount.agencyService,
          1,
          true,
        );
        if (status === AccountBalanceTransactionStatus.PENDING_APPROVAL) {
          createArgs.updatedById = updatedById;
          createArgs.status = AccountBalanceTransactionStatus.PENDING_APPROVAL;
          createArgs.traceLv1Id = traceId;
        }
        /**
         * @description
         * Tích hợp với hệ thống C1 để nạp tiền vào tài khoản quảng cáo
         * ___________________________ END ___________________________
         */
      }
      if (createArgs.status === AccountBalanceTransactionStatus.ADDED) {
        await this.agencyBalanceLogsService.create({
          data: {
            amount: element.amount,
            createdById: user.data.id,
            advertisingAccountId,
            type: AgencyBalanceLogsType.DECREASE,
            agencyWalletBalanceBefore: agencyWallet.balance,
            agencyWalletBalanceAfter: agencyWallet.balance - element.amount,
          },
        });
        await this.agencyBalanceService.update(agencyWallet.id, { balance: { decrement: element.amount } });
      }
      const accountBalanceTransactionLog = await this.accountBalanceTransactionService.create({
        data: createArgs,
      });

      await this.notificationLogsService.create({
        data: {
          title: NotificationTitle.ACCOUNT_BALANCE,
          type: NotificationType.ACCOUNT_BALANCE,
          accountBalanceTransactionId: accountBalanceTransactionLog.id,
        },
      });
      accountBalanceTransaction.push(accountBalanceTransactionLog);
      // }
    }
    return accountBalanceTransaction;
  }

  // Create Account Balance Withdraw
  @ApiBearerAuth()
  @Roles(UserRole.CUSTOMER, UserRole.EMPLOYEE)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Post('account-balance-withdraw')
  async createAccountBalanceWithdraw(@UserDecorator() user: IUserJwt, @Body() body: Array<CreateAccountBalanceDto>) {
    const userFound = await this.userService.findOne({
      where: { id: user.data.id, status: { not: UserStatus.DELETED } },
    });
    if (!userFound) throw new BaseException(Errors.BAD_REQUEST('User not found'));
    if (userFound.status === UserStatus.BANNED) throw new BaseException(Errors.BAD_REQUEST('User has been banned'));

    const accountBalanceTransaction = [];
    for (let i = 0; i < body.length; i++) {
      const element = body[i];
      const advertisingAccountId = element.advertisingAccountId;

      let createArgs: Prisma.AccountBalanceTransactionUncheckedCreateInput = {
        ...element,
        advertisingAccountId,
        userId: userFound.id,
        target: element?.target || '',
        imageWithdraw: element?.imageWithdraw || '',
        refundReceived: 0,
        type: AccountBalanceTransactionType.WITHDRAWAL,
      };

      const advertisingAccount = await this.advertisingAccountService.findOne({
        where: { id: advertisingAccountId, status: { not: AdvertisingAccountStatus.DELETED } },
      });
      if (!advertisingAccount) throw new BaseException(Errors.BAD_REQUEST('Account not found'));

      switch (advertisingAccount.status) {
        case AdvertisingAccountStatus.DEACTIVE:
        case AdvertisingAccountStatus.PENDING:
          throw new BaseException(Errors.BAD_REQUEST('Account is not active'));
        case AdvertisingAccountStatus.REFUNDED:
          throw new BaseException(Errors.BAD_REQUEST('Account is refunded'));
        default:
          break;
      }

      // WITHDRAWAL
      // if (advertisingAccount.balance < element.amount) throw new BaseException(Errors.BAD_REQUEST(`${advertisingAccount.title} balance is not enough`));
      await this.advertisingAccountService.update(advertisingAccountId, { balance: { decrement: element.amount } });

      const accountBalanceTransactionLog = await this.accountBalanceTransactionService.create({ data: createArgs });
      await this.notificationLogsService.create({
        data: {
          title: NotificationTitle.ACCOUNT_BALANCE,
          type: NotificationType.ACCOUNT_BALANCE,
          accountBalanceTransactionId: accountBalanceTransactionLog.id,
        },
      });
      accountBalanceTransaction.push(accountBalanceTransactionLog);
    }
    return accountBalanceTransaction;
  }

  @ApiBearerAuth()
  @Roles(UserRole.ADMIN, UserRole.EMPLOYEE)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Patch(':id/wallet-recharge-status')
  async updateWalletRechargeStatus(@Param('id') id: number, @UserDecorator() user: IUserJwt, @Body() body: AccountBalanceTransactionChangeStatus) {
    const accountBalanceTransaction = await this.accountBalanceTransactionService.findOne({
      where: { id, status: AccountBalanceTransactionStatus.PENDING },
      include: { advertisingAccount: true },
    });
    if (!accountBalanceTransaction) throw new BaseException(Errors.BAD_REQUEST('Request not found'));

    const userFound = await this.userService.findOne({
      where: { id: accountBalanceTransaction.userId },
    });
    if (!userFound) throw new BaseException(Errors.BAD_REQUEST('User not found'));

    const argsUpdateAccBalanceTrans: Prisma.AccountBalanceTransactionUncheckedUpdateInput = {
      ...body,
      updatedById: user.data.id,
    };
    if (body.status === AccountBalanceTransactionStatus.PENDING) {
      argsUpdateAccBalanceTrans.updatedById = null;
      argsUpdateAccBalanceTrans.updatedAt = null;
    }
    const updatedResponse = await this.accountBalanceTransactionService.update(id, argsUpdateAccBalanceTrans);
    if (body.status === AccountBalanceTransactionStatus.ADDED) {
      await this.userService.update(userFound.id, { balance: { increment: accountBalanceTransaction.amount } });
      // Send Email to user
      await this.nodeMailer.sendEmail([userFound.email], `Transaction Confirmation`, `The deposit transaction (id: ${id}) has been successfully confirmed`);
    }
    // Notification to user
    await this.notificationLogsService.create({
      data: {
        title: NotificationUserTitle.MONEY_TRANSACTION_DEPOSIT_ACCEPTED,
        accountBalanceTransactionId: accountBalanceTransaction.id,
        type: NotificationType.MONEY_TRANSACTION,
        targetUserId: userFound.id,
      },
    });
    return updatedResponse;
  }

  // update topup
  @ApiBearerAuth()
  @Roles(UserRole.ADMIN, UserRole.EMPLOYEE)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Patch(':id/update-topup')
  async updateStatusTopup(@Param('id') id: number, @UserDecorator() user: IUserJwt, @Body() body: AccountBalanceTransactionChangeStatus) {
    const accountBalanceTransaction = await this.accountBalanceTransactionService.findOne({
      where: {
        id,
        status: { in: [AccountBalanceTransactionStatus.PENDING, AccountBalanceTransactionStatus.PENDING_APPROVAL] },
        type: AccountBalanceTransactionType.DEPOSIT,
      },
      include: { advertisingAccount: true },
    });
    // const adsAccount = await this.advertisingAccountService.findOne({where: {id: accountBalanceTransaction.advertisingAccountId}});
    // const isHKAndGG = adsAccount.agencyService === AgencyServiceType.HONGKONG && adsAccount.type === AdvertisingAccountType.GOOGLE;

    if (
      (accountBalanceTransaction.status === AccountBalanceTransactionStatus.PENDING &&
        body.status !== AccountBalanceTransactionStatus.CANCELED &&
        body.status !== AccountBalanceTransactionStatus.PENDING_APPROVAL) ||
      (accountBalanceTransaction.status === AccountBalanceTransactionStatus.PENDING_APPROVAL &&
        body.status !== AccountBalanceTransactionStatus.CANCELED &&
        body.status !== AccountBalanceTransactionStatus.ADDED)
    ) {
      throw new BaseException(
        Errors.BAD_REQUEST(`Can not update status to ${body.status === AccountBalanceTransactionStatus.PENDING_APPROVAL ? 'ON THE WAY' : body.status}`),
      );
    }
    const advertisingAccount = await this.advertisingAccountService.findOne({ where: { id: accountBalanceTransaction.advertisingAccountId } });
    if (!accountBalanceTransaction) throw new BaseException(Errors.BAD_REQUEST('Request not found'));
    const userFound = await this.userService.findOne({
      // @ts-ignore
      where: { id: accountBalanceTransaction.advertisingAccount.userId },
    });
    if (!userFound) throw new BaseException(Errors.BAD_REQUEST('User not found'));

    const agencyWallet = await this.agencyBalanceService.findOne({ where: { type: advertisingAccount.agencyService } });
    if (!agencyWallet) throw new BaseException(Errors.BAD_REQUEST(`Agency wallet ${advertisingAccount.agencyService} not founded`));
    // C1: Nạp tiền vào tài khoản quảng cáo
    if (body.status === AccountBalanceTransactionStatus.PENDING_APPROVAL) {
      if (agencyWallet.balance < accountBalanceTransaction.amount)
        throw new BaseException(Errors.BAD_REQUEST(`The ${agencyWallet.type} wallet does not have sufficient balance`));
      const { updatedById, status, error } = await this.autoCallAgencyLv1(
        accountBalanceTransaction.amount,
        advertisingAccount.idAccount,
        advertisingAccount.type,
        advertisingAccount.agencyService,
        user.data.id,
      );
      if (error) throw new BaseException(Errors.BAD_REQUEST(error));
      // if (status === AccountBalanceTransactionStatus.PENDING_APPROVAL) {
      // await this.advertisingAccountService.update(accountBalanceTransaction.advertisingAccountId, {
      //   balance: { increment: accountBalanceTransaction.amount },
      // });
      // }
    }
    if (body.status === AccountBalanceTransactionStatus.ADDED) {
      // if (agencyWallet.balance < accountBalanceTransaction.amount)
      //   throw new BaseException(Errors.BAD_REQUEST(`The ${agencyWallet.type} wallet does not have sufficient balance`));
      // const { updatedById, status, error } = await this.autoCallAgencyLv1(
      //   accountBalanceTransaction.amount,
      //   advertisingAccount.idAccount,
      //   advertisingAccount.type,
      //   advertisingAccount.agencyService,
      //   user.data.id,
      // );

      // if (error) throw new BaseException(Errors.BAD_REQUEST(error));

      // if (status === AccountBalanceTransactionStatus.ADDED) {
      await this.advertisingAccountService.update(accountBalanceTransaction.advertisingAccountId, {
        balance: { increment: accountBalanceTransaction.amount },
      });
      const logs = await this.notificationLogsService.create({
        data: {
          title: NotificationUserTitle.ACCOUNT_BALANCE_DEPOSIT_ACCEPTED,
          accountBalanceTransactionId: accountBalanceTransaction.id,
          type: NotificationType.ACCOUNT_BALANCE,
          targetUserId: userFound.id,
        },
      });
      await this.agencyBalanceLogsService.create({
        data: {
          amount: accountBalanceTransaction.amount,
          advertisingAccountId: advertisingAccount.id,
          type: AgencyBalanceLogsType.DECREASE,
          createdById: user.data.id,
          NotificationLogs: { connect: { id: logs.id } },

          agencyWalletBalanceBefore: agencyWallet.balance,
          agencyWalletBalanceAfter: agencyWallet.balance - accountBalanceTransaction.amount,
        },
      });
      await this.agencyBalanceService.update(agencyWallet.id, { balance: { decrement: accountBalanceTransaction.amount } });
      // }
    }

    if (body.status === AccountBalanceTransactionStatus.CANCELED) {
      await this.userService.update(userFound.id, {
        balance: { increment: accountBalanceTransaction.totalCost },
      });
      await this.notificationLogsService.create({
        data: {
          title: NotificationUserTitle.ACCOUNT_BALANCE_DEPOSIT_REJECTED,
          accountBalanceTransactionId: accountBalanceTransaction.id,
          type: NotificationType.ACCOUNT_BALANCE,
          targetUserId: userFound.id,
        },
      });
      await this.accountBalanceTransactionService.create({
        data: {
          type: AccountBalanceTransactionType.DENINED_DEPOSIT,
          advertisingAccountId: advertisingAccount.id,
          amount: accountBalanceTransaction.amount,
          totalCost: accountBalanceTransaction.totalCost,
          userId: userFound.id,
          updatedById: user.data.id,
        },
      });
    }

    const argsUpdateAccBalanceTrans: Prisma.AccountBalanceTransactionUncheckedUpdateInput = {
      status: body.status,
      updatedById: user.data.id,
    };
    if (body.status !== AccountBalanceTransactionStatus.ADDED && body.status !== AccountBalanceTransactionStatus.CANCELED) {
      argsUpdateAccBalanceTrans.updatedAt = null;
      argsUpdateAccBalanceTrans.updatedById = null;
    }

    return this.accountBalanceTransactionService.update(id, argsUpdateAccBalanceTrans);
  }

  // update Refund
  @ApiBearerAuth()
  @Roles(UserRole.ADMIN, UserRole.EMPLOYEE)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Patch(':id/update-refund')
  async updateStatusRefund(@Param('id') id: number, @UserDecorator() user: IUserJwt, @Body() body: AccountBalanceTransactionChangeStatus) {
    // if (body.status === AccountBalanceTransactionStatus.PENDING) {
    //   throw new BaseException(Errors.BAD_REQUEST(`Can not update status to ${AccountBalanceTransactionStatus.PENDING}`));
    // }
    const accountBalanceTransaction = await this.accountBalanceTransactionService.findOne({
      where: { id, status: AccountBalanceTransactionStatus.PENDING, type: AccountBalanceTransactionType.WITHDRAWAL },
      include: { advertisingAccount: true },
    });
    if (!accountBalanceTransaction) throw new BaseException(Errors.BAD_REQUEST('Request not found'));
    const userFound = await this.userService.findOne({
      // @ts-ignore
      where: { id: accountBalanceTransaction.advertisingAccount.userId },
    });
    if (!userFound) throw new BaseException(Errors.BAD_REQUEST('User not found'));

    const incrementAmount = accountBalanceTransaction.amount - body.refundReceived;
    // const adsAccount = await this.advertisingAccountService.findOne({
    //   where: { id: accountBalanceTransaction.advertisingAccountId },
    // });
    // const actuallyBalance = adsAccount.balance + accountBalanceTransaction.amount;
    // const updateArgs: Prisma.AdvertisingAccountUncheckedUpdateInput = {
    //   updatedById: user.data.id
    // }
    if (body.refundReceived) {
      // If actually refund smaller than request
      // if (actuallyBalance < body.refundReceived) throw new BaseException(Errors.BAD_REQUEST(`${adsAccount.title} balance is not enough`));
      // Change balance for new ads account
      await this.advertisingAccountService.update(accountBalanceTransaction.advertisingAccountId, { balance: { increment: incrementAmount } });
    }
    await this.accountBalanceTransactionService.update(accountBalanceTransaction.id, { refundReceived: body.refundReceived, updatedById: user.data.id });

    if (body.status === AccountBalanceTransactionStatus.ADDED) {
      // increase wallet balance
      if (accountBalanceTransaction.target === WithdrawTarget.WALLET) {
        await this.userService.update(userFound.id, {
          // balance: { increment: accountBalanceTransaction.amount },
          balance: { increment: body.refundReceived || accountBalanceTransaction.amount },
        });
        // add transaction refund when added
        // await this.accountBalanceTransactionService.create({data:
        //   { refundReceived: body.refundReceived,
        //     amount: accountBalanceTransaction.amount,
        //     type: AccountBalanceTransactionType.WALLET_WITHDRAWAL,
        //     updatedById: user.data.id,
        //     userId: user.data.id
        //   }});
      }
      // Update status of ads account to REFUNDED, user can not using it to Ads
      await this.advertisingAccountService.update(accountBalanceTransaction.advertisingAccountId, {
        status: AdvertisingAccountStatus.REFUNDED,
        updatedBy: { connect: { id: user.data.id } },
      });
      // await this.nodeMailer.sendEmail(
      //   [userFound.email],
      //   `Request Confirmation`,
      //   `Refund request (id: ${id}) has been confirmed, Your balance has been updated`
      // )
      await this.notificationLogsService.create({
        data: {
          title: NotificationUserTitle.ACCOUNT_BALANCE_WITHDRAWAL_ACCEPTED,
          accountBalanceTransactionId: accountBalanceTransaction.id,
          type: NotificationType.ACCOUNT_BALANCE,
          targetUserId: userFound.id,
        },
      });
    }

    const argsUpdateAccBalanceTrans: Prisma.AccountBalanceTransactionUncheckedUpdateInput = {
      status: body.status,
      updatedById: user.data.id,
    };
    switch (body.status) {
      case AccountBalanceTransactionStatus.PENDING:
        argsUpdateAccBalanceTrans.updatedAt = null;
        argsUpdateAccBalanceTrans.updatedById = user.data.id;
        break;
      case AccountBalanceTransactionStatus.ADDED:
      case AccountBalanceTransactionStatus.CANCELED:
      default:
        break;
    }
    // if (body.status !== AccountBalanceTransactionStatus.ADDED && body.status !== AccountBalanceTransactionStatus.CANCELED) {
    //   argsUpdateAccBalanceTrans.updatedAt = null;
    //   argsUpdateAccBalanceTrans.updatedById = null;
    // }

    if (body.status === AccountBalanceTransactionStatus.CANCELED) {
      argsUpdateAccBalanceTrans.refundReceived = 0;
      await this.accountBalanceTransactionService.create({
        data: {
          type: AccountBalanceTransactionType.DENINED_WITHDRAWAL,
          advertisingAccountId: accountBalanceTransaction.advertisingAccountId,
          amount: accountBalanceTransaction.amount,
          refundReceived: 0,
          totalCost: 0,
          userId: userFound.id,
          updatedById: user.data.id,
          target: accountBalanceTransaction.target,
        },
      });
      await this.advertisingAccountService.update(accountBalanceTransaction.advertisingAccountId, { balance: { increment: accountBalanceTransaction.amount } });
      await this.notificationLogsService.create({
        data: {
          title: NotificationUserTitle.ACCOUNT_BALANCE_WITHDRAWAL_REJECTED,
          accountBalanceTransactionId: accountBalanceTransaction.id,
          type: NotificationType.ACCOUNT_BALANCE,
          targetUserId: userFound.id,
        },
      });
    }

    return this.accountBalanceTransactionService.update(id, argsUpdateAccBalanceTrans);
  }
}
