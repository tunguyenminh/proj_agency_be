import { Module, forwardRef } from '@nestjs/common';
import { AccountBalanceTransactionService } from './account-balance-transaction.service';
import { AccountBalanceTransactionController } from './account-balance-transaction.controller';
import { PrismaModule } from 'prisma/prisma.module';
import { UserModule } from '../user/user.module';
import { AdvertisingAccountModule } from '../advertising_account/advertising_account.module';
import { NotificationModule } from '../notification-log/notification-log.module';
import { NodeMailerModule } from 'src/core/node-mailer/node-mailer.module';
import { AgencyBalanceModule } from '../agency-balance/agency-balance.module';
import { AgencyBalanceLogsModule } from '../agency-balance-logs/agency-balance-logs.module';
import { ConfigurationModule } from '../configuration/configuration.module';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [
    PrismaModule,
    forwardRef(() => UserModule),
    forwardRef(() => AdvertisingAccountModule),
    forwardRef(() => NotificationModule),
    forwardRef(() => NodeMailerModule),
    forwardRef(() => AgencyBalanceModule),
    forwardRef(() => AgencyBalanceLogsModule),
    forwardRef(() => ConfigurationModule),
    forwardRef(() => ConfigModule),

  ],
  controllers: [AccountBalanceTransactionController],
  providers: [AccountBalanceTransactionService],
  exports: [AccountBalanceTransactionService]
})
export class AccountBalanceTransactionModule { }
