import { ApiProperty } from '@nestjs/swagger';
import { AccountBalanceTransactionType, AccountBalanceTransactionStatus } from '@prisma/client';
import { Transform, Type } from 'class-transformer';
import { IsBoolean, IsEnum, IsNotEmpty, IsOptional, IsString } from 'class-validator';
import moment from 'moment';
import { FilterOptions } from 'src/base/filterOption.dto';
import { MoneyTransactionMethod, WalletFlowStatus } from 'src/constants/common.constant';
import { AccountTypeFilterEnum, Language } from 'src/constants/enum.constant';
export class FilterAccountBalanceTransactionDto extends FilterOptions {
  @ApiProperty({
    example: AccountBalanceTransactionType.DEPOSIT,
    required: false,
  })
  @IsOptional()
  @IsEnum(AccountBalanceTransactionType)
  readonly type?: AccountBalanceTransactionType;

  @ApiProperty({
    example: [AccountBalanceTransactionStatus.ADDED],
    required: false,
  })
  @IsOptional()
  @IsString({ each: true })
  @Transform(({ value }) => (value ? value.split(',') : null))
  readonly status: AccountBalanceTransactionStatus[];

  @ApiProperty({
    example: [AccountTypeFilterEnum.FACEBOOK],
    required: false,
  })
  @IsOptional()
  @IsString({ each: true })
  @Transform(({ value }) => (value ? value.split(',') : null))
  readonly accountType?: AccountTypeFilterEnum[];

  @ApiProperty({
    example: '2023-08-21',
    required: false,
  })
  @Transform(({ value }) => moment(value ?? null).toDate())
  @Type(() => Date)
  @IsOptional()
  readonly from?: Date;

  @ApiProperty({
    example: '2023-08-21',
    required: false,
  })
  @Transform(({ value }) => moment(value ?? null).toDate())
  @Type(() => Date)
  @IsOptional()
  readonly to?: Date;

  @ApiProperty({
    example: [WalletFlowStatus.GOOGLE_DEPOSIT, WalletFlowStatus.GOOGLE_WITHDRAWAL, WalletFlowStatus.FACEBOOK_DEPOSIT, WalletFlowStatus.FACEBOOK_WITHDRAWAL],
    required: false,
  })
  @IsOptional()
  @IsString({ each: true })
  @Transform(({ value }) => (value ? value.split(',') : null))
  readonly walletFlowStatus?: WalletFlowStatus[];

  @ApiProperty({
    // example: Language.EN,
    example: Language.EN,
    required: false,
  })
  @IsOptional()
  @IsEnum(Language)
  readonly lang?: Language;

  @ApiProperty({
    // example: Language.EN,
    example: true,
    required: false,
  })
  @IsOptional()
  @IsBoolean()
  @Transform(({ value }) => value === 'true')
  readonly walletFlow?: Language;
}
