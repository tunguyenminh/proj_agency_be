import { ApiProperty } from '@nestjs/swagger';
import {
    AccountBalanceTransactionType,
} from '@prisma/client';
import {
    IsNotEmpty,
    IsEnum,
    IsNumber,
    IsString,
    IsOptional,
    IsBoolean,
} from 'class-validator';
import { MoneyTransactionMethod } from 'src/constants/common.constant';
import { WithdrawTarget } from 'src/constants/enum.constant';

export class CreateAccountBalanceDto {
    @ApiProperty({
        example: AccountBalanceTransactionType.DEPOSIT,
        required: true,
    })
    @IsNotEmpty()
    @IsEnum(AccountBalanceTransactionType)
    type: AccountBalanceTransactionType

    @ApiProperty({
        example: 3,
        required: true,
    })
    @IsNotEmpty()
    @IsNumber()
    amount: number

    @ApiProperty({
        example: 1,
        required: true,
    })
    @IsNotEmpty()
    @IsNumber()
    advertisingAccountId: number

    @ApiProperty({
        example: WithdrawTarget.BANK
    })
    @IsEnum(WithdrawTarget)
    target?: WithdrawTarget

    @ApiProperty({
        example: 'Lorem Ipsum'
    })
    @IsOptional()
    @IsString()
    imageWithdraw?: string

    @ApiProperty({
        example: 'Lorem Ipsum'
    })
    @IsOptional()
    @IsString()
    reasonRefund?: string

}

export class CreateWalletRechargeDto {
    @ApiProperty({
        example: "Lorem Ipsum",
        required: true,
    })
    @IsOptional()
    @IsString()
    note?: string

    @ApiProperty({
        example: MoneyTransactionMethod.BANK_PHILIPPINES,
        required: true,
    })
    @IsNotEmpty()
    @IsOptional()
    @IsEnum(MoneyTransactionMethod)
    method: MoneyTransactionMethod

    @ApiProperty({
        example: "Lorem Ipsum",
        required: true,
    })
    @IsOptional()
    @IsString()
    image?: string

    @ApiProperty({
        example: 1000,
        required: true,
    })
    @IsOptional()
    @IsNumber()
    amount?: number
}

export class CalculatorWalletRechargeDto {
    @ApiProperty({
        example: 1000,
        required: true,
    })
    @IsOptional()
    @IsNumber()
    amount?: number

    @ApiProperty({
        example: MoneyTransactionMethod.BANK_PHILIPPINES,
        required: true,
    })
    @IsNotEmpty()
    @IsOptional()
    @IsEnum(MoneyTransactionMethod)
    method: MoneyTransactionMethod
}