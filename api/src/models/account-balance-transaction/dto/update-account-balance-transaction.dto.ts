import { ApiProperty, PartialType } from '@nestjs/swagger';
import { AccountBalanceTransactionStatus, AccountBalanceTransactionType } from '@prisma/client';
import { IsNotEmpty, IsEnum, IsNumber, IsOptional } from 'class-validator';
import { CreateAccountBalanceDto } from './create-account-balance-transaction.dto';

export class UpdateAccountBalanceTransactionDto extends PartialType(CreateAccountBalanceDto) { }

export class AccountBalanceTransactionChangeStatus {
    @ApiProperty({
        example: AccountBalanceTransactionStatus.ADDED,
        required: true,
    })
    @IsNotEmpty()
    @IsEnum(AccountBalanceTransactionStatus)
    status: AccountBalanceTransactionStatus

    @ApiProperty({
        example: 100,
        required: true,
    })
    @IsOptional()
    @IsNumber()
    refundReceived?: number
}