import { Test, TestingModule } from '@nestjs/testing';
import { BmShareRequestController } from './bm-share-request.controller';
import { BMShareRequestService } from './bm-share-request.service';

describe('BmShareRequestController', () => {
  let controller: BmShareRequestController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [BmShareRequestController],
      providers: [BMShareRequestService],
    }).compile();

    controller = module.get<BmShareRequestController>(BmShareRequestController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
