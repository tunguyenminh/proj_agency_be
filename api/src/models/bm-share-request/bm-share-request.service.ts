import { Injectable } from '@nestjs/common';
import { PrismaService } from 'prisma/prisma.service';
import { Prisma } from '@prisma/client';

@Injectable()
export class BMShareRequestService {
  constructor(
    private readonly prismaService: PrismaService,
  ) { }
  create(args: Prisma.BMShareRequestCreateArgs) {
    return this.prismaService.bMShareRequest.create(args)
  }

  findAll(args: Prisma.BMShareRequestFindManyArgs) {
    return this.prismaService.bMShareRequest.findMany(args)
  }

  findOne(args: Prisma.BMShareRequestFindFirstArgs) {
    return this.prismaService.bMShareRequest.findFirst(args)
  }

  update(id: number, args: Prisma.BMShareRequestUpdateInput) {
    return this.prismaService.bMShareRequest.update({ where: { id }, data: args })
  }

  count(args: Prisma.BMShareRequestCountArgs) {
    return this.prismaService.bMShareRequest.count(args)
  }
  remove(args: Prisma.BMShareRequestDeleteArgs) {
    return this.prismaService.bMShareRequest.delete(args)

  }
}
