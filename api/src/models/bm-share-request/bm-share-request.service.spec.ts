import { Test, TestingModule } from '@nestjs/testing';
import { BmShareRequestService } from './bm-share-request.service';

describe('BmShareRequestService', () => {
  let service: BmShareRequestService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [BmShareRequestService],
    }).compile();

    service = module.get<BmShareRequestService>(BmShareRequestService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
