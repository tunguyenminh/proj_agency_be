import { Body, Controller, Delete, Get, Param, Patch, Post, Query, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { AdvertisingAccountStatus, AgencyServiceType, BMShareRequestStatus, Prisma, UserRole } from '@prisma/client';
import { Roles } from 'src/auth/decorators/roles.decorator';
import { User, User as UserDecorator } from 'src/auth/decorators/user.decorator';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { RolesGuard } from 'src/auth/guards/roles.guard';
import { IUserJwt } from 'src/auth/strategies/jwt.strategy';
import { NotificationTitle, NotificationType, NotificationUserTitle, UNICode } from 'src/constants/common.constant';
import { SortOrder } from 'src/constants/enum.constant';
import { BaseException, Errors } from 'src/constants/error.constant';
import { funcListPaging } from 'src/helpers/list-paging';
import { AdvertisingAccountService } from '../advertising_account/advertising_account.service';
import { NotificationLogsService } from '../notification-log/notification-log.service';
import { BMShareRequestService } from './bm-share-request.service';
import { CreateBmShareRequestDto } from './dto/create-bm-share-request.dto';
import { FilterBmShareRequestDto } from './dto/filter-bm-share-request.dto';
import { BMShareUpdateStatusDto } from './dto/update-bm-share-request.dto';
import { IBodyManageApplication, UniService } from 'src/services/uni/uni.service';
import { tranformAdsType } from 'src/utils/common.utils';

@ApiTags('BM Share Request')
@Controller('bm-share-request')
export class BmShareRequestController {
  constructor(
    private readonly bmShareRequestService: BMShareRequestService,
    private readonly advertisingAccountService: AdvertisingAccountService,
    private readonly notificationService: NotificationLogsService,
    private readonly uniService: UniService,
  ) {}

  @ApiBearerAuth()
  @Roles(UserRole.CUSTOMER, UserRole.EMPLOYEE)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Post(':advertisingAccountId')
  async create(@Param('advertisingAccountId') advertisingAccountId: number, @Body() createBmShareRequestDto: CreateBmShareRequestDto, @User() user: IUserJwt) {
    if (!Number(advertisingAccountId)) throw new BaseException(Errors.BAD_REQUEST('advertisingAccountId is not number'));
    const advertisingAccount = await this.advertisingAccountService.findOne({
      where: { id: advertisingAccountId, status: { not: AdvertisingAccountStatus.DELETED }, userId: user.data.id },
    });
    if (!advertisingAccount) throw new BaseException(Errors.BAD_REQUEST('Account not found'));
    if (advertisingAccount.status !== AdvertisingAccountStatus.ACTIVE) throw new BaseException(Errors.BAD_REQUEST('Account is not active'));
    const bmShare = await this.bmShareRequestService.create({ data: { ...createBmShareRequestDto, advertisingAccountId } });
    await this.notificationService.create({ data: { title: NotificationTitle.BM_SHARE, bMShareRequestId: bmShare.id, type: NotificationType.BM_SHARE } });
    return bmShare;
  }

  @ApiBearerAuth()
  @Roles(UserRole.CUSTOMER, UserRole.ADMIN, UserRole.EMPLOYEE)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Get()
  async findAll(@UserDecorator() user: IUserJwt, @Query() options: FilterBmShareRequestDto) {
    let whereInput: Prisma.BMShareRequestFindManyArgs;

    const sortField = options?.sortField || 'createdAt';
    const sortOrder = options?.sortOrder || SortOrder.DESC;
    let where: Prisma.BMShareRequestWhereInput = {};
    where.AND = [];

    if (user.role === UserRole.CUSTOMER) {
      where = {
        ...where,
        advertisingAccount: {
          userId: user.data.id,
        },
      };
    }

    if (options.textSearch) {
      // @ts-ignore
      where.AND.push({
        OR: [
          { advertisingAccount: { title: { contains: options.textSearch } } },
          // { advertisingAccount: { user: { firstName: { contains: options.textSearch } } } },
          // { advertisingAccount: { user: { email: { contains: options.textSearch } } } }
        ],
      });
    }

    if (options.code) {
      // @ts-ignore
      where.AND.push({
        code: { contains: options.code },
      });
    }

    if (options.status) {
      where = {
        ...where,
        status: { in: options.status },
      };
    }

    if (options.accountType) {
      where = {
        ...where,
        // @ts-ignore
        advertisingAccount: {
          ...where?.advertisingAccount,
          type: options.accountType,
        },
      };
    }

    whereInput = {
      where,
      orderBy: {
        [sortField]: sortOrder,
      },
      include: {
        updatedBy: true,
        advertisingAccount: {
          include: { user: true },
        },
      },
    };

    return await funcListPaging(this.bmShareRequestService, whereInput, options?.page, options?.perPage);
  }

  @ApiBearerAuth()
  @Roles(UserRole.CUSTOMER, UserRole.ADMIN, UserRole.EMPLOYEE)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Get(':id')
  findOne(@Param('id') id: number) {
    return this.bmShareRequestService.findOne({
      where: { id },
      include: {
        updatedBy: true,
        advertisingAccount: {
          include: { user: true },
        },
      },
    });
  }

  @ApiBearerAuth()
  @Roles(UserRole.ADMIN, UserRole.EMPLOYEE)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Patch(':id')
  async update(@Param('id') id: number, @Body() args: BMShareUpdateStatusDto, @UserDecorator() user: IUserJwt) {
    let bmShare = await this.bmShareRequestService.findOne({ where: { id, status: BMShareRequestStatus.PENDING }, include: { advertisingAccount: true } });
    if (!bmShare) throw new BaseException(Errors.BAD_REQUEST('BM Share not found'));
    if (args.status === BMShareRequestStatus.ACCEPTED) {
      if (!args.code) throw new BaseException(Errors.BAD_REQUEST('BM Share code not found'));
      // update on Agency Lv1
      // @ts-ignore
      switch (bmShare.advertisingAccount.agencyService) {
        case AgencyServiceType.UNI:
          const body: IBodyManageApplication = {
            // @ts-ignore
            mediaPlatform: tranformAdsType(bmShare.advertisingAccount.type),
            // @ts-ignore
            mediaAccountId: bmShare.advertisingAccount.idAccount,
            taskNumber: new Date().toISOString(),
            value: bmShare.code,
            role: 10,
          };
          try {
            const shareResponse = await this.uniService.manageApplication(body);
            if (shareResponse.data.code !== UNICode.SUCCESS) throw new BaseException(Errors.BAD_REQUEST(shareResponse.data.message));
          } catch (error) {
            throw new BaseException(Errors.BAD_REQUEST(error));
          }
          break;
        case AgencyServiceType.VN:
        case AgencyServiceType.HONGKONG:
        default:
          break;
      }

      await this.notificationService.create({
        data: {
          title: NotificationUserTitle.BM_SHARE_ACCEPTED,
          bMShareRequestId: id,
          type: NotificationType.BM_SHARE,
          // @ts-ignore
          targetUserId: bmShare.advertisingAccount.userId,
        },
      });
    }
    await this.notificationService.create({
      data: {
        title: NotificationUserTitle.BM_SHARE_REJECTED,
        bMShareRequestId: id,
        type: NotificationType.BM_SHARE,
        // @ts-ignore
        targetUserId: bmShare.advertisingAccount.userId,
      },
    });

    bmShare = await this.bmShareRequestService.update(+id, { status: args.status, updatedBy: { connect: { id: user.data.id } } });
    await this.advertisingAccountService.update(bmShare.advertisingAccountId, { bmShareCode: args.code });
    return bmShare;
  }

  @ApiBearerAuth()
  @Roles(UserRole.CUSTOMER, UserRole.ADMIN)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Delete(':id')
  remove(@Param('id') id: number) {
    return this.bmShareRequestService.remove({ where: { id } });
  }
}
