import { ApiProperty } from "@nestjs/swagger";
import { IsOptional, IsString } from "class-validator";

export class CreateBmShareRequestDto {
    @ApiProperty({
        example: "note",
        required: false,
    })
    @IsOptional()
    @IsString()
    note?: string

    @ApiProperty({
        example: "code",
        required: false,
    })
    @IsString()
    code: string
}
