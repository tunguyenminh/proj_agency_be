import { ApiProperty, PartialType } from '@nestjs/swagger';
import { CreateBmShareRequestDto } from './create-bm-share-request.dto';
import { BmShareRequest } from '../entities/bm-share-request.entity';
import { BMShareRequestStatus } from '@prisma/client';
import { IsEnum, IsNotEmpty, IsOptional } from 'class-validator';

export class UpdateBmShareRequestDto extends PartialType(CreateBmShareRequestDto) { }
export class BMShareUpdateStatusDto {
    @ApiProperty({
        example: BMShareRequestStatus.ACCEPTED
    })
    @IsNotEmpty()
    @IsEnum(BMShareRequestStatus)
    status: BMShareRequestStatus

    @ApiProperty({
        example: "code"
    })
    @IsOptional()
    code?: string
}