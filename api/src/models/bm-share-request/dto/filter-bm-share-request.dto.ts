import { ApiProperty } from "@nestjs/swagger";
import { BMShareRequestStatus, MoneyTransactionLogType } from "@prisma/client";
import { IsNotEmpty, IsEnum, IsOptional, IsString, IsNumber } from "class-validator";
import { FilterOptions } from "src/base/filterOption.dto";
import { BmShareRequest } from "../entities/bm-share-request.entity";
import { Transform, Type } from "class-transformer";
import { AccountTypeFilterEnum } from "src/constants/enum.constant";

export class FilterBmShareRequestDto extends FilterOptions {
    @ApiProperty({
        example: [BMShareRequestStatus.PENDING, BMShareRequestStatus.ACCEPTED],
        required: false,
    })
    @IsOptional()
    @IsString({ each: true })
    @Transform(({ value }) => value ? value.split(','): null)
    readonly status?: BMShareRequestStatus[];

    @Type(() => Number)
    @ApiProperty({
        example: 1,
        required: false,
    })
    @IsOptional()
    @IsNumber()
    userId?: number

    @ApiProperty({
        example: AccountTypeFilterEnum.FACEBOOK,
        required: false,
    })
    @IsOptional()
    @IsEnum(AccountTypeFilterEnum)
    readonly accountType?: AccountTypeFilterEnum;

    @ApiProperty({
        example: 'e6d64f9c-d37d-4f70-b60d-8e7919c4096f',
        required: false,
    })
    @IsOptional()
    @IsString()
    readonly code?: string;
}
