import { Module } from '@nestjs/common';
import { BMShareRequestService } from './bm-share-request.service';
import { BmShareRequestController } from './bm-share-request.controller';
import { PrismaModule } from 'prisma/prisma.module';
import { AdvertisingAccountModule } from '../advertising_account/advertising_account.module';
import { NotificationModule } from '../notification-log/notification-log.module';

@Module({
  controllers: [BmShareRequestController],
  providers: [BMShareRequestService],
  imports: [PrismaModule, AdvertisingAccountModule, NotificationModule],
  exports: [BMShareRequestService]
})
export class BmShareRequestModule { }
