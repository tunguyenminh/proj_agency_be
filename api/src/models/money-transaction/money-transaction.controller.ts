import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
  Query,
  Res,
} from '@nestjs/common';
import { CreateMoneyTransactionDto } from './dto/create-money-transaction-dto';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import {
  MoneyTransactionLogStatus,
  MoneyTransactionLogType,
  Prisma,
  User,
  UserRole,
  UserStatus,
} from '@prisma/client';
import { Roles } from 'src/auth/decorators/roles.decorator';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { RolesGuard } from 'src/auth/guards/roles.guard';
import { User as UserDecorator } from 'src/auth/decorators/user.decorator';
import { UserService } from '../user/user.service';
import { BaseException, Errors } from 'src/constants/error.constant';
import { IUserJwt } from 'src/auth/strategies/jwt.strategy';
import { SortOrder } from 'src/constants/enum.constant';
import { funcListPaging } from 'src/helpers/list-paging';
import { MoneyTransactionService } from './money-transaction.service';
import { FilterMoneyTransactionDto } from './dto/money-transaction-filter.dto';
import {
  MoneyTransactionChangeStatus,
  UpdateMoneyTransactionDto,
} from './dto/update-money-transaction.dto';
import { NotificationLogsService } from '../notification-log/notification-log.service';
import {
  CONFIGURATION_TYPE,
  NotificationTitle,
  NotificationType,
  NotificationUserTitle,
} from 'src/constants/common.constant';
import { Response } from 'express';
import moment from 'moment';
import { exportToExcel } from 'src/utils/common.utils';
import { ConfigurationService } from '../configuration/configuration.service';
import { NodeMailerService } from 'src/core/node-mailer/node-mailer.service';

@ApiTags('Money Transaction')
@Controller('money-transaction')
export class MoneyTransactionController {
  constructor(
    private readonly moneyTransactionService: MoneyTransactionService,
    private readonly userService: UserService,
    private readonly configurationService: ConfigurationService,
    private readonly notificationLogsService: NotificationLogsService,
    private readonly nodeMailer: NodeMailerService,
  ) { }

  @ApiBearerAuth()
  @Roles(UserRole.ADMIN, UserRole.CUSTOMER, UserRole.EMPLOYEE)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Get()
  async findAll(
    @UserDecorator() user: IUserJwt,
    @Query() options: FilterMoneyTransactionDto,
  ) {
    let whereInput: Prisma.MoneyTransactionLogFindManyArgs;

    const sortField = options?.sortField || 'createdAt';
    const sortOrder = options?.sortOrder || SortOrder.DESC;
    let where: Prisma.MoneyTransactionLogWhereInput = {};
    where.AND = [];

    if (user.role === UserRole.CUSTOMER) {
      where = {
        ...where,
        userId: user.data.id,
      };
    }

    if (options.textSearch) {
      // @ts-ignore
      where.AND.push({
        OR: [
          { user: { lastName: { contains: options.textSearch } } },
          { user: { firstName: { contains: options.textSearch } } },
          { user: { email: { contains: options.textSearch } } },
        ],
      });
    }

    if (options.status) {
      where = {
        ...where,
        status: { in: options.status },
      };
    }

    if (options.method) {
      where = {
        ...where,
        //@ts-ignore
        method: options.method,
      };
    }

    if (options.type) {
      where = {
        ...where,
        type: options.type,
      };
    }

    if (options.userId) {
      where = {
        ...where,
        userId: options.userId,
      };
    }

    if (options.from) {
      where = {
        ...where,
        createdAt: {
          gte: moment(options?.from).startOf('day').toDate(),
          lte: moment(options?.to).endOf('day').toDate(),
        },
      };
    }

    whereInput = {
      where,
      orderBy: {
        [sortField]: sortOrder,
      },
      include: {
        user: true,
        updatedBy: true,
      },
    };

    return await funcListPaging(
      this.moneyTransactionService,
      whereInput,
      options?.page,
      options?.perPage,
    );
  }

  @ApiBearerAuth()
  @Roles(UserRole.CUSTOMER, UserRole.ADMIN, UserRole.EMPLOYEE)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Get('export-excel')
  async exportExcel(
    @UserDecorator() user: IUserJwt,
    @Query() options: FilterMoneyTransactionDto,
  ) {
    let whereInput: Prisma.MoneyTransactionLogFindManyArgs;

    const sortField = options?.sortField || 'createdAt';
    const sortOrder = options?.sortOrder || SortOrder.DESC;
    let where: Prisma.MoneyTransactionLogWhereInput = {};
    where.AND = [];

    if (user.role === UserRole.CUSTOMER) {
      where = {
        ...where,
        userId: user.data.id,
      };
    }

    if (options.status) {
      where = {
        ...where,
        status: { in: options.status },
      };
    }

    if (options.method) {
      where = {
        ...where,
        //@ts-ignore
        method: options.method,
      };
    }

    if (options.from) {
      where = {
        ...where,
        createdAt: {
          gte: moment(options.from).startOf('day').toDate(),
          lte: moment(options.to).endOf('day').toDate(),
        },
      };
    }

    if (options.type) {
      where = {
        ...where,
        type: options.type,
      };
    }

    whereInput = {
      where,
      orderBy: {
        [sortField]: sortOrder,
      },
      include: {
        user: true,
        updatedBy: true,
      },
    };

    const data = await this.moneyTransactionService.findAll(whereInput);

    const _data = [];
    let headers: string[]
  
    data.forEach((x: any) => {
      if (user.role === UserRole.ADMIN || UserRole.EMPLOYEE) {
        _data.push({
          createdAt: x.createdAt,
          account: x.user.email,
          amount: x.amount,
          type: x.method,
          status: x.status,
        });
      } else {
        _data.push({
          createdAt: x.createdAt,
          amount: x.amount,
          type: x.method,
          status: x.status,
        });
      }
    });

    if (user.role === UserRole.ADMIN || UserRole.EMPLOYEE) {
      headers = ['Thời gian tạo', 'Tên người tạo', 'Số Tiền', 'Phương thức nạp', 'Trạng thái']
    }

    else {
      headers = ['Thời gian tạo', 'Số Tiền', 'Phương thức nạp', 'Trạng thái']
    }

    const fileName = await exportToExcel(_data, headers);

    return {
      uri: `excels/${fileName}`,
    };
  }

  @ApiBearerAuth()
  @Roles(UserRole.ADMIN, UserRole.CUSTOMER, UserRole.EMPLOYEE)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Get(':id')
  async findOne(@Param('id') id: number) {
    return this.moneyTransactionService.findOne({
      where: { id },
      include: { user: true, updatedBy: true },
    });
  }

  @ApiBearerAuth()
  @Roles(UserRole.CUSTOMER, UserRole.EMPLOYEE)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Post()
  async create(
    @UserDecorator() user: IUserJwt,
    @Body() body: CreateMoneyTransactionDto,
  ) {
    const userFound = await this.userService.findOne({
      where: { id: user.data.id, status: { not: UserStatus.DELETED } },
    });
    if (!userFound)
      throw new BaseException(Errors.BAD_REQUEST('User not found'));
    if (userFound.status === UserStatus.BANNED)
      throw new BaseException(Errors.BAD_REQUEST('User has been banned'));
    const config = await this.configurationService.findOne({
      where: { settingCode: body.method },
    });
    if (!config)
      throw new BaseException(Errors.BAD_REQUEST('Method not found'));


    // TODO Calculate fee
    let TOTAL_AMOUNT = body.amount + body.amount * config.settingFee / 100
    if (config.type === CONFIGURATION_TYPE.RATIO) {
      TOTAL_AMOUNT = body.amount * config.settingFee;
    }    
    
    if (body.type === MoneyTransactionLogType.WITHDRAWAL) {
      if (TOTAL_AMOUNT > userFound.balance)
        throw new BaseException(
          Errors.BAD_REQUEST('Account balance is not enough to withdrawal'),
        );

      await this.userService.update(userFound.id, {
        balance: { decrement: body.amount },
      });
    }

    let createArgs: Prisma.MoneyTransactionLogUncheckedCreateInput = {
      ...body,
      userId: user.data.id,
      //@ts-ignore
      methodDetail: config.settingValue,
      totalPaid: TOTAL_AMOUNT
    };    

    const moneyTransactionLog = await this.moneyTransactionService.create({
      data: createArgs,
    });
    await this.notificationLogsService.create({
      data: {
        title: NotificationTitle.MONEY_TRANSACTION,
        moneyTransactionLogId: moneyTransactionLog.id,
        type: NotificationType.MONEY_TRANSACTION,
      },
    });
    return moneyTransactionLog;
  }

  @ApiBearerAuth()
  @Roles(UserRole.CUSTOMER)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Patch(':id')
  async update(
    @Param('id') id: number,
    @UserDecorator() user: IUserJwt,
    @Body() body: UpdateMoneyTransactionDto,
  ) {
    //@ts-ignore
    let updateArgs: Prisma.MoneyTransactionLogUncheckedUpdateInput = {
      ...body,
      updatedById: user.data.id
    };
    return this.moneyTransactionService.update(id, updateArgs);
  }

  @ApiBearerAuth()
  @Roles(UserRole.ADMIN, UserRole.EMPLOYEE)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Patch(':id/status')
  async changeStatus(
    @Param('id') id: number,
    @UserDecorator() user: IUserJwt,
    @Body() body: MoneyTransactionChangeStatus,
  ) {
    const moneyTransaction = await this.moneyTransactionService.findOne({
      where: { id, status: MoneyTransactionLogStatus.PENDING },
    });
    if (!moneyTransaction)
      throw new BaseException(
        Errors.BAD_REQUEST('Money transaction not found'),
      );
    const userFound = await this.userService.findOne({
      where: { id: moneyTransaction.userId },
    });
    if (!userFound)
      throw new BaseException(Errors.BAD_REQUEST('User not found'));
    if (body.status === MoneyTransactionLogStatus.ADDED) {
      if (moneyTransaction.type === MoneyTransactionLogType.DEPOSIT) {
        await this.userService.update(userFound.id, {
          balance: { increment: moneyTransaction.amount },
        });

        // send email to user
        await this.nodeMailer.sendEmail(
          [userFound.email],
          `Transaction Confirmation`,
          `The deposit transaction (id: ${id}) into the wallet has been successfully confirmed.`
        );
        // notification to user
        await this.notificationLogsService.create({
          data: {
            title: NotificationUserTitle.MONEY_TRANSACTION_DEPOSIT_ACCEPTED,
            moneyTransactionLogId: moneyTransaction.id,
            type: NotificationType.MONEY_TRANSACTION,
            targetUserId: userFound.id,
          },
        });
      }

      if (moneyTransaction.type === MoneyTransactionLogType.WITHDRAWAL) {
        // notification to user
        await this.notificationLogsService.create({
          data: {
            title: NotificationUserTitle.MONEY_TRANSACTION_WITHDRAWAL_ACCEPTED,
            moneyTransactionLogId: moneyTransaction.id,
            type: NotificationType.MONEY_TRANSACTION,
            targetUserId: userFound.id,
          },
        });
      }
    }

    if (body.status === MoneyTransactionLogStatus.CANCELED) {
      if (moneyTransaction.type === MoneyTransactionLogType.DEPOSIT) {
        // send email to user
        await this.nodeMailer.sendEmail(
          [userFound.email],
          `"Transaction Confirmation"`,
          `The deposit transaction (id: ${id}) into the wallet has been declined.`
        );
        // notification to user
        await this.notificationLogsService.create({
          data: {
            title: NotificationUserTitle.MONEY_TRANSACTION_DEPOSIT_REJECTED,
            moneyTransactionLogId: moneyTransaction.id,
            type: NotificationType.MONEY_TRANSACTION,
            targetUserId: userFound.id,
          },
        });
      }

      if (moneyTransaction.type === MoneyTransactionLogType.WITHDRAWAL) {
        // notification to user
        await this.userService.update(userFound.id, {
          balance: { increment: moneyTransaction.amount },
        });
        await this.notificationLogsService.create({
          data: {
            title: NotificationUserTitle.MONEY_TRANSACTION_WITHDRAWAL_REJECTED,
            moneyTransactionLogId: moneyTransaction.id,
            type: NotificationType.MONEY_TRANSACTION,
            targetUserId: userFound.id,
          },
        });
      }
    }

    return this.moneyTransactionService.update(id, {
      status: body.status,
      updatedBy: { connect: { id: user.data.id } },
      adminConfirmImage: body.adminConfirmImage,
    });
  }
}
