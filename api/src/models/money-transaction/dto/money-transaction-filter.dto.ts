import { ApiProperty } from "@nestjs/swagger";
import { MoneyTransactionLogType, MoneyTransactionLogStatus } from "@prisma/client";
import { Transform, Type } from "class-transformer";
import { IsEnum, IsNumber, IsOptional, IsString } from "class-validator";
import moment from "moment";
import { FilterOptions } from "src/base/filterOption.dto";
import { MoneyTransactionMethod } from "src/constants/common.constant";

export class FilterMoneyTransactionDto extends FilterOptions {
    @ApiProperty({
        example: MoneyTransactionLogType.DEPOSIT,
        required: false,
    })
    @IsOptional()
    @IsEnum(MoneyTransactionLogType)
    readonly type?: MoneyTransactionLogType;

    @ApiProperty({
        example: MoneyTransactionMethod.BANK_PHILIPPINES,
        required: false,
    })
    @IsOptional()
    @IsEnum(MoneyTransactionMethod)
    readonly method?: MoneyTransactionMethod;

    @ApiProperty({
        example: [MoneyTransactionLogStatus.PENDING, MoneyTransactionLogStatus.ADDED, MoneyTransactionLogStatus.CANCELED],
        required: false,
    })
    @IsOptional()
    @IsString({ each: true })
    @Transform(({ value }) => value ? value.split(','): null)
    readonly status?: MoneyTransactionLogStatus[];

    @ApiProperty({
        required: false,
        example: 1,
    })
    @Type(() => Number)
    @IsOptional()
    @IsNumber()
    readonly userId?: number;


    @ApiProperty({
        example: "2023-08-21",
        required: false,
    })
    @Transform(({ value }) => moment(value ?? null).toDate())
    @Type(() => Date)
    @IsOptional()
    readonly from?: Date;

    @ApiProperty({
        example: "2023-08-21",
        required: false,
    })
    @Transform(({ value }) => moment(value ?? null).toDate())
    @Type(() => Date)
    @IsOptional()
    readonly to?: Date;
}
