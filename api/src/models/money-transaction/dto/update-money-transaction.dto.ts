import { ApiProperty, PartialType } from '@nestjs/swagger';
import { CreateMoneyTransactionDto } from './create-money-transaction-dto';
import { MoneyTransactionLogStatus, MoneyTransactionLogType } from '@prisma/client';
import { IsNotEmpty, IsEnum, IsOptional, IsString } from 'class-validator';

export class UpdateMoneyTransactionDto extends PartialType(CreateMoneyTransactionDto) { }

export class MoneyTransactionChangeStatus {
    @ApiProperty({
        example: MoneyTransactionLogStatus.ADDED,
        required: true,
    })
    @IsNotEmpty()
    @IsEnum(MoneyTransactionLogStatus)
    status: MoneyTransactionLogStatus


    @ApiProperty({
        example: "23",
        required: false,
    })
    @IsOptional()
    @IsString()
    adminConfirmImage?: string
}