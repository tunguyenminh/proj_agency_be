import { ApiProperty } from '@nestjs/swagger';
import {
    MoneyTransactionLogType,
} from '@prisma/client';
import {
    IsNotEmpty,
    IsEnum,
    IsString,
    IsOptional,
    IsNumber,
} from 'class-validator';
import { MoneyTransactionMethod } from 'src/constants/common.constant';

export class CreateMoneyTransactionDto {
    @ApiProperty({
        example: MoneyTransactionLogType.DEPOSIT,
        required: true,
    })
    @IsNotEmpty()
    @IsEnum(MoneyTransactionLogType)
    type: MoneyTransactionLogType

    @ApiProperty({
        example: "123",
        required: true,
    })
    @IsOptional()
    @IsString()
    note?: string

    @ApiProperty({
        example: "123",
        required: true,
    })
    @IsOptional()
    @IsString()
    image?: string

    @ApiProperty({
        example: 3,
        required: true,
    })
    @IsNotEmpty()
    @IsNumber()
    amount: number

    @ApiProperty({
        example: MoneyTransactionMethod.BANK_PHILIPPINES,
        required: true,
    })
    @IsNotEmpty()
    @IsEnum(MoneyTransactionMethod)
    method: MoneyTransactionMethod
}
