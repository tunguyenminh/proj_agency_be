import { Injectable } from '@nestjs/common';
import { PrismaService } from 'prisma/prisma.service';
import { Prisma } from '@prisma/client';

@Injectable()
export class MoneyTransactionService {
  constructor(
    private readonly prismaService: PrismaService,
  ) { }
  create(args: Prisma.MoneyTransactionLogCreateArgs) {
    return this.prismaService.moneyTransactionLog.create(args)
  }

  findAll(args: Prisma.MoneyTransactionLogFindManyArgs) {
    return this.prismaService.moneyTransactionLog.findMany(args)
  }

  findOne(args: Prisma.MoneyTransactionLogFindFirstArgs) {
    return this.prismaService.moneyTransactionLog.findFirst(args)
  }

  update(id: number, args: Prisma.MoneyTransactionLogUpdateInput) {
    return this.prismaService.moneyTransactionLog.update({ where: { id }, data: args })
  }

  count(args: Prisma.MoneyTransactionLogCountArgs) {
    return this.prismaService.moneyTransactionLog.count(args)
  }
}
