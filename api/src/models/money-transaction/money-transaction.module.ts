import { Module, forwardRef } from '@nestjs/common';
import { MoneyTransactionService } from './money-transaction.service';
import { MoneyTransactionController } from './money-transaction.controller';
import { PrismaModule } from 'prisma/prisma.module';
import { UserModule } from '../user/user.module';
import { NotificationModule } from '../notification-log/notification-log.module';
import { ConfigurationModule } from '../configuration/configuration.module';
import { NodeMailerModule } from 'src/core/node-mailer/node-mailer.module';

@Module({
  imports: [
    PrismaModule,
    forwardRef(() => UserModule),
    forwardRef(() => NotificationModule),
    forwardRef(() => ConfigurationModule),
    forwardRef(() => NodeMailerModule),

  ],
  controllers: [MoneyTransactionController],
  providers: [MoneyTransactionService],
  exports: [MoneyTransactionService]
})
export class MoneyTransactionModule { }
