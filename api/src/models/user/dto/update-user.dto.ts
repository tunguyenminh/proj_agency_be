import { PartialType } from '@nestjs/mapped-types';
import { CreateUserDto } from './create-user.dto';
import { ApiProperty } from '@nestjs/swagger';
import { IsEnum, IsOptional, IsString } from 'class-validator';
import { UserStatus } from '.prisma/client';

export class UpdateUserDto extends PartialType(CreateUserDto) { }
export class UpdateUserStatusDto {
    @ApiProperty({
        example: UserStatus.ACTIVE,
        required: true,
    })
    @IsEnum(UserStatus)
    readonly status?: UserStatus;
}