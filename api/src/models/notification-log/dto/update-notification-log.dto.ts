import { PartialType } from '@nestjs/swagger';
import { CreateAdvertisingAccountRequestDto } from './create-notification-log.dto';

export class UpdateAdvertisingAccountRequestDto extends PartialType(CreateAdvertisingAccountRequestDto) { }
