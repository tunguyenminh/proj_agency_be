import { ApiProperty } from '@nestjs/swagger';
import {
  IsInt,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
  NotContains,
} from 'class-validator';

export class CreateOtpDto {
  @ApiProperty()
  @IsNotEmpty()
  @NotContains(' ')
  phone: string;

  @ApiProperty()
  @IsNotEmpty()
  action: string;
}
