import { ApiProperty } from '@nestjs/swagger';
import { AgencyBalanceLogsType, AgencyServiceType } from '@prisma/client';
import { IsEnum, IsOptional, IsString } from 'class-validator';
import { FilterOptions } from 'src/base/filterOption.dto';

export class FilterAgencyBalanceLogsDto extends FilterOptions {
  // @ApiProperty({
  //   example: 'Lorem ipsum',
  //   required: false,
  // })
  // @IsOptional()
  // @IsString()
  // name: string;

  @ApiProperty({
    example: 'Lorem ipsum',
    required: false,
  })
  @IsString()
  @IsOptional()
  advertisingAccountId: string;

  @ApiProperty({
    example: AgencyBalanceLogsType.DECREASE,
    required: false,
  })
  @IsOptional()
  @IsEnum(AgencyBalanceLogsType)
  type: AgencyBalanceLogsType;

  @ApiProperty({
    example: AgencyServiceType.HONGKONG,
    required: false,
  })
  @IsOptional()
  @IsEnum(AgencyServiceType)
  agencyService: AgencyServiceType;
}
