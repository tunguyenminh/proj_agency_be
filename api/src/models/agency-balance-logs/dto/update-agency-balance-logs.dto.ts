import { PartialType } from '@nestjs/swagger';
import { CreateAgencyBalanceLogsDto } from './create-agency-balance-logs.dto';

export class UpdateAgencyBalanceLogsDto extends PartialType(
  CreateAgencyBalanceLogsDto,
) {}
