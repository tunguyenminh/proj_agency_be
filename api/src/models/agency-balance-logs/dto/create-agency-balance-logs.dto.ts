import { ApiProperty } from '@nestjs/swagger';
import { AgencyServiceType } from '@prisma/client';
import { IsEnum, IsNumber, IsOptional, IsString } from 'class-validator';

export class CreateAgencyBalanceLogsDto {
  @ApiProperty({
    example: 'Lorem ipsum',
    required: false,
  })
  @IsString()
  @IsOptional()
  name: string;

  @ApiProperty({
    example: AgencyServiceType.UNI,
    required: false,
  })
  @IsEnum(AgencyServiceType)
  type: AgencyServiceType;

  @ApiProperty({
    example: 100.3,
    required: false,
  })
  @IsNumber()
  balance: number;
}
