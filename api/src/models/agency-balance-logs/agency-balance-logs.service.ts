import { Injectable } from '@nestjs/common';
import { PrismaService } from 'prisma/prisma.service';
import { Prisma } from '@prisma/client';

@Injectable()
export class AgencyBalanceLogsService {
  constructor(
    private readonly prismaService: PrismaService,
  ) { }
  create(args: Prisma.AgencyBalanceLogsCreateArgs) {
    return this.prismaService.agencyBalanceLogs.create(args)
  }

  findAll(args: Prisma.AgencyBalanceLogsFindManyArgs) {
    return this.prismaService.agencyBalanceLogs.findMany({...args, include: { updatedBy: true, createdBy: true, advertisingAccount: true }})
  }

  findOne(args: Prisma.AgencyBalanceLogsFindFirstArgs) {
    return this.prismaService.agencyBalanceLogs.findFirst({...args, include: { updatedBy: true, createdBy: true, advertisingAccount: true }})
  }

  update(id: number, args: Prisma.AgencyBalanceLogsUpdateInput) {
    return this.prismaService.agencyBalanceLogs.update({ where: { id }, data: args, include: { updatedBy: true, createdBy: true, advertisingAccount: true } })
  }

  count(args: Prisma.AgencyBalanceLogsCountArgs) {
    return this.prismaService.agencyBalanceLogs.count(args)
  }
}
