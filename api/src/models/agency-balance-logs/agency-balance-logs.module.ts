import { Module, forwardRef } from '@nestjs/common';
import { AgencyBalanceLogsService } from './agency-balance-logs.service';
import { AgencyBalanceLogsController } from './agency-balance-logs.controller';
import { PrismaModule } from 'prisma/prisma.module';
import { UserModule } from '../user/user.module';
import { AdvertisingAccountModule } from '../advertising_account/advertising_account.module';
import { NotificationModule } from '../notification-log/notification-log.module';
import { NodeMailerModule } from 'src/core/node-mailer/node-mailer.module';

@Module({
  imports: [
    PrismaModule,
    forwardRef(() => UserModule),
    forwardRef(() => NotificationModule),
    forwardRef(() => AdvertisingAccountModule),
    forwardRef(() => NodeMailerModule)

  ],
  controllers: [AgencyBalanceLogsController],
  providers: [AgencyBalanceLogsService],
  exports: [AgencyBalanceLogsService]
})
export class AgencyBalanceLogsModule { }
