import { Controller, Get, Query, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Prisma, UserRole } from '@prisma/client';
import { Roles } from 'src/auth/decorators/roles.decorator';
import { User as UserDecorator } from 'src/auth/decorators/user.decorator';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { RolesGuard } from 'src/auth/guards/roles.guard';
import { IUserJwt } from 'src/auth/strategies/jwt.strategy';
import { SortOrder } from 'src/constants/enum.constant';
import { funcListPaging } from 'src/helpers/list-paging';
import { AdvertisingAccountService } from '../advertising_account/advertising_account.service';
import { NotificationLogsService } from '../notification-log/notification-log.service';
import { UserService } from '../user/user.service';
import { AgencyBalanceLogsService } from './agency-balance-logs.service';
import { FilterAgencyBalanceLogsDto } from './dto/filter-agency-balance-logs.dto';

@ApiTags('Agency Balance')
@Controller('agency-balance-logs')
export class AgencyBalanceLogsController {
  constructor(
    private readonly agencyBalanceService: AgencyBalanceLogsService,
    private readonly userService: UserService,
    private readonly notificationService: NotificationLogsService,
    private readonly advertisingAccountService: AdvertisingAccountService,
  ) {}
  @ApiBearerAuth()
  @Roles(UserRole.ADMIN, UserRole.EMPLOYEE)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Get()
  async findAll(@UserDecorator() user: IUserJwt, @Query() options: FilterAgencyBalanceLogsDto) {
    let whereInput: Prisma.AgencyBalanceFindManyArgs;

    const sortField = options?.sortField || 'createdAt';
    const sortOrder = options?.sortOrder || SortOrder.DESC;
    let where: Prisma.AgencyBalanceLogsWhereInput = {};
    where.AND = [] as Prisma.AgencyBalanceLogsWhereInput[];

    // if (user.role === UserRole.CUSTOMER) {
    //   where = {
    //     ...where,
    //     advertisingAccount: {
    //       userId: user.data.id,
    //     },
    //   };
    // }

    // if (user.role === UserRole.CUSTOMER) {
    //   where.AND.push({
    //     advertisingAccount: {
    //       userId: user.data.id,
    //     },
    //   });
    // }

    if (options.agencyService) {
      where.AND.push({
        advertisingAccount: {
          agencyService: options.agencyService,
        },
      });
    }

    if (options.textSearch) {
      where.AND.push({
        OR: [
          {
            advertisingAccount: {
              idAccount: { contains: options.textSearch },
            },
          },
        ],
      });
    }

    if (options.type) {
      where = {
        ...where,
        type: options.type,
      };
    }

    whereInput = {
      //@ts-ignore
      where,
      orderBy: {
        [sortField]: sortOrder,
      },
    };

    return await funcListPaging(this.agencyBalanceService, whereInput, options?.page, options?.perPage);
  }
}
