import { Controller, Get, Post, Body, Patch, Param, Delete, UseGuards } from '@nestjs/common';
import { ConfigurationService } from './configuration.service';
import { CreateConfigurationDto } from './dto/create-configuration.dto';
import { ConfigurationUpdateDto, UpdateConfigurationDto } from './dto/update-configuration.dto';
import { CONFIGURATION_CODE_CONSTANT } from 'src/constants/common.constant';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Prisma, UserRole } from '@prisma/client';
import { Roles } from 'src/auth/decorators/roles.decorator';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { RolesGuard } from 'src/auth/guards/roles.guard';
import { BaseException, Errors } from 'src/constants/error.constant';

@ApiTags('Configuration')
@Controller('configuration')
export class ConfigurationController {
  constructor(private readonly configurationService: ConfigurationService) {

  }
  @Get()
  async findAll() {
    return this.configurationService.findAll({})
  }

  @ApiBearerAuth()
  @Roles(UserRole.ADMIN)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Patch()
  async update(
    @Body() body: Array<ConfigurationUpdateDto>
  ) {
    body.forEach((async x => {
      const setting = await this.configurationService.findOne({ where: { settingCode: x.settingCode } })
      if (!setting) throw new BaseException(Errors.BAD_REQUEST("Setting code not found"))
      const _updateInputs: Prisma.ConfigurationUpdateInput = {
        settingValue: x.settingValue,
        settingFee: x?.settingFee,
        type: x?.type,
        unit: x?.unit,
      }
      await this.configurationService.update(setting.id, _updateInputs)

    }))
    const MESSAGE = "UPDATED CONFIGURATION SUCCESSFUL"
    return MESSAGE
  }

  @ApiBearerAuth()
  @Roles(UserRole.ADMIN)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Post()
  async post(
    @Body() body: Array<ConfigurationUpdateDto>
  ) {
    body.forEach((async x => {
      const setting = await this.configurationService.findOne({ where: { settingCode: x.settingCode } })      
      if (setting) throw new BaseException(Errors.BAD_REQUEST("Setting code is existed"))
      const _createConfigurationInput = {
        settingCode: x.settingCode,
        settingValue: x.settingCode,
        settingFee: x.settingFee,
        type: x?.type
        
      }
      await this.configurationService.create({data: _createConfigurationInput})

    }))
    const MESSAGE = "CREATED CONFIGURATION SUCCESSFUL"
    return MESSAGE
  }
}
