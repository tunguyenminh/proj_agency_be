import { Injectable } from '@nestjs/common';
import { CreateConfigurationDto } from './dto/create-configuration.dto';
import { UpdateConfigurationDto } from './dto/update-configuration.dto';
import { PrismaService } from 'prisma/prisma.service';
import { Prisma } from '@prisma/client';

@Injectable()
export class ConfigurationService {
  constructor(private readonly prismaService: PrismaService) { }

  async create(args: Prisma.ConfigurationCreateArgs) {
    return this.prismaService.configuration.create(args);
  }

  async count(args: Prisma.ConfigurationCountArgs) {
    return this.prismaService.configuration.count(args);
  }

  async findAll(args: Prisma.ConfigurationFindManyArgs) {
    return this.prismaService.configuration.findMany(args);
  }

  async findOne(args: Prisma.ConfigurationFindFirstArgs) {
    return this.prismaService.configuration.findFirst(args);
  }

  async update(id: number, args: Prisma.ConfigurationUpdateInput) {
    return this.prismaService.configuration.update({ where: { id }, data: args });
  }

  async remove(args: Prisma.ConfigurationDeleteArgs) {
    return this.prismaService.configuration.delete(args);
  }
  async createMany(args: Prisma.ConfigurationCreateManyArgs) {
    return this.prismaService.configuration.createMany(args);
  }
}
