import { ApiProperty, PartialType } from '@nestjs/swagger';
import { CreateConfigurationDto } from './create-configuration.dto';
import { IsString, IsOptional, IsEnum, IsNumber } from 'class-validator';
import { CONFIGURATION_CODE_CONSTANT, CONFIGURATION_TYPE } from 'src/constants/common.constant';

export class UpdateConfigurationDto extends PartialType(CreateConfigurationDto) { }

export class ConfigurationUpdateDto {
    @ApiProperty({
        example: CONFIGURATION_CODE_CONSTANT.BANK_PHILIPPINES,
        enum: CONFIGURATION_CODE_CONSTANT
    })
    @IsEnum(CONFIGURATION_CODE_CONSTANT)
    @IsOptional()
    readonly settingCode?: CONFIGURATION_CODE_CONSTANT;

    @ApiProperty({
        example: 'lastName',
    })
    @IsString()
    @IsOptional()
    readonly settingValue?: string;

    @ApiProperty({
        example: 'lastName',
    })
    @IsNumber()
    @IsOptional()
    readonly settingFee?: number;

    @ApiProperty({
        example: CONFIGURATION_TYPE.PERCENT,
        enum: CONFIGURATION_TYPE
    })
    @IsEnum(CONFIGURATION_TYPE)
    @IsOptional()
    readonly type?: CONFIGURATION_TYPE;

    @ApiProperty({
        example: 'vnđ',
    })
    @IsString()
    @IsOptional()
    readonly unit?: string;
}