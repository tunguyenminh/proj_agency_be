import { ApiProperty, PartialType } from '@nestjs/swagger';
import { IsString } from 'class-validator';
import { CreateAdvertisingAccountRequestDto } from './create-advertising-account-request';

export class UpdateAdvertisingAccountRequestDto extends PartialType(CreateAdvertisingAccountRequestDto) { }

export class ModifiedAdvertisingAccountRequestDto {
    @ApiProperty({
        example: 'lorem ipsum',
        required: true,
    })
    @IsString()
    note: string
}