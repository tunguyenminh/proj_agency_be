import { ApiProperty } from '@nestjs/swagger';
import {
    AdvertisingAccountStatus,
    AdvertisingAccountType,
    UserStatus,
} from '@prisma/client';
import {
    IsNotEmpty,
    IsEmail,
    IsEnum,
    IsString,
    IsOptional,
    IsNumber,
    IsArray,
    ValidateNested,
} from 'class-validator';
import { TimezoneEnum } from 'src/constants/enum.constant';

export class AdvertisingAccountRequestPageDto {
    @ApiProperty({
        example: 'Edufit@gmail.com',
        required: true,
    })
    @IsNotEmpty()
    @IsString()
    namePage?: string;

    @ApiProperty({
        example: 'Edufit@gmail.com',
        required: true,
    })
    @IsNotEmpty()
    @IsString()
    linkPage?: string;
}
export class CreateAdvertisingAccountRequestDto {
    @ApiProperty({
        example: ['Edufit@gmail.com'],
        required: true,
    })
    @IsNotEmpty()
    @IsArray()
    domain?: string[];

    @ValidateNested()
    @ApiProperty({
        example: [
            {
                namePage: '1',
                linkPage: '2',
            },
            {
                namePage: '1',

                linkPage: '2',
            },
            {
                namePage: '1',

                linkPage: '2',
            },
        ],
        required: true,
    })
    @IsArray()
    @IsOptional()
    page?: Array<AdvertisingAccountRequestPageDto>;

    @ApiProperty({
        example: 'Edufit@gmail.com',
        required: true,
    })
    @IsNotEmpty()
    @IsEnum(AdvertisingAccountType)
    readonly type?: AdvertisingAccountType;

    @ApiProperty({
        example: 1,
        required: true,
    })
    @IsNumber()
    @IsNotEmpty()
    readonly numberOfAccount: number;

    @ApiProperty({
        example: 1,
        required: true,
    })
    @IsNumber()
    @IsNotEmpty()
    readonly balancePerAccount: number;

    @ApiProperty({
        example: 1,
        required: true,
    })
    @IsEnum(TimezoneEnum)
    readonly timezone: TimezoneEnum;

    @ApiProperty({
        example: 'lorem ipsum',
        required: false,
    })
    @IsString()
    @IsOptional()
    note?: string
}
