import { ApiProperty } from "@nestjs/swagger";
import { AdvertisingAccountRequestStatus, AdvertisingAccountType } from "@prisma/client";
import { Transform } from "class-transformer";
import { IsEnum, IsOptional, IsString } from "class-validator";
import { FilterOptions } from "src/base/filterOption.dto";

export class AdvertisingAccountRequestFilterDto extends FilterOptions {


    @ApiProperty({
        example: AdvertisingAccountType.FACEBOOK,
        required: false,
    })
    @IsOptional()
    @IsEnum(AdvertisingAccountType)
    readonly type?: AdvertisingAccountType;

    @ApiProperty({
        example: [
           AdvertisingAccountRequestStatus.ACCEPTED,
           AdvertisingAccountRequestStatus.PENDING,
           AdvertisingAccountRequestStatus.DELETED,
           AdvertisingAccountRequestStatus.REJECTED,
           AdvertisingAccountRequestStatus.SUBMITTED,
        ],
        required: false,
    })
    @IsOptional()
    @IsString({each: true})
    @Transform(({value}) => value? value.split(',') : null)

    readonly status?: AdvertisingAccountRequestStatus[];
}
