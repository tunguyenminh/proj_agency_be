import {
    Body, Controller, Delete, Get, Param, Patch, Post, Query, UseGuards
} from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import {
  AccountBalanceTransactionStatus,
  AccountBalanceTransactionType,
    AdvertisingAccountRequestStatus,
    AdvertisingAccountStatus,
    AdvertisingAccountType,
    Prisma, UserRole,
    UserStatus
} from '@prisma/client';
import { Roles } from 'src/auth/decorators/roles.decorator';
import { User as UserDecorator } from 'src/auth/decorators/user.decorator';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { RolesGuard } from 'src/auth/guards/roles.guard';
import { IUserJwt } from 'src/auth/strategies/jwt.strategy';
import { CONFIGURATION_CODE_CONSTANT, NotificationTitle, NotificationType, NotificationUserTitle } from 'src/constants/common.constant';
import { SortOrder } from 'src/constants/enum.constant';
import { BaseException, Errors } from 'src/constants/error.constant';
import { funcListPaging } from 'src/helpers/list-paging';
import { ConfigurationService } from '../configuration/configuration.service';
import { NotificationLogsService } from '../notification-log/notification-log.service';
import { UserService } from '../user/user.service';
import { AdvertisingAccountRequestService } from './advertising-account-request.service';
import { AdvertisingAccountRequestFilterDto } from './dto/advertising-account-request-filter.dto';
import { CreateAdvertisingAccountRequestDto } from './dto/create-advertising-account-request';
import { ModifiedAdvertisingAccountRequestDto, UpdateAdvertisingAccountRequestDto } from './dto/update-advertising-account-request.dto';
import { AccountBalanceTransactionService } from '../account-balance-transaction/account-balance-transaction.service';

@ApiTags('Advertising Account Request')
@Controller('advertising-account-request')
export class AdvertisingAccountRequestController {
  constructor(
    private readonly advertisingAccountRequestService: AdvertisingAccountRequestService,
    private readonly userService: UserService,
    private readonly configurationService: ConfigurationService,
    private readonly notificationService: NotificationLogsService,
    private readonly accountBalanceTransactionService: AccountBalanceTransactionService,

  ) { }

  @ApiBearerAuth()
  @Roles(UserRole.ADMIN, UserRole.CUSTOMER, UserRole.EMPLOYEE)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Get()
  async findAll(
    @UserDecorator() user: IUserJwt,
    @Query() options: AdvertisingAccountRequestFilterDto
  ) {
    let whereInput: Prisma.AdvertisingAccountRequestFindManyArgs;

    const sortField = options?.sortField || 'createdAt';
    const sortOrder = options?.sortOrder || SortOrder.DESC;
    let where: Prisma.AdvertisingAccountRequestWhereInput = {};
    where.AND = [
      { NOT: { status: AdvertisingAccountRequestStatus.DELETED } },
    ];

    if (user.role === UserRole.CUSTOMER) {
      where = {
        ...where,
        userId: user.data.id
      }
    }

    if (options.textSearch) {
      // @ts-ignore
      where.AND.push({
        OR: [
          { user: { lastName: { contains: options.textSearch } } },
          { user: { firstName: { contains: options.textSearch } } },
          { user: { email: { contains: options.textSearch } } },
          {
            AdvertisingAccountRequestDomain: {
              some: { domain: { contains: options.textSearch } }
            }
          },
          {
            AdvertisingAccountRequestPage: {
              some: { linkPage: options.textSearch }
            }
          },
          {
            AdvertisingAccountRequestPage: {
              some: { namePage: options.textSearch }
            }
          },
        ],
      });
    }

    if (options.status) {
      where = {
        ...where,
        status: { in: options.status },
      }
    }

    if (options.type) {
      where = {
        ...where,
        type: options.type,
      };
    }

    whereInput = {
      where,
      orderBy: {
        [sortField]: sortOrder,
      },
      include: { AdvertisingAccountRequestPage: true, AdvertisingAccountRequestDomain: true, user: true, updatedBy: true }
    };

    return await funcListPaging(
      this.advertisingAccountRequestService,
      whereInput,
      options?.page,
      options?.perPage,
    );
  }

  @ApiBearerAuth()
  @Roles(UserRole.ADMIN, UserRole.CUSTOMER, UserRole.EMPLOYEE)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Get(':id')
  async findOne(@Param('id') id: number) {
    return this.advertisingAccountRequestService.findOne({ where: { id } });
  }

  @ApiBearerAuth()
  @Roles(UserRole.CUSTOMER, UserRole.EMPLOYEE)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Post()
  async create(
    @UserDecorator() user: IUserJwt,
    @Body() body: CreateAdvertisingAccountRequestDto,
  ) {
    const userFound = await this.userService.findOne({
      where: { id: user.data.id, status: { not: UserStatus.DELETED } },
    });
    if (!userFound)
      throw new BaseException(Errors.BAD_REQUEST('User not found'));
    if (userFound.status === UserStatus.BANNED)
      throw new BaseException(Errors.BAD_REQUEST('User has been banned'));
    // get fee by request created account type 
    let settingCode = CONFIGURATION_CODE_CONSTANT.CREATE_ACCOUNT_ADS_FEE
    if (body.type === AdvertisingAccountType.GOOGLE) {
      settingCode = CONFIGURATION_CODE_CONSTANT.CREATE_ACCOUNT_ADS_FEE_GOOGLE
    }
    const configuration = await this.configurationService.findOne({ where: { settingCode } })
    if (!configuration) throw new BaseException(Errors.BAD_REQUEST("Type is required"))
    const CREATE_ACCOUNT_ADS_FEE = Number(configuration.settingValue)
    const TOP_UP_FEE_PER_ACCOUNT = body.balancePerAccount + body.balancePerAccount * Number(userFound?.depositAdsFee) / 100;
    const totalFee = body.numberOfAccount * (TOP_UP_FEE_PER_ACCOUNT + CREATE_ACCOUNT_ADS_FEE)

    if (totalFee > userFound.balance) throw new BaseException(Errors.BAD_REQUEST("Account balance is not enough"))
    let createArgs: Prisma.AdvertisingAccountRequestUncheckedCreateInput = {
      numberOfAccount: body.numberOfAccount,
      userId: user.data.id,
      balancePerAccount: body.balancePerAccount,
      type: body.type,
      timezone: body.timezone,
      createdFee: totalFee,
      currentlyCreatedFee: CREATE_ACCOUNT_ADS_FEE // each account
    };

    if (body.page && body.page.length > 0) {
      createArgs = {
        ...createArgs,
        AdvertisingAccountRequestPage: {
          createMany: {
            data: body.page.map((x) => { return { namePage: x.namePage, linkPage: x.linkPage } })
          }
        }
      }
    }

    if (body.domain && body.domain.length > 0) {
      createArgs = {
        ...createArgs,
        AdvertisingAccountRequestDomain: {
          createMany: {
            data: body.domain.map((x) => { return { domain: x } })
          }
        }
      }
    }

    await this.userService.update(userFound.id, { balance: { decrement: totalFee } })
    const advertisingAccountRequest = await this.advertisingAccountRequestService.create({ data: createArgs });
    const notification = await this.notificationService.create({ data: { title: NotificationTitle.NEW_ACCOUNT_REQUEST, advertisingAccountRequestId: advertisingAccountRequest.id, type: NotificationType.ACCOUNT_REQUEST } })
    
    await this.accountBalanceTransactionService.create({
      data: {
        type: AccountBalanceTransactionType.ACCOUNT_REQUEST,
        amount: advertisingAccountRequest.balancePerAccount * advertisingAccountRequest.numberOfAccount,
        advertisingAccountRequestId: advertisingAccountRequest.id,
        NotificationLogs: {connect: {id: notification.id}},
        totalCost: advertisingAccountRequest.createdFee,
        userId: user.data.id
      }
    })
    return advertisingAccountRequest
  }

  @ApiBearerAuth()
  @Roles(UserRole.CUSTOMER)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Patch(':id')
  async update(
    @Param('id') id: number,
    @UserDecorator() user: IUserJwt,
    @Body() body: UpdateAdvertisingAccountRequestDto,
  ) {
    const {domain, page, ...remainBody} = body
    let updateArgs: Prisma.AdvertisingAccountRequestUncheckedUpdateInput = {
      ...remainBody,
      updatedById: user.data.id,
      // updatedAt: new Date()
    };

    if (page && page.length > 0) {
      updateArgs = {
        ...updateArgs,
        AdvertisingAccountRequestPage: {
          deleteMany: {},
          createMany: {
            data: page.map((x) => { return { namePage: x.namePage, linkPage: x.linkPage } })
          }
        }
      }
    }
    if (domain && domain.length > 0) {
      updateArgs = {
        ...updateArgs,
        AdvertisingAccountRequestDomain: {
          deleteMany: {},
          createMany: {
            data: domain.map((x) => { return { domain: x } })
          },
        }
      }
    }

    const adsAccountRequest = await this.advertisingAccountRequestService.findOne({where: {id}})
    if (!adsAccountRequest) throw new BaseException(Errors.BAD_REQUEST("Advertising Account Request Not Found"))
    if (adsAccountRequest.status !== AdvertisingAccountRequestStatus.MODIFIED) throw new BaseException(Errors.BAD_REQUEST("Unable to Modify Advertising Account Request"))
    const accountBalanceTransactionIns = await this.accountBalanceTransactionService.findOne({where: {advertisingAccountRequestId: id}});
    if (!accountBalanceTransactionIns) throw new BaseException(Errors.BAD_REQUEST("Request Not Found"))

    const userRequest = adsAccountRequest.userId;
    const userFound = await this.userService.findOne({ where: {id: userRequest }})
    
    let settingCode = CONFIGURATION_CODE_CONSTANT.CREATE_ACCOUNT_ADS_FEE
    if (body.type === AdvertisingAccountType.GOOGLE) {
      settingCode = CONFIGURATION_CODE_CONSTANT.CREATE_ACCOUNT_ADS_FEE_GOOGLE
    }
    const configuration = await this.configurationService.findOne({ where: { settingCode } })
    if (!configuration) throw new BaseException(Errors.BAD_REQUEST("Type is required"))

    const CREATE_ACCOUNT_ADS_FEE = Number(configuration.settingValue)
    const TOP_UP_FEE_PER_ACCOUNT = body.balancePerAccount + body.balancePerAccount * Number(userFound?.depositAdsFee) / 100;
    const totalFee = body.numberOfAccount * (TOP_UP_FEE_PER_ACCOUNT + CREATE_ACCOUNT_ADS_FEE)

    const currentBalance = userFound.balance + adsAccountRequest.createdFee;
    if (currentBalance < totalFee) throw new BaseException(Errors.BAD_REQUEST("Insufficient Balance"))

    // update new Fee
    updateArgs.createdFee = totalFee;
    updateArgs.currentlyCreatedFee= CREATE_ACCOUNT_ADS_FEE
    
    // update user balance
    await this.userService.update(userRequest, {balance: { increment: adsAccountRequest.createdFee }})
    await this.userService.update(userRequest, {balance: { decrement: totalFee }})

    if (adsAccountRequest.status === AdvertisingAccountRequestStatus.MODIFIED) {
      updateArgs.status = AdvertisingAccountRequestStatus.PENDING
    }
    await this.accountBalanceTransactionService.update(accountBalanceTransactionIns.id, {amount: body.balancePerAccount * body.numberOfAccount, totalCost: totalFee, updatedById: user.data.id})
    await this.notificationService.create({ data: { title: NotificationTitle.NEW_ACCOUNT_REQUEST, advertisingAccountRequestId: adsAccountRequest.id, type: NotificationType.ACCOUNT_REQUEST } })
    // return this.advertisingAccountRequestService.update(id, { ...updateArgs, user: { connect: { id: userRequest } } });
    return this.advertisingAccountRequestService.update(id, { ...updateArgs });
  }

  @ApiBearerAuth()
  @Roles(UserRole.ADMIN, UserRole.EMPLOYEE)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Patch(':id/rejected')
  async adminRejected(
    @Param('id') id: number,
    @UserDecorator() user: IUserJwt,
    @Body() body: UpdateAdvertisingAccountRequestDto,
  ) {
    const advertisingAccountRequest = await this.advertisingAccountRequestService.findOne({ where: { id, status: AdvertisingAccountRequestStatus.PENDING } })
    if (!advertisingAccountRequest)
      throw new BaseException(Errors.BAD_REQUEST('Request not found'));
    const userFound = await this.userService.findOne({ where: { id: advertisingAccountRequest.userId } })
    if (!userFound)
      throw new BaseException(Errors.BAD_REQUEST('User not found'));
    // add balance to user wallet when rejected
    await this.userService.update(userFound.id, { balance: { increment: advertisingAccountRequest.createdFee } })
    await this.accountBalanceTransactionService.create({
      data: {
        type: AccountBalanceTransactionType.DENINED_ACCOUNT_REQUEST,
        status: AccountBalanceTransactionStatus.ADDED,
        totalCost: advertisingAccountRequest.createdFee,
        advertisingAccountRequestId: id,
        updatedById: user.data.id,
        userId: userFound.id,
        amount: advertisingAccountRequest.createdFee
      }
    })
    await this.notificationService.create({
      data: {
        title: NotificationUserTitle.NEW_ACCOUNT_REQUEST_REJECTED,
        type: NotificationType.ACCOUNT_REQUEST,
        advertisingAccountRequestId: id,
        targetUserId: userFound.id,
      }
    })
    return this.advertisingAccountRequestService.update(id, { status: AdvertisingAccountRequestStatus.REJECTED, note: body.note, updatedBy: { connect: { id: user.data.id }}});
  }

  @ApiBearerAuth()
  @Roles(UserRole.ADMIN, UserRole.EMPLOYEE)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Patch(':id/modified')
  async adminModified(
    @Param('id') id: number,
    @UserDecorator() user: IUserJwt,
    @Body() body: ModifiedAdvertisingAccountRequestDto,
  ) {
    const advertisingAccountRequest = await this.advertisingAccountRequestService.findOne({ where: { id, status: AdvertisingAccountRequestStatus.PENDING } })
    if (!advertisingAccountRequest) throw new BaseException(Errors.BAD_REQUEST('Request not found'));
    const userFound = await this.userService.findOne({ where: { id: advertisingAccountRequest.userId } })
    if (!userFound) throw new BaseException(Errors.BAD_REQUEST('User not found'));
    // Keep balance to user wallet when modified
    // await this.userService.update(userFound.id, { balance: { increment: advertisingAccountRequest.createdFee } })
    return this.advertisingAccountRequestService.update(id, { status: AdvertisingAccountRequestStatus.MODIFIED, note: body.note, updatedBy: { connect: { id: user.data.id }}});
  }

  @ApiBearerAuth()
  @Roles(UserRole.ADMIN, UserRole.CUSTOMER, UserRole.EMPLOYEE)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Delete(':id')
  async delete(@Param('id') id: number, @UserDecorator() user: IUserJwt) {
    return this.advertisingAccountRequestService.update(id, {
      status: AdvertisingAccountStatus.DELETED,
      updatedBy: { connect: { id: user.data.id } },
    });
  }
}
