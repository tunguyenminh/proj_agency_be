import { Module, forwardRef } from '@nestjs/common';
import { AdvertisingAccountRequestService } from './advertising-account-request.service';
import { AdvertisingAccountRequestController } from './advertising-account-request.controller';
import { PrismaModule } from 'prisma/prisma.module';
import { UserModule } from '../user/user.module';
import { ConfigurationModule } from '../configuration/configuration.module';
import { NotificationModule } from '../notification-log/notification-log.module';
import { AccountBalanceTransactionModule } from '../account-balance-transaction/account-balance-transaction.module';

@Module({
  imports: [
    PrismaModule,
    forwardRef(() => UserModule),
    forwardRef(() => ConfigurationModule),
    forwardRef(() => NotificationModule),
    forwardRef(() => AccountBalanceTransactionModule),
  ],
  controllers: [AdvertisingAccountRequestController],
  providers: [AdvertisingAccountRequestService],
  exports: [AdvertisingAccountRequestService]
})
export class AdvertisingAccountRequestModule { }
