import { Injectable } from '@nestjs/common';
import { PrismaService } from 'prisma/prisma.service';
import { Prisma } from '@prisma/client';

@Injectable()
export class AdvertisingAccountRequestService {
  constructor(
    private readonly prismaService: PrismaService,
  ) { }
  create(args: Prisma.AdvertisingAccountRequestCreateArgs) {
    return this.prismaService.advertisingAccountRequest.create(args)
  }

  findAll(args: Prisma.AdvertisingAccountRequestFindManyArgs) {
    return this.prismaService.advertisingAccountRequest.findMany(args)
  }

  findOne(args: Prisma.AdvertisingAccountRequestFindFirstArgs) {
    return this.prismaService.advertisingAccountRequest.findFirst(args)
  }

  update(id: number, args: Prisma.AdvertisingAccountRequestUpdateInput) {
    return this.prismaService.advertisingAccountRequest.update({ where: { id }, data: args })
  }

  count(args: Prisma.AdvertisingAccountRequestCountArgs) {
    return this.prismaService.advertisingAccountRequest.count(args)
  }
}
