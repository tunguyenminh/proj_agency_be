import { ApiProperty } from '@nestjs/swagger';
import { AgencyServiceType } from '@prisma/client';
import { IsEnum, IsNumber, IsOptional, IsString } from 'class-validator';

export class CreateAgencyBalanceDto {
  @ApiProperty({
    example: 'Lorem ipsum',
    required: false,
  })
  @IsString()
  @IsOptional()
  name: string;

  @ApiProperty({
    example: AgencyServiceType.UNI,
    required: false,
  })
  @IsEnum(AgencyServiceType)
  type: AgencyServiceType;

  @ApiProperty({
    example: 100.3,
    required: false,
  })
  @IsNumber()
  balance: number;

  @IsOptional()
  createdById: number
}

export class TopupAgencyBalanceDto {
  @ApiProperty({
    example: 'Lorem ipsum',
    required: true,
  })
  @IsNumber()
  amount: number;

  @ApiProperty({
    example: 1,
    required: true,
  })
  @IsString()
  adAccountId: string;

  @ApiProperty({
    example: AgencyServiceType.HONGKONG,
    required: true,
  })
  @IsEnum(AgencyServiceType)
  type: AgencyServiceType;
}