import { ApiProperty } from '@nestjs/swagger';
import { AgencyServiceType } from '@prisma/client';
import { IsEnum, IsOptional, IsString } from 'class-validator';
import { FilterOptions } from 'src/base/filterOption.dto';

export class FilterAgencyBalanceDto extends FilterOptions {
  @ApiProperty({
    example: 'Lorem ipsum',
    required: false,
  })
  @IsOptional()
  @IsString()
  name: string;

  @ApiProperty({
    example: AgencyServiceType.UNI,
    required: false,
  })
  @IsOptional()
  @IsEnum(AgencyServiceType)
  type: AgencyServiceType;
}

export class FilterUniTracking extends FilterOptions {
  @ApiProperty({
    example: ['30211959751976', 302109855435922],
    required: false,
  })
  @IsOptional()
  taskIds?: Array<number | string>;
}
