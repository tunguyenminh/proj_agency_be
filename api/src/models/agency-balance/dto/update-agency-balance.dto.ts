import { PickType } from '@nestjs/swagger';
import { CreateAgencyBalanceDto } from './create-agency-balance.dto';

export class UpdateAgencyBalanceDto extends PickType(CreateAgencyBalanceDto, ['balance'] as const) {}
