import { Body, Controller, Get, Param, Patch, Post, Query, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { AgencyBalanceLogsType, Prisma, UserRole } from '@prisma/client';
import { Roles } from 'src/auth/decorators/roles.decorator';
import { User as UserDecorator } from 'src/auth/decorators/user.decorator';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { RolesGuard } from 'src/auth/guards/roles.guard';
import { IUserJwt } from 'src/auth/strategies/jwt.strategy';
import { SortOrder } from 'src/constants/enum.constant';
import { BaseException, Errors } from 'src/constants/error.constant';
import { funcListPaging } from 'src/helpers/list-paging';
import { AdvertisingAccountService } from '../advertising_account/advertising_account.service';
import { NotificationLogsService } from '../notification-log/notification-log.service';
import { UserService } from '../user/user.service';
import { AgencyBalanceService } from './agency-balance.service';
import { CreateAgencyBalanceDto, TopupAgencyBalanceDto } from './dto/create-agency-balance.dto';
import { FilterAgencyBalanceDto, FilterUniTracking } from './dto/filter-agency-balance.dto';
import { AgencyBalanceLogsService } from '../agency-balance-logs/agency-balance-logs.service';
import { NotificationTitle, NotificationType } from 'src/constants/common.constant';
import { UpdateAgencyBalanceDto } from './dto/update-agency-balance.dto';
import { AuthGuard } from '@nestjs/passport';
import { UniService } from 'src/services/uni/uni.service';

@ApiTags('Agency Balance')
@Controller('agency-balance')
export class AgencyBalanceController {
  constructor(
    private readonly agencyBalanceService: AgencyBalanceService,
    private readonly userService: UserService,
    private readonly notificationService: NotificationLogsService,
    private readonly advertisingAccountService: AdvertisingAccountService,
    private readonly agencyBalanceLogsService: AgencyBalanceLogsService,
    private readonly uniService: UniService,
  ) {}

  @ApiBearerAuth()
  @Roles(UserRole.ADMIN, UserRole.EMPLOYEE)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Get()
  async findAll(@UserDecorator() user: IUserJwt, @Query() options: FilterAgencyBalanceDto) {
    let whereInput: Prisma.AgencyBalanceFindManyArgs;

    const sortField = options?.sortField || 'createdAt';
    const sortOrder = options?.sortOrder || SortOrder.DESC;
    let where: Prisma.AgencyBalanceWhereInput = {};
    where.AND = [] as Prisma.AgencyBalanceWhereInput[];

    // if (user.role === UserRole.CUSTOMER) {
    //   where = {
    //     ...where,
    //     advertisingAccount: {
    //       userId: user.data.id,
    //     },
    //   };
    // }

    // if (user.role === UserRole.CUSTOMER) {
    //   where.AND.push({
    //     advertisingAccount: {
    //       userId: user.data.id,
    //     },
    //   });
    // }

    if (options.textSearch) {
      where.AND.push({
        OR: [
          {
            name: { contains: options.textSearch },
          },
        ],
      });
    }

    if (options.type) {
      where = {
        ...where,
        type: options.type,
      };
    }

    whereInput = {
      where,
      orderBy: {
        [sortField]: sortOrder,
      },
    };

    return await funcListPaging(this.agencyBalanceService, whereInput, options?.page, options?.perPage);
  }

  @ApiBearerAuth()
  @Roles(UserRole.ADMIN, UserRole.EMPLOYEE)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Get(':id')
  async findOne(@Param('id') id: number) {
    return this.agencyBalanceService.findOne({
      where: { id },
      include: { updatedBy: true, createdBy: true },
    });
  }

  @ApiBearerAuth()
  @Roles(UserRole.ADMIN, UserRole.EMPLOYEE)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Patch(':id')
  async patch(@Param('id') id: number, @Body() body: UpdateAgencyBalanceDto, @UserDecorator() user: IUserJwt) {
    const agencyBalanceWallet = await this.agencyBalanceService.findOne({ where: { id } });
    if (!agencyBalanceWallet) {
      throw new BaseException(Errors.BAD_REQUEST(`The wallet with the ID ${id} does not exist`));
    }
    if (body.balance < 0) {
      throw new BaseException(Errors.BAD_REQUEST(`Balance must be greater than 0`));
    }
    const data: Prisma.AgencyBalanceUncheckedUpdateInput = {
      ...body,
      updatedById: user.data.id,
    };
    return this.agencyBalanceService.update(id, data);
  }

  @ApiBearerAuth()
  @Roles(UserRole.ADMIN, UserRole.EMPLOYEE)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Post()
  async create(@Body() body: CreateAgencyBalanceDto, @UserDecorator() user: IUserJwt) {
    const isExisted = await this.agencyBalanceService.findOne({
      where: { type: body.type },
    });

    if (isExisted) {
      throw new BaseException(Errors.BAD_REQUEST(`Wallet type ${body.type} is existed`));
    }

    if (body.balance < 0) {
      throw new BaseException(Errors.BAD_REQUEST(`Balance must be greater than 0`));
    }

    const data: CreateAgencyBalanceDto = body;
    if (!body.name) {
      body.name = body.type;
      body.createdById = user.data.id;
    }

    const agencyWalletResult = await this.agencyBalanceService.create({ data });
    // Notification to Admin
    await this.notificationService.create({
      data: {
        title: NotificationTitle.NEW_AGENCY_WALLET,
        type: NotificationType.AGENCY_WALLET,
      },
    });

    return agencyWalletResult;
  }

  @ApiBearerAuth()
  @Roles(UserRole.ADMIN, UserRole.EMPLOYEE)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Post('topup')
  async topup(@Body() body: TopupAgencyBalanceDto, @UserDecorator() user: IUserJwt) {
    const adAccountId = await this.advertisingAccountService.findOne({
      where: { idAccount: body.adAccountId },
    });
    if (!adAccountId) {
      throw new BaseException(Errors.BAD_REQUEST(`Ads account id has been deleted or refunded`));
    }

    const agencyWallet = await this.agencyBalanceService.findOne({
      where: { type: body.type },
    });
    if (!agencyWallet) {
      throw new BaseException(Errors.BAD_REQUEST(`Wallet ${body.type} not founded`));
    }
    if (body.amount > agencyWallet.balance) {
      throw new BaseException(Errors.BAD_REQUEST(`The ${body.type} wallet does not have sufficient balance to top up.`));
    }
    const agencyBalanceBeforeChange = agencyWallet.balance;
    const agencyBalanceResult = await this.agencyBalanceService.update(agencyWallet.id, {
      balance: { decrement: body.amount },
      updatedBy: { connect: { id: user.data.id } },
    });
    const notificationLogs = await this.notificationService.create({
      data: {
        title: NotificationTitle.CHANGE_AGENCY_WALLET,
        type: NotificationType.AGENCY_WALLET_BALANCE_CHANGED,
        agencyBalanceId: agencyBalanceResult.id,
      },
    });
    const agencyBalanceLogsInput: Prisma.AgencyBalanceLogsCreateInput = {
      amount: body.amount,
      type: AgencyBalanceLogsType.DECREASE,
      advertisingAccount: { connect: { id: adAccountId.id } },
      createdBy: { connect: { id: user.data.id } },
      NotificationLogs: { connect: { id: notificationLogs.id } },
      agencyWalletBalanceBefore: agencyBalanceBeforeChange,
      agencyWalletBalanceAfter: agencyBalanceBeforeChange - body.amount,
    };
    await this.agencyBalanceLogsService.create({
      data: agencyBalanceLogsInput,
    });
    return agencyBalanceResult;
  }

  @ApiBearerAuth()
  @Roles(UserRole.ADMIN, UserRole.EMPLOYEE)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Post('tracking-uni')
  async trackingUni(@Body() body: FilterUniTracking, @UserDecorator() user: IUserJwt) {
    const { page = 1, perPage = 10, taskIds = [] } = body;
    const bodyRequest: any = {
      pageNumber: page,
      pageSize: perPage,
    };
    if (taskIds && taskIds.length > 0) {
      bodyRequest.taskIds = taskIds;
    }
    const result = await this.uniService.queryManageApplication(bodyRequest, {});
    return result.data?.data;
  }
}
