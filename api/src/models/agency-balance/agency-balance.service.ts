import { Injectable } from '@nestjs/common';
import { PrismaService } from 'prisma/prisma.service';
import { Prisma } from '@prisma/client';

@Injectable()
export class AgencyBalanceService {
  constructor(
    private readonly prismaService: PrismaService,
  ) { }
  create(args: Prisma.AgencyBalanceCreateArgs) {
    return this.prismaService.agencyBalance.create(args)
  }

  findAll(args: Prisma.AgencyBalanceFindManyArgs) {
    return this.prismaService.agencyBalance.findMany({...args, include: { updatedBy: true, createdBy: true }})
  }

  findOne(args: Prisma.AgencyBalanceFindFirstArgs) {
    return this.prismaService.agencyBalance.findFirst({...args, include: { updatedBy: true, createdBy: true }})
  }

  update(id: number, args: Prisma.AgencyBalanceUpdateInput) {
    return this.prismaService.agencyBalance.update({ where: { id }, data: args })
  }

  count(args: Prisma.AgencyBalanceCountArgs) {
    return this.prismaService.agencyBalance.count(args)
  }
}
