import { Module, forwardRef } from '@nestjs/common';
import { AgencyBalanceService } from './agency-balance.service';
import { AgencyBalanceController } from './agency-balance.controller';
import { PrismaModule } from 'prisma/prisma.module';
import { UserModule } from '../user/user.module';
import { AdvertisingAccountModule } from '../advertising_account/advertising_account.module';
import { NotificationModule } from '../notification-log/notification-log.module';
import { NodeMailerModule } from 'src/core/node-mailer/node-mailer.module';
import { AgencyBalanceLogsModule } from '../agency-balance-logs/agency-balance-logs.module';

@Module({
  imports: [
    PrismaModule,
    forwardRef(() => UserModule),
    forwardRef(() => NotificationModule),
    forwardRef(() => AdvertisingAccountModule),
    forwardRef(() => NodeMailerModule),
    forwardRef(() => AgencyBalanceLogsModule)

  ],
  controllers: [AgencyBalanceController],
  providers: [AgencyBalanceService],
  exports: [AgencyBalanceService]
})
export class AgencyBalanceModule { }
