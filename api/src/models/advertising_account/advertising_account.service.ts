import { Injectable } from '@nestjs/common';
import { PrismaService } from 'prisma/prisma.service';
import { AgencyServiceType, Prisma } from '@prisma/client';
import { BaseException, Errors } from 'src/constants/error.constant';

@Injectable()
export class AdvertisingAccountService {
  constructor(
    private readonly prismaService: PrismaService,
  ) { }
  create(args: Prisma.AdvertisingAccountCreateArgs) {
    // C1: Tạo tài khoản quảng cáo
    switch (args.data.agencyService) {
      case AgencyServiceType.HONGKONG:
        break;
      case AgencyServiceType.UNI:
        // create
        const createUni = true
        if (!createUni) throw new BaseException(Errors.BAD_REQUEST('Something went wrong create uni account'));
        break;
      case AgencyServiceType.VN:
      default:
        break;
    }
    return this.prismaService.advertisingAccount.create(args)
  }

  createMany(args: Prisma.AdvertisingAccountCreateManyArgs) {
    return this.prismaService.advertisingAccount.createMany(args)
  }

  findAll(args: Prisma.AdvertisingAccountFindManyArgs) {
    return this.prismaService.advertisingAccount.findMany(args)
  }

  findOne(args: Prisma.AdvertisingAccountFindFirstArgs) {
    return this.prismaService.advertisingAccount.findFirst(args)
  }

  update(id: number, args: Prisma.AdvertisingAccountUpdateInput) {
    return this.prismaService.advertisingAccount.update({ where: { id }, data: args })
  }

  count(args: Prisma.AdvertisingAccountCountArgs) {
    return this.prismaService.advertisingAccount.count(args)
  }
  updateMany(args: Prisma.AdvertisingAccountWhereInput, data: Prisma.AdvertisingAccountUpdateInput) {
    return this.prismaService.advertisingAccount.updateMany({ where: args, data })

  }
}
