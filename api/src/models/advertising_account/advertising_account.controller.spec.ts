import { Test, TestingModule } from '@nestjs/testing';
import { AdvertisingAccountController } from './advertising_account.controller';
import { AdvertisingAccountService } from './advertising_account.service';

describe('AdvertisingAccountController', () => {
  let controller: AdvertisingAccountController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AdvertisingAccountController],
      providers: [AdvertisingAccountService],
    }).compile();

    controller = module.get<AdvertisingAccountController>(AdvertisingAccountController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
