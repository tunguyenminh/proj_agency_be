import { Module, forwardRef } from '@nestjs/common';
import { AdvertisingAccountService } from './advertising_account.service';
import { AdvertisingAccountController } from './advertising_account.controller';
import { PrismaModule } from 'prisma/prisma.module';
import { UserModule } from '../user/user.module';
import { AdvertisingAccountRequestModule } from '../advertising-account-request/advertising-account-request.module';
import { NotificationModule } from '../notification-log/notification-log.module';
import { NodeMailerModule } from 'src/core/node-mailer/node-mailer.module';
import { AgencyBalanceModule } from '../agency-balance/agency-balance.module';
import { AgencyBalanceLogsModule } from '../agency-balance-logs/agency-balance-logs.module';
import { ConfigurationModule } from '../configuration/configuration.module';
import { AccountBalanceTransactionModule } from '../account-balance-transaction/account-balance-transaction.module';

@Module({
  imports: [
    PrismaModule,
    forwardRef(() => UserModule),
    forwardRef(() => AdvertisingAccountRequestModule),
    forwardRef(() => NotificationModule),
    forwardRef(() => NodeMailerModule),
    forwardRef(() => AgencyBalanceModule),
    forwardRef(() => AgencyBalanceLogsModule),
    forwardRef(() => ConfigurationModule),
    forwardRef(() => AccountBalanceTransactionModule),
  ],
  controllers: [AdvertisingAccountController],
  providers: [AdvertisingAccountService],
  exports: [AdvertisingAccountService]
})
export class AdvertisingAccountModule { }
