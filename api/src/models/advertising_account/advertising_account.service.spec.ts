import { Test, TestingModule } from '@nestjs/testing';
import { AdvertisingAccountService } from './advertising_account.service';

describe('AdvertisingAccountService', () => {
  let service: AdvertisingAccountService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [AdvertisingAccountService],
    }).compile();

    service = module.get<AdvertisingAccountService>(AdvertisingAccountService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
