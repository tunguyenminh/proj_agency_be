import { Body, Controller, Delete, Get, Param, Patch, Post, Query, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import {
  AccountBalanceTransactionStatus,
  AccountBalanceTransactionType,
  AdvertisingAccountRequestStatus,
  AdvertisingAccountStatus,
  AdvertisingAccountType,
  AgencyBalanceLogsType,
  Prisma,
  UserRole,
  UserStatus,
} from '@prisma/client';
import { Roles } from 'src/auth/decorators/roles.decorator';
import { User as UserDecorator } from 'src/auth/decorators/user.decorator';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { RolesGuard } from 'src/auth/guards/roles.guard';
import { IUserJwt } from 'src/auth/strategies/jwt.strategy';
import { SortOrder } from 'src/constants/enum.constant';
import { BaseException, Errors } from 'src/constants/error.constant';
import { NodeMailerService } from 'src/core/node-mailer/node-mailer.service';
import { funcListPaging } from 'src/helpers/list-paging';
import { AdvertisingAccountRequestService } from '../advertising-account-request/advertising-account-request.service';
import { AgencyBalanceLogsService } from '../agency-balance-logs/agency-balance-logs.service';
import { AgencyBalanceService } from '../agency-balance/agency-balance.service';
import { NotificationLogsService } from '../notification-log/notification-log.service';
import { UserService } from '../user/user.service';
import { AdvertisingAccountService } from './advertising_account.service';
import { AdvertisingAccountFilterDto } from './dto/advertising_account-filter.dto';
import { CreateAdvertisingAccountDto } from './dto/create-advertising_account.dto';
import { UpdateAdvertisingAccountDto, UpdateIdAccountDto } from './dto/update-advertising_account.dto';
import { CONFIGURATION_CODE_CONSTANT, NotificationType, NotificationUserTitle } from 'src/constants/common.constant';
import { ConfigurationService } from '../configuration/configuration.service';
import { AccountBalanceTransactionService } from '../account-balance-transaction/account-balance-transaction.service';

@ApiTags('Advertising Account')
@Controller('advertising-account')
export class AdvertisingAccountController {
  constructor(
    private readonly advertisingAccountService: AdvertisingAccountService,
    private readonly advertisingAccountRequestService: AdvertisingAccountRequestService,
    private readonly userService: UserService,
    private readonly notificationService: NotificationLogsService,
    private readonly nodeMailer: NodeMailerService,
    private readonly agencyBalanceService: AgencyBalanceService,
    private readonly agencyBalanceLogsService: AgencyBalanceLogsService,
    private readonly configurationService: ConfigurationService,
    private readonly accountBalanceTransactionService: AccountBalanceTransactionService,
  ) {}

  @ApiBearerAuth()
  @Roles(UserRole.ADMIN, UserRole.CUSTOMER, UserRole.EMPLOYEE)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Get()
  async findAll(@UserDecorator() user: IUserJwt, @Query() options: AdvertisingAccountFilterDto) {
    let whereInput: Prisma.AdvertisingAccountFindManyArgs;

    const sortField = options?.sortField || 'createdAt';
    const sortOrder = options?.sortOrder || SortOrder.DESC;
    let where: Prisma.AdvertisingAccountWhereInput = {
      AND: [],
    };
    if (options.all) {
      where.AND = [
        {
          NOT: {
            status: {
              in: [AdvertisingAccountStatus.DELETED, AdvertisingAccountStatus.PENDING, AdvertisingAccountStatus.REFUNDED],
            },
          },
        },
      ];
    } else {
      if (options.status) {
        // @ts-ignore
        where.AND.push({
          status: { in: options.status },
        });
      }
    }

    if (options.textSearch) {
      // @ts-ignore
      where.AND.push({
        OR: [
          // { user: { lastName: { contains: options.textSearch } } },
          // { user: { firstName: { contains: options.textSearch } } },
          // { user: { email: { contains: options.textSearch } } },
          { title: { contains: options.textSearch } },
          { idAccount: { contains: options.textSearch } },
        ],
      });
    }

    if (user.role === UserRole.CUSTOMER) {
      where = {
        ...where,
        userId: user.data.id,
      };
    }

    if (options.agencyService) {
      // @ts-ignore
      where.AND.push({
        agencyService: { in: options.agencyService },
      });
    }

    if (options.type) {
      where = {
        ...where,
        type: options.type,
      };
    }

    if (options.userId) {
      where = {
        ...where,
        userId: options.userId,
      };
    }

    whereInput = {
      where,
      orderBy: {
        [sortField]: sortOrder,
      },
      include: { user: true, advertisingAccountDomain: true, advertisingAccountPage: true, BMShareRequest: true },
    };

    if (options?.all) {
      const items = await this.advertisingAccountService.findAll(whereInput);
      return {
        items,
      };
    }

    return await funcListPaging(this.advertisingAccountService, whereInput, options?.page, options?.perPage);
  }

  @ApiBearerAuth()
  @Roles(UserRole.ADMIN, UserRole.EMPLOYEE)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Get(':id')
  async findOne(@Param('id') id: number) {
    return this.advertisingAccountService.findOne({
      where: { id },
      include: { user: true, advertisingAccountDomain: true, advertisingAccountPage: true, BMShareRequest: true },
    });
  }

  @ApiBearerAuth()
  @Roles(UserRole.ADMIN, UserRole.EMPLOYEE)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Post(':requestId')
  async create(@Param('requestId') requestId: number, @UserDecorator() user: IUserJwt, @Body() body: Array<CreateAdvertisingAccountDto>) {
    const request = await this.advertisingAccountRequestService.findOne({ where: { id: requestId, status: AdvertisingAccountRequestStatus.PENDING } });
    if (!request) throw new BaseException(Errors.BAD_REQUEST('Request not found'));

    const userFound = await this.userService.findOne({
      where: { id: request.userId, status: { not: UserStatus.DELETED } },
    });
    if (!userFound) throw new BaseException(Errors.BAD_REQUEST('User not found'));

    if (userFound.status === UserStatus.BANNED) throw new BaseException(Errors.BAD_REQUEST('User has been banned'));
    let _createInputs: Prisma.AdvertisingAccountUncheckedCreateInput = {
      userId: userFound.id,
      updatedById: user.data.id,
    };
    const agencyService = body[0].agencyService;
    const type = body[0].type;
    let configType = CONFIGURATION_CODE_CONSTANT.CREATE_ACCOUNT_ADS_FEE;
    if (type === AdvertisingAccountType.GOOGLE) {
      configType = CONFIGURATION_CODE_CONSTANT.CREATE_ACCOUNT_ADS_FEE_GOOGLE;
    }
    const configuration = await this.configurationService.findOne({
      where: { settingCode: configType },
    });
    if (!configuration) throw new BaseException(Errors.BAD_REQUEST(`Configuration fee for ${type} does not existed`));
    const totalAmount = body.reduce((preValue, curValue) => preValue + curValue.balance, 0);

    const agencyWallet = await this.agencyBalanceService.findOne({ where: { type: agencyService } });
    if (!agencyWallet) throw new BaseException(Errors.BAD_REQUEST(`Agency wallet ${agencyService} not founded`));

    if (agencyWallet.balance < totalAmount) throw new BaseException(Errors.BAD_REQUEST(`The ${agencyService} wallet does not have sufficient balance`));
    for (let index = 0; index < body.length; index++) {
      const x = body[index];
      if (x.page && x.page.length > 0) {
        _createInputs = {
          ..._createInputs,
          advertisingAccountPage: {
            createMany: {
              data: x.page.map((x) => {
                return { linkPage: x.linkPage, namePage: x.namePage };
              }),
            },
          },
        };
      }

      if (x.domain && x.domain.length > 0) {
        _createInputs = {
          ..._createInputs,
          advertisingAccountDomain: {
            createMany: {
              data: x.domain.map((x) => {
                return { domain: x };
              }),
            },
          },
        };
      }
      delete x.page;
      delete x.domain;
      _createInputs = {
        ..._createInputs,
        ...x,
        advertisingAccountRequestId: requestId,
        createdById: user.data.id,
        type: x.type,
        status: AdvertisingAccountStatus.PENDING,
      };
      const createdAccount = await this.advertisingAccountService.create({ data: _createInputs });
      /**
       * Create topup request for each account created
       */
      const accountBalanceTopup = await this.accountBalanceTransactionService.create({
        data: {
          type: AccountBalanceTransactionType.DEPOSIT,
          amount: request.balancePerAccount,
          totalCost: request.balancePerAccount + (request.balancePerAccount * userFound.depositAdsFee) / 100,
          advertisingAccountId: createdAccount.id,
          status: AccountBalanceTransactionStatus.PENDING_APPROVAL,
          userId: userFound.id,
          updatedById: user.data.id,
        },
      });
      const currentWalletBalance = await this.agencyBalanceService.findOne({ where: { type: agencyService } });

      await this.agencyBalanceLogsService.create({
        data: {
          amount: x.balance,
          type: AgencyBalanceLogsType.DECREASE,
          createdById: user.data.id,
          advertisingAccountId: createdAccount.id,
          agencyWalletBalanceBefore: currentWalletBalance.balance,
          agencyWalletBalanceAfter: currentWalletBalance.balance - x.balance,
        },
      });
      await this.agencyBalanceService.update(agencyWallet.id, { balance: { decrement: x.balance } });
      await this.advertisingAccountRequestService.update(request.id, {
        status: AdvertisingAccountRequestStatus.SUBMITTED,
        updatedBy: { connect: { id: user.data.id } },
      });
    }
    // user notification
    await this.notificationService.create({
      data: {
        title: NotificationUserTitle.NEW_ACCOUNT_REQUEST_SUBMITTED,
        advertisingAccountRequestId: request.id,
        type: NotificationType.ACCOUNT_REQUEST,
        targetUserId: user.data.id,
      },
    });
    const accountBalanceRepo = await this.accountBalanceTransactionService.findOne({ where: { advertisingAccountRequestId: requestId } });
    if (accountBalanceRepo) {
      await this.accountBalanceTransactionService.update(accountBalanceRepo.id, { updatedById: user.data.id, status: AccountBalanceTransactionStatus.ADDED });
    }

    return { message: 'CREATE SUCCESSFUL' };
  }

  @ApiBearerAuth()
  @Roles(UserRole.ADMIN, UserRole.CUSTOMER, UserRole.EMPLOYEE)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Patch(':id')
  async update(@Param('id') id: number, @UserDecorator() user: IUserJwt, @Body() body: UpdateAdvertisingAccountDto) {
    let updateArgs: Prisma.AdvertisingAccountUncheckedUpdateInput = {
      ...body,
    };
    if (user.role === UserRole.ADMIN || user.role === UserRole.EMPLOYEE) {
      updateArgs = {
        ...updateArgs,
        updatedById: user.data.id,
        status: body.status,
      };
    }

    return this.advertisingAccountService.update(id, updateArgs);
  }

  @ApiBearerAuth()
  @Roles(UserRole.ADMIN)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Delete(':id')
  async delete(@Param('id') id: number, @UserDecorator() user: IUserJwt) {
    return this.advertisingAccountService.update(id, {
      updatedBy: { connect: { id: user.data.id } },
      status: AdvertisingAccountStatus.DELETED,
    });
  }

  @ApiBearerAuth()
  @Roles(UserRole.ADMIN, UserRole.EMPLOYEE)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Post(':requestId/status')
  async updateStatus(
    @Param('requestId') requestId: number,
    @UserDecorator() user: IUserJwt,
    @Body() body: Prisma.AdvertisingAccountRequestUncheckedUpdateInput,
  ) {
    if (!(user.role === UserRole.ADMIN || user.role === UserRole.EMPLOYEE)) {
      throw new BaseException(Errors.FORBIDDEN('Forbidden resource'));
    }

    const updateArgs: Prisma.AdvertisingAccountRequestUncheckedUpdateInput = {
      status: body.status || AdvertisingAccountRequestStatus.ACCEPTED,
    };

    await this.advertisingAccountRequestService.update(requestId, updateArgs);
    if (updateArgs.status === AdvertisingAccountRequestStatus.ACCEPTED) {
      await this.advertisingAccountService.updateMany({ advertisingAccountRequestId: requestId }, { status: AdvertisingAccountStatus.ACTIVE });
      const requestFound = this.advertisingAccountRequestService.findOne({ where: { id: requestId }, include: { user: true } });
      // @ts-ignore
      const userEmail = requestFound.user.email;
      const createdAccount = await this.advertisingAccountService.findAll({ where: { advertisingAccountRequestId: requestId } });
      const accountToString = createdAccount.map((e) => e.title).join(', ');
      this.nodeMailer.sendEmail(
        [userEmail],
        `Account Creation Successful`,
        `The account creation request (id: ${requestId}) has been successfully created. Your new advertising account: <${accountToString}>`,
      );
    }
    return { message: 'UPDATED SUCCESSFUL' };
  }

  @ApiBearerAuth()
  @Roles(UserRole.ADMIN, UserRole.EMPLOYEE)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Get(':requestId/accounts')
  async getRequestedAccount(
    @Param('requestId') requestId: number,
    @UserDecorator() user: IUserJwt,
    // @Body() body: Prisma.AdvertisingAccountRequestUncheckedUpdateInput,
  ) {
    const requestInstance = await this.advertisingAccountRequestService.findOne({
      where: { id: requestId, status: AdvertisingAccountRequestStatus.SUBMITTED },
    });
    if (!requestInstance) throw new BaseException(Errors.BAD_REQUEST(`Request Id ${requestId} not founded`));
    const accountRequestedInstance = await this.advertisingAccountService.findAll({ where: { advertisingAccountRequestId: requestId } });
    return accountRequestedInstance;
  }

  @ApiBearerAuth()
  @Roles(UserRole.ADMIN, UserRole.EMPLOYEE)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Post(':requestId/update')
  async updateIdAccount(@Param('requestId') requestId: number, @UserDecorator() user: IUserJwt, @Body() body: Array<UpdateIdAccountDto>) {
    const requestInstance = await this.advertisingAccountRequestService.findOne({
      where: { id: requestId, status: AdvertisingAccountRequestStatus.SUBMITTED },
    });
    if (!requestInstance) throw new BaseException(Errors.BAD_REQUEST(`Request Id ${requestId} not founded`));

    const accountRequestedInstance = await this.getRequestedAccount(requestId, user);

    const ids = body.map((e) => e.id);
    const accountRequestedIds = accountRequestedInstance.map(({ id }) => id);
    const allIdsMatch = accountRequestedIds.every((id) => ids.includes(id));
    if (accountRequestedIds.length !== ids.length) throw new BaseException(Errors.BAD_REQUEST(`Your request is missing one or more of the specified accounts`));
    if (!allIdsMatch) throw new BaseException(Errors.BAD_REQUEST(`An ID does not match the requested Account ID ${requestId}`));

    const updatedAccount = [];
    for (let i = 0; i < body.length; i++) {
      const element = body[i];
      const updatedInstance = await this.advertisingAccountService.update(element.id, {
        idAccount: element.idAccount,
        status: AdvertisingAccountStatus.ACTIVE,
      });
      const pendingApprovalTopup = await this.accountBalanceTransactionService.findOne({
        where: {
          status: AccountBalanceTransactionStatus.PENDING_APPROVAL,
          advertisingAccountId: element.id,
          type: AccountBalanceTransactionType.DEPOSIT,
        },
      });

      if (pendingApprovalTopup) {
        await this.accountBalanceTransactionService.update(pendingApprovalTopup.id, {
          status: AccountBalanceTransactionStatus.ADDED,
        });
        await this.notificationService.create({
          data: {
            title: NotificationUserTitle.ACCOUNT_BALANCE_DEPOSIT_ACCEPTED,
            accountBalanceTransactionId: pendingApprovalTopup.id,
            type: NotificationType.ACCOUNT_BALANCE,
            targetUserId: requestInstance.userId,
          },
        });
      }
      updatedAccount.push(updatedInstance);
    }

    await this.advertisingAccountRequestService.update(requestInstance.id, { status: AdvertisingAccountRequestStatus.ACCEPTED });

    return updatedAccount;
  }
}
