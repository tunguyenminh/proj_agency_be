import { ApiProperty } from "@nestjs/swagger";
import { AdvertisingAccountStatus, AdvertisingAccountType, AgencyServiceType, UserStatus } from "@prisma/client";
import { IsNotEmpty, IsEmail, IsEnum, IsString, IsOptional, IsNumber, ValidateNested, IsArray } from "class-validator";
import { TimezoneEnum } from "src/constants/enum.constant";
import { AdvertisingAccountRequestPageDto } from "src/models/advertising-account-request/dto/create-advertising-account-request";



export class CreateAdvertisingAccountDto {
    @ApiProperty({
        example: 'Edufit@gmail.com',
        required: true,
    })
    @IsNotEmpty()
    @IsArray()
    domain?: string[];

    @ApiProperty({
        example: 1,
        required: true,
    })
    @IsNotEmpty()
    @IsNumber()
    readonly balance?: number;

    @ValidateNested()
    @ApiProperty({
        example: [
            {
                namePage: '1',
                linkPage: '2',
            },
            {
                namePage: '1',

                linkPage: '2',
            },
            {
                namePage: '1',

                linkPage: '2',
            },
        ],
        required: true,
    })
    @IsArray()
    @IsOptional()
    page?: Array<AdvertisingAccountRequestPageDto>;

    @ApiProperty({
        example: 1,
        required: true,
    })
    @IsNotEmpty()
    @IsString()
    readonly title?: string;

    @ApiProperty({
        example: 'Edufit@gmail.com',
        required: true,
    })
    @IsNotEmpty()
    @IsEnum(AdvertisingAccountType)
    readonly type?: AdvertisingAccountType;

    @ApiProperty({
        example: AdvertisingAccountStatus.ACTIVE,
        required: true,
    })
    @IsOptional()
    @IsEnum(AdvertisingAccountStatus)
    readonly status?: AdvertisingAccountStatus;

    @ApiProperty({
        example: 1,
        required: true,
    })
    @IsEnum(TimezoneEnum)
    readonly timezone: TimezoneEnum;

    @ApiProperty({
        example: 'abcxyz',
        required: true,
    })
    @IsNotEmpty()
    @IsString()
    idAccount: string;

    @ApiProperty({
        example: AgencyServiceType.VN,
        required: true,
    })
    @IsNotEmpty()
    @IsEnum(AgencyServiceType)
    agencyService: AgencyServiceType;
}
