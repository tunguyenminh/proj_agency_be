import { ApiProperty } from "@nestjs/swagger";
import { AdvertisingAccountStatus, AdvertisingAccountType, AgencyServiceType, UserStatus } from "@prisma/client";
import { Transform, Type } from "class-transformer";
import { IsNotEmpty, IsEmail, IsEnum, IsString, IsOptional, IsNumber } from "class-validator";
import { FilterOptions } from "src/base/filterOption.dto";
import { AllEnum } from "src/constants/enum.constant";

export class AdvertisingAccountFilterDto extends FilterOptions {
    @ApiProperty({
        example: 'Edufit@gmail.com',
        required: false,
    })
    @IsOptional()
    @IsEnum(AdvertisingAccountType)
    readonly type?: AdvertisingAccountType;

    @ApiProperty({
        example: [AdvertisingAccountStatus.ACTIVE],
        required: false,
    })
    @IsOptional()
    @IsString({each: true})
    @Transform(({ value }) => value ? value.split(','): null)
    // @IsEnum(AdvertisingAccountStatus)
    readonly status?: AdvertisingAccountStatus[];

    @Type(() => Number)
    @ApiProperty({
        example: 1,
        required: false,
    })
    @IsOptional()
    @IsNumber()
    readonly userId?: number;

    @ApiProperty({
        example: AllEnum.ALL,
        required: false,
    })
    @IsOptional()
    @IsEnum(AllEnum)
    readonly all?: AllEnum


    @ApiProperty({
        example: [AgencyServiceType.HONGKONG,AgencyServiceType.VN,AgencyServiceType.UNI],
        required: false,
    })
    @IsOptional()
    @IsString({each: true})
    @Transform(({ value }) => value ? value.split(','): null)
    // @IsEnum(AdvertisingAccountStatus)
    readonly agencyService?: AgencyServiceType[];
}
