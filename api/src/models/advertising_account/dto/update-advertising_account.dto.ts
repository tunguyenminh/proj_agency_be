import { ApiProperty, PartialType } from '@nestjs/swagger';
import { IsNumber, IsString } from 'class-validator';
import { CreateAdvertisingAccountDto } from './create-advertising_account.dto';

export class UpdateAdvertisingAccountDto extends PartialType(CreateAdvertisingAccountDto) {}

export class UpdateIdAccountDto {
  @ApiProperty({
    example: 1,
    required: false,
  })
  @IsNumber()
  readonly id?: number;

  @ApiProperty({
    example: "12345678",
    required: false,
  })
  @IsString()
  readonly idAccount?: string;
}
