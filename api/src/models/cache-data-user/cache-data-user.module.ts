import { Module } from '@nestjs/common';
import { CacheDataUserService } from './cache-data-user.service';
import { CacheDataUserController } from './cache-data-user.controller';
import { PrismaModule } from 'prisma/prisma.module';

@Module({
    controllers: [CacheDataUserController],
    providers: [CacheDataUserService],
    exports: [CacheDataUserService],
    imports: [
        PrismaModule,
    ],
})

export class CacheDataUserModule { }
