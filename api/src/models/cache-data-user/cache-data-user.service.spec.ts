import { Test, TestingModule } from '@nestjs/testing';
import { CacheDataUserService } from './cache-data-user.service';

describe('CacheDataUserService', () => {
  let service: CacheDataUserService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CacheDataUserService],
    }).compile();

    service = module.get<CacheDataUserService>(CacheDataUserService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
