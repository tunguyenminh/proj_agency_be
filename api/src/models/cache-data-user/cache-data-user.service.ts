import { Injectable } from '@nestjs/common';
import { Prisma } from '@prisma/client';
import { PrismaService } from 'prisma/prisma.service';

@Injectable()
export class CacheDataUserService {
  constructor(
    private readonly prismaService: PrismaService,
  ) { }
  create(args: Prisma.CacheDataUserCreateArgs) {
    return this.prismaService.cacheDataUser.create(args)
  }

  findAll(args: Prisma.CacheDataUserFindManyArgs) {
    return this.prismaService.cacheDataUser.findMany(args)
  }

  findOne(args: Prisma.CacheDataUserFindFirstArgs) {
    return this.prismaService.cacheDataUser.findFirst(args)
  }

  update(id: number, args: Prisma.CacheDataUserUpdateInput) {
    return this.prismaService.cacheDataUser.update({ where: { id }, data: args })
  }

  count(args: Prisma.CacheDataUserCountArgs) {
    return this.prismaService.cacheDataUser.count(args)
  }
  remove(args: Prisma.CacheDataUserDeleteArgs) {
    return this.prismaService.cacheDataUser.delete(args)

  }
}

