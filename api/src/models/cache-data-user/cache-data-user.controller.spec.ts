import { Test, TestingModule } from '@nestjs/testing';
import { CacheDataUserController } from './cache-data-user.controller';
import { CacheDataUserService } from './cache-data-user.service';

describe('CacheDataUserController', () => {
  let controller: CacheDataUserController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CacheDataUserController],
      providers: [CacheDataUserService],
    }).compile();

    controller = module.get<CacheDataUserController>(CacheDataUserController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
