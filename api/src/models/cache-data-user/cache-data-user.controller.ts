import { Controller } from '@nestjs/common';
import { CacheDataUserService } from './cache-data-user.service';

@Controller('cache-data-user')
export class CacheDataUserController {
  constructor(private readonly cacheDataUserService: CacheDataUserService) {}

//   @Post()
//   create(@Body() createCacheDataUserDto: CreateCacheDataUserDto) {
//     return this.cacheDataUserService.create(createCacheDataUserDto);
//   }

//   @Get()
//   findAll() {
//     return this.cacheDataUserService.findAll();
//   }

//   @Get(':id')
//   findOne(@Param('id') id: string) {
//     return this.cacheDataUserService.findOne(+id);
//   }

//   @Patch(':id')
//   update(@Param('id') id: string, @Body() updateCacheDataUserDto: UpdateCacheDataUserDto) {
//     return this.cacheDataUserService.update(+id, updateCacheDataUserDto);
//   }

//   @Delete(':id')
//   remove(@Param('id') id: string) {
//     return this.cacheDataUserService.remove(+id);
//   }
}
