import { PartialType } from '@nestjs/mapped-types';
import { CreateCacheDataUserDto } from './create-cache-data-user.dto';

export class UpdateCacheDataUserDto extends PartialType(CreateCacheDataUserDto) {}
