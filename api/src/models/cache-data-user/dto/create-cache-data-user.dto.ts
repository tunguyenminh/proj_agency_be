import { ApiProperty } from "@nestjs/swagger";
import { CacheType } from "@prisma/client";
import { Transform, Type } from "class-transformer";
import { IsEnum, IsNotEmpty, IsNumber, IsOptional, IsString } from "class-validator";
import moment from "moment";

export class CreateCacheDataUserDto {
    @ApiProperty({
        required: true,
    })
    @IsNotEmpty()
    @IsNumber()
    userId: number

    @ApiProperty({
        required: true,
    })
    @IsNotEmpty()
    @IsEnum(CacheType)
    cacheType: CacheType

    @ApiProperty({
        required: false,
    })
    @IsString()
    @IsOptional()
    readonly keyCacheLogin?: string;

    @ApiProperty({
        required: false,
    })
    @Transform(({ value }) => moment(value ?? null).toDate())
    @Type(() => Date)
    @IsOptional()
    readonly expiredAt?: Date;
}
