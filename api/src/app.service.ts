import { Injectable } from '@nestjs/common';
import { PrismaService } from 'prisma/prisma.service';

import {
  CONFIGURATION_CODE_CONSTANT,
  CONFIGURATION_TYPE,
} from './constants/common.constant';
import { AuthService } from './auth/auth.service';
import { UserRole } from '@prisma/client';

@Injectable()
export class AppService {
  constructor(
    private readonly prisma: PrismaService,
    private readonly authService: AuthService,
  ) {}

  async getHello() {
    return this.prisma.user.findFirstOrThrow({ where: { id: 1 } });
  }

  async onApplicationBootstrap() {
    const user = await this.prisma.user.findFirst({});
    if (!user) {
      await this.prisma.user.createMany({
        data: [
          {
            email: 'gdtagencyy@gmail.com',
            password: await this.authService.hashPassword('123456'),
            phone: '0352368898',
            role: UserRole.ADMIN,
            phoneCode: 'VN'
          },
          {
            email: 'admin01@gmail.com',
            password: await this.authService.hashPassword('123456'),
            phone: '0372732834',
            role: UserRole.ADMIN,
          },
          {
            email: 'user01@gmail.com',
            password: await this.authService.hashPassword('123456'),
            phone: '0372732832',
            role: UserRole.CUSTOMER,
          },
          {
            email: 'user02@gmail.com',
            password: await this.authService.hashPassword('123456'),
            phone: '0372732831',
            role: UserRole.CUSTOMER,
          },
          {
            email: 'admin02@gmail.com',
            password: await this.authService.hashPassword('123456'),
            phone: '0372732833',
            role: UserRole.ADMIN,
          },
        ],
      });
    }
    const configuration = await this.prisma.configuration.findFirst({});
    if (!configuration) {
      await this.prisma.configuration.createMany({
        data: [
          // note config
          {
            settingCode: CONFIGURATION_CODE_CONSTANT.CREATE_ACCOUNT_ADS_NOTE,
            settingValue: 'Please point your domains to IP @A 103.75.187.23',
            unit: '%',
          },
          // create ads account fee config
          {
            settingCode:
              CONFIGURATION_CODE_CONSTANT.CREATE_ACCOUNT_ADS_FEE_GOOGLE,
            settingValue: '10',
            unit: '$',
          },
          {
            settingCode: CONFIGURATION_CODE_CONSTANT.CREATE_ACCOUNT_ADS_FEE,
            settingValue: '10',
            unit: '$',
          },
          // banking and exchange fee
          {
            settingCode: CONFIGURATION_CODE_CONSTANT.BANK_PHILIPPINES,
            settingValue: '0023 0001 0155',
            settingFee: 57.5,
            type: CONFIGURATION_TYPE.RATIO,
            unit: 'peso',
          },
          {
            settingCode: CONFIGURATION_CODE_CONSTANT.VN_BANK,
            settingValue: 'VN_BANK',
            settingFee: 25900,
            type: CONFIGURATION_TYPE.RATIO,
            unit: 'vnđ',
          },
          {
            settingCode: CONFIGURATION_CODE_CONSTANT.MAIL_PAYONNER,
            settingValue: 'doihue2912@gmail.com',
            type: CONFIGURATION_TYPE.PERCENT,
            unit: '%',
          },
          {
            settingCode: CONFIGURATION_CODE_CONSTANT.MAIL_PING_PONG,
            settingValue: 'duykhanh1075@gmail.com',
            type: CONFIGURATION_TYPE.PERCENT,
            unit: '%',
          },
          {
            settingCode: CONFIGURATION_CODE_CONSTANT.UNION_BANK,
            settingValue: 'TALITHA CUMI ONLINE STORE',
            type: CONFIGURATION_TYPE.PERCENT,
            unit: '%',
          },

          {
            settingCode: CONFIGURATION_CODE_CONSTANT.USDT,
            settingValue: 'USDT',
            type: CONFIGURATION_TYPE.PERCENT,
            unit: '%',
          },
          {
            settingCode: CONFIGURATION_CODE_CONSTANT.WISE,
            settingValue: 'WISE',
            type: CONFIGURATION_TYPE.PERCENT,
            unit: '%',
          },
        ],
      });
    }

    const role = await this.prisma.role.findFirst({});
    if (!role) {
      await this.prisma.role.create({
        data: {
          roleName: 'Super Admin',
          Permission: {
            create: {
              permissionName: 'Access all admin api',
              feature: '',
            },
          },
        },
      });
      await this.prisma.role.create({
        data: {
          roleName: 'Employee',
          Permission: {
            create: {
              permissionName: 'Employee access',
              feature:
                'GET /advertising-account,GET advertising-account/:id,POST advertising-account/:requestId,PATCH advertising-account/:id,DELETE advertising-account/:id',
            },
          },
        },
      });
    }
  }
}
