export enum SortOrder {
  ASC = 'asc',
  DESC = 'desc',
}

export enum Platform {
  WEB = 'WEB',
  APP = 'APP',
}

export enum ResetPasswordMethod {
  PHONE = "PHONE",
  EMAIL = "EMAIL",
}

export enum LoginMethod {
  PHONE = "PHONE",
  EMAIL = "EMAIL",
}

export enum SocialConnectionType {
  GOOGLE = 'GOOGLE',
  FACEBOOK = 'FACEBOOK',
  APPLE = 'APPLE',
}

export enum UserRole {
  CUSTOMER = "CUSTOMER",
  ADMIN = "ADMIN"
}

export enum MediaType {
  IMAGE = "IMAGE",
  VIDEO = "VIDEO",
  PDF = "PDF",
}

export enum UserStatus {
  ACTIVE = "ACTIVE",
  BANNED = "BANNED",
  DELETED = "DELETED"
}

export enum Gender {
  MALE = "MALE",
  FEMALE = "FEMALE",
  OTHER = "OTHER"
}

export enum TimezoneEnum {
  GMT_MINUS_11 = 'GMT-11:00',
  GMT_MINUS_12 = 'GMT-12:00',
  GMT_MINUS_10 = 'GMT-10:00',
  GMT_MINUS_9 = 'GMT-09:00',
  GMT_MINUS_8 = 'GMT-08:00',
  GMT_MINUS_7 = 'GMT-07:00',
  GMT_MINUS_6 = 'GMT-06:00',
  GMT_MINUS_5 = 'GMT-05:00',
  GMT_MINUS_4 = 'GMT-04:00',
  GMT_MINUS_3 = 'GMT-03:00',
  GMT_MINUS_2 = 'GMT-02:00',
  GMT_MINUS_1 = 'GMT-01:00',
  GMT_0 = 'GMT+00:00',
  GMT_PLUS_1 = 'GMT+01:00',
  GMT_PLUS_2 = 'GMT+02:00',
  GMT_PLUS_3 = 'GMT+03:00',
  GMT_PLUS_4 = 'GMT+04:00',
  GMT_PLUS_5 = 'GMT+05:00',
  GMT_PLUS_6 = 'GMT+06:00',
  GMT_PLUS_7 = 'GMT+07:00',
  GMT_PLUS_8 = 'GMT+08:00',
  GMT_PLUS_9 = 'GMT+09:00',
  GMT_PLUS_10 = 'GMT+10:00',
  GMT_PLUS_11 = 'GMT+11:00',
  GMT_PLUS_12 = 'GMT+12:00'
};

export enum NotificationType {
  DEPOSIT = 'DEPOSIT',
  WITHDRAWAL = 'WITHDRAWAL',
  ADVERTISING_REQUEST = 'ADVERTISING_REQUEST',
}

export enum WithdrawTarget {
  BANK = "BANK",
  WALLET = "WALLET"
}

export enum AccountTypeFilterEnum {
  FACEBOOK = 'FACEBOOK',
  GOOGLE = 'GOOGLE',
  INSTAGRAM = 'INSTAGRAM',
  TIKTOK = 'TIKTOK',
}

export enum AllEnum {
  ALL = 'ALL',
  PAGING = 'PAGING'
}

export enum Language {
  VI = 'VI',
  EN = 'EN',
}