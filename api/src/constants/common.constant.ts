export const COMMON_CONSTANT = {
  OTP_HEADERS: {
    CODE: 'otp-code',
    ACTION: 'otp-action',
  },
  REDIS_DEFAULT_NAMESPACE: 'redisDefault',
  NOTIFY_FORMAT_TIME: 'HH:mm - MMM DD, YYYY',

};

export enum CONFIGURATION_CODE_CONSTANT {
  CREATE_ACCOUNT_ADS_FEE = "CREATE_ACCOUNT_ADS_FEE",
  MAIL_PAYONNER = "MAIL_PAYONEER",
  MAIL_PING_PONG = "MAIL_PING_PONG",
  BANK_PHILIPPINES = "BANK_PHILIPPINES",
  UNION_BANK = "UNION_BANK",
  VN_BANK = "VN_BANK",
  USDT = "USDT",
  CREATE_ACCOUNT_ADS_NOTE = "CREATE_ACCOUNT_ADS_NOTE",
  WISE = "WISE",
  CREATE_ACCOUNT_ADS_FEE_GOOGLE = "CREATE_ACCOUNT_ADS_FEE_GOOGLE"
};

export enum CONFIGURATION_TYPE {
  PERCENT = 'PERCENT',
  RATIO = 'RATIO'
}

export enum MoneyTransactionMethod {
  MAIL_PAYONNER = "MAIL_PAYONEER",
  MAIL_PING_PONG = "MAIL_PING_PONG",
  BANK_PHILIPPINES = "BANK_PHILIPPINES",
  UNION_BANK = "UNION_BANK",
  VN_BANK = "VN_BANK",
  USDT = "USDT",
  WISE = "WISE"
}

export enum NotificationType {
  ACCOUNT_REQUEST = "ACCOUNT_REQUEST",
  MONEY_TRANSACTION = "MONEY_TRANSACTION",
  ACCOUNT_BALANCE = "ACCOUNT_BALANCE",
  BM_SHARE = "BM_SHARE",
  AGENCY_WALLET = "AGENCY_WALLET",
  AGENCY_WALLET_BALANCE_CHANGED = "AGENCY_WALLET_BALANCE_CHANGED",

}

export enum NotificationTitle {
  NEW_ACCOUNT_REQUEST = "You have new create account request",
  MONEY_TRANSACTION = "You have new money transaction request",
  ACCOUNT_BALANCE = "You have new account balance request",
  BM_SHARE = "You have new bm share request",
  NEW_AGENCY_WALLET = "You created new Agency Wallet",
  CHANGE_AGENCY_WALLET = "The balance of an agency's wallet has changed.",

}

export enum NotificationUserTitle {
  NEW_ACCOUNT_REQUEST_ACCEPTED = "Your create account request has been accepted",
  NEW_ACCOUNT_REQUEST_SUBMITTED = "Your create account request has been submitted",
  NEW_ACCOUNT_REQUEST_REJECTED = "Your create account request has been rejected",

  MONEY_TRANSACTION_WITHDRAWAL_ACCEPTED = "Your money transaction withdrawal request has been accepted",
  MONEY_TRANSACTION_WITHDRAWAL_REJECTED = "Your money transaction withdrawal request has been rejected",
  MONEY_TRANSACTION_DEPOSIT_ACCEPTED = "Your money transaction deposit request has been accepted",
  MONEY_TRANSACTION_DEPOSIT_REJECTED = "Your money transaction deposit request has been rejected",

  BM_SHARE_REJECTED = "Your BM Share request has been rejected",
  BM_SHARE_ACCEPTED = "Your BM Share request has been accepted",

  ACCOUNT_BALANCE_WITHDRAWAL_ACCEPTED = "Your account balance withdrawal request has been accepted",
  ACCOUNT_BALANCE_WITHDRAWAL_REJECTED = "Your account balance withdrawal request has been rejected",
  ACCOUNT_BALANCE_DEPOSIT_ACCEPTED = "Your account balance deposit request has been accepted",
  ACCOUNT_BALANCE_DEPOSIT_REJECTED = "Your account balance deposit request has been rejected",

}

export enum FORMAT_DATE {
  DATE_TIME = 'YYYY-MM-DD HH:mm:ss',
  DATE = 'YYYY-MM-DD',
  TIME = 'HH:mm:ss'
}

export enum ISO_WEEK_DAY {
  MONDAY = 1,
  TUESDAY = 2,
  WEDNESDAY = 3,
  THURSDAY = 4,
  FRIDAY = 5,
  SATURDAY = 6,
  SUNDAY = 7,
}

export enum Filter_Time {
  THIS_MONTH = 'THIS_MONTH',
  BY_WEEK = 'BY_WEEK',
  BY_MONTH = 'BY_MONTH',
  BY_QUARTER_YEAR = 'BY_QUARTER_YEAR',
}


export enum AdminDefaultActions {
  AUTH = "POST /auth/login",
  FORGOT_PASSWORD = "POST /auth/forgot-password",
  GET_PROFILE = "GET /auth"
}

export enum WalletFlowStatus {
  GOOGLE_DEPOSIT = 'GOOGLE_DEPOSIT',
  GOOGLE_WITHDRAWAL = 'GOOGLE_WITHDRAWAL',
  FACEBOOK_DEPOSIT = 'FACEBOOK_DEPOSIT',
  FACEBOOK_WITHDRAWAL = 'FACEBOOK_WITHDRAWAL',
  WALLET_DEPOSIT = 'WALLET_DEPOSIT',
  ACCOUNT_REQUEST = 'ACCOUNT_REQUEST',
  DENINED_ACCOUNT_REQUEST = 'DENINED_ACCOUNT_REQUEST'
}

export enum UNICode {
  SUCCESS = "0"
}

export enum UniMediaAccountApplicationStatus {
  UNDER_REVIEW = 100,
  AWAITING_MODIFICATION = 120,
  REJECTED = 130,
  COMPLETED = 150,
  FAILED = 160,
}
