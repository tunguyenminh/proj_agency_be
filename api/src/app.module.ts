import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { APP_FILTER, APP_INTERCEPTOR } from '@nestjs/core';
import { PrismaModule } from 'prisma/prisma.module';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AllExceptionsFilter } from './helpers/http-exception.filter';
import { LoggerMiddleware } from './helpers/logger.middleware';
import { TransformInterceptor } from './helpers/transform.interceptor';
import { CommonModule } from './common.module';
import { AuthModule } from './auth/auth.module';
import { CoreModule } from './core/core.module';
import { IPMiddleware } from './helpers/ip.middleware';
import { RedisModule } from '@liaoliaots/nestjs-redis';
import { BullModule } from '@nestjs/bull';
import { BackendConfigService } from './services/backend-config.service';
import { I18nModule, AcceptLanguageResolver, QueryResolver, HeaderResolver, I18nMiddleware } from 'nestjs-i18n';

import path from 'path';
import { I18nCustomModule } from './i18n/i18n.module';
import { AdvertisingAccountModule } from './models/advertising_account/advertising_account.module';
import { UserModule } from './models/user/user.module';
import { AdvertisingAccountRequestModule } from './models/advertising-account-request/advertising-account-request.module';
import { MoneyTransactionModule } from './models/money-transaction/money-transaction.module';
import { AccountBalanceTransactionModule } from './models/account-balance-transaction/account-balance-transaction.module';
import { BmShareRequestModule } from './models/bm-share-request/bm-share-request.module';
import { ConfigurationModule } from './models/configuration/configuration.module';
import { PermissionModule } from './models/permission/permission.module';
import { ScheduleModule } from '@nestjs/schedule';
import { CacheDataUserModule } from './models/cache-data-user/cache-data-user.module';
import { AgencyBalanceModule } from './models/agency-balance/agency-balance.module';
import { AgencyBalanceLogsModule } from './models/agency-balance-logs/agency-balance-logs.module';


@Module({
  imports: [
    PrismaModule,
    ScheduleModule.forRoot(),
    CommonModule,
    AuthModule,
    CoreModule,
    I18nCustomModule,
    I18nModule.forRootAsync({
      useFactory: (configService: BackendConfigService) => ({
        fallbackLanguage: configService.getEnv("FALLBACK_LANGUAGE"),
        loaderOptions: {
          path: path.join(__dirname, '../resources/locales'),
          watch: true,
        },
      }),
      resolvers: [
        { use: HeaderResolver, options: ['x-lang'] },
        AcceptLanguageResolver
      ],
      inject: [BackendConfigService],
    }),
    AdvertisingAccountModule,
    AdvertisingAccountRequestModule,
    MoneyTransactionModule,
    AccountBalanceTransactionModule,
    BmShareRequestModule,
    ConfigurationModule,
    PermissionModule,
    CacheDataUserModule,
    AgencyBalanceModule,
    AgencyBalanceLogsModule
  ],
  controllers: [AppController],
  providers: [
    AppService,
    {
      provide: APP_FILTER,
      useClass: AllExceptionsFilter,
    },
    {
      provide: APP_INTERCEPTOR,
      useClass: TransformInterceptor,
    },
  ],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer): void {
    consumer.apply(IPMiddleware).forRoutes('*');
    consumer.apply(LoggerMiddleware).forRoutes('*');
  }
}
