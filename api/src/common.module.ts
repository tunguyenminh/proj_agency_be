import { ConfigService } from '@nestjs/config';
import { BackendConfigService } from './services/backend-config.service';
import { Global, Module } from '@nestjs/common';
import { FirebaseService } from './services/firebase.service';
import { NotificationTemplateService } from './services/notification-template.service';
import { PrismaModule } from 'prisma/prisma.module';
import { UniService } from './services/uni/uni.service';
import { HKService } from './services/hongkong/hongkong.service';

const providers = [BackendConfigService, ConfigService, FirebaseService, NotificationTemplateService, UniService, HKService];

@Global()
@Module({
  providers,
  exports: [...providers],
  imports: [
    PrismaModule

  ],
})
export class CommonModule { }
