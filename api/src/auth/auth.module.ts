import { Module, forwardRef } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';

import { JwtStrategy } from './strategies/jwt.strategy';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import configurationCommon from 'src/common/configuration.common';
import { JwtAuthGuard } from './guards/jwt-auth.guard';
import { RolesGuard } from './guards/roles.guard';
import { PrismaModule } from '../../prisma/prisma.module';
import { UserModule } from 'src/models/user/user.module';
import { NodeMailerModule } from 'src/core/node-mailer/node-mailer.module';
import { OtpModule } from 'src/models/otp/otp.module';
import { PermissionModule } from 'src/models/permission/permission.module';
import { AdminRoleModule } from 'src/models/admin-role/admin-role.module';
import { CacheDataUserModule } from 'src/models/cache-data-user/cache-data-user.module';

@Module({
  imports: [
    PassportModule,
    JwtModule.register({
      secret: configurationCommon().jwt.secret,
      signOptions: { expiresIn: '30d' },
    }),
    PrismaModule,
    OtpModule,
    NodeMailerModule,
    forwardRef(() => UserModule),
    forwardRef(() => PermissionModule),
    forwardRef(() => AdminRoleModule),
    forwardRef(() => CacheDataUserModule),
  ],
  controllers: [AuthController],
  providers: [AuthService, RolesGuard, JwtAuthGuard, JwtStrategy],
  exports: [AuthService],
})
export class AuthModule { }
