import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { _excludeObject, excludeObject, getApiAndMethodFromRequest } from 'src/utils/common.utils';
import { BackendConfigService } from '../../services/backend-config.service';
import { IJwtPayload } from '../interfaces/jwt-payload.interface';
import { BaseException, Errors } from 'src/constants/error.constant';
import { UserStatus } from 'src/constants/enum.constant';
import { User, UserRole } from '@prisma/client';
import { UserService } from 'src/models/user/user.service';
import { PermissionService } from 'src/models/permission/permission.service';
import { AdminRoleService } from 'src/models/admin-role/admin-role.service';
import { AdminDefaultActions } from 'src/constants/common.constant';
// type UserWithoutPassword = Omit<User, 'password'>;
// interface IUser extends UserWithoutPassword {
//   Customer?: Customer[];
//   TourGuides?: TourGuide[];
//   Vendor?: Vendor[];
// }
export interface IUserJwt {
  data: User;
  role: UserRole;
}
@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(

    private readonly configService: BackendConfigService,
    private readonly userService: UserService,
    private readonly permissionService: PermissionService,
    private readonly adminRoleService: AdminRoleService,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: configService.getEnv('AUTH_JWT_ACCESS_SECRET'),
      passReqToCallback: true,
    });
  }

  async validate(req: Request, payload: IJwtPayload): Promise<IUserJwt> {
    let user: any;
    // const accessToken = req.headers['authorization'].replace('Bearer ', '');

    // if (accessToken !== user?.lastAccessToken) {
    //   throw new HttpException('Invalid Access Token', HttpStatus.UNAUTHORIZED);
    // }  
    user = await this.userService.findOne({ where: { id: payload.sub } })
    if (!user) throw new BaseException(Errors.ITEM_NOT_FOUND('Account'));

    if (user.status === UserStatus.BANNED) {
      throw new HttpException('User has been locked', HttpStatus.UNAUTHORIZED);
    }
    // let isAuthenticated = true
    // if (user.role === UserRole.ADMIN) {
    //   const api = getApiAndMethodFromRequest(req)
    //   isAuthenticated = Object.values(AdminDefaultActions).some((x) => api.includes(x))
    //   if (!isAuthenticated) {
    //     const role = await this.adminRoleService.findOne({
    //       where: {
    //         id: user.roleId,
    //       },
    //     });
    //     const permission = await this.permissionService.findAll({ where: { roleId: role.id } })
    //     permission.forEach((x) => {
    //       if (x.feature.split(',').includes(api)) {
    //         isAuthenticated = true;
    //       }
    //     });
    //   }
    //   if (!isAuthenticated)
    //     throw new BaseException(Errors.FORBIDDEN());
    // }

    const userExcludePassword = _excludeObject(user, ['password']);
    return { data: userExcludePassword, role: payload.role };
  }
}
