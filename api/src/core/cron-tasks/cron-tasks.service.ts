import { Injectable } from '@nestjs/common';
import { Cron, CronExpression, Timeout } from '@nestjs/schedule';
import { AccountBalanceTransactionStatus, AgencyServiceType, Prisma } from '@prisma/client';
import { PrismaService } from 'prisma/prisma.service';
import { UniMediaAccountApplicationStatus } from 'src/constants/common.constant';
import { UniService } from 'src/services/uni/uni.service';
import { chunkArray } from 'src/utils/common.utils';

@Injectable()
export class CronTasksService {
  constructor(private primsa: PrismaService, private uniService: UniService) {}
  UNIAccounts = [];
  async updateNotification() {
    await this.primsa.notificationLogs.updateMany({ where: {}, data: { isRead: false } });
  }

  async checkPageNumbers(params: { pageNumber: number }) {
    const UNIAccount = await this.uniService.getMediaAccount({ pageSize: 250, statuses: [2], pageNumber: params.pageNumber }, {});
    // if (UNIAccount?.data?.data?.pages !== UNIAccount?.data?.data?.pageNumber) {
    this.UNIAccounts = [...this.UNIAccounts, ...UNIAccount?.data?.data.mediaAccounts];
    if (params.pageNumber === UNIAccount?.data?.data?.pages) {
      // Perform your desired action here
      // For example, call another function or execute some code
      return;
    } else {
      // Call the function recursively with updated parameters
      // Assuming you have newParams defined somewhere
      await this.checkPageNumbers({ pageNumber: Number(UNIAccount?.data?.data?.pageNumber) + 1 });
    }
  }

  // @Cron(CronExpression.EVERY_5_MINUTES)
  // async handleEveryMinute() {
  //   /**
  //    * Update balance of Uni account each minute
  //    * Because only UNI provide API, so we only can update balance for UNI
  //    */
  //   this.UNIAccounts = [];
  //   // const params: IQueryMediaAccount = { mediaAccountIds: '' };
  //   // const UNIAccount = await this.uniService.getMediaAccount({pageSize: 250, statuses: [2]}, params);
  //   // Call the recursive function
  //   await this.checkPageNumbers({ pageNumber: 1 });
  //   console.log('Called EVERY_MINUTE', this.UNIAccounts);
  //   const mediaAccounts = this.UNIAccounts || [];
  //   for (let index = 0; index < mediaAccounts.length; index++) {
  //     const account = mediaAccounts[index];
  //     const abc = await this.primsa.advertisingAccount.findFirst({ where: { idAccount: account.mediaAccountId, agencyService: AgencyServiceType.UNI }});
  //     if (abc) {
  //       await this.primsa.advertisingAccount.update({ where: { id: abc.id }, data: { balance: Number(account.balance) } });
  //     }
  //   }
  // }

  @Cron(CronExpression.EVERY_5_MINUTES)
  async handleSyncTransaction() {
    const where: Prisma.AccountBalanceTransactionWhereInput = {
      status: AccountBalanceTransactionStatus.PENDING_APPROVAL,
      advertisingAccount: {
        agencyService: AgencyServiceType.UNI,
      },
    };
    const accountBalanceTransactionInstance = await this.primsa.accountBalanceTransaction.findMany({ where });
    const traceIds = accountBalanceTransactionInstance.map((e) => e.traceLv1Id);
    const chunkTraceId = [...chunkArray(traceIds, 50)];
    console.log(`Schedule run in ${CronExpression.EVERY_5_MINUTES}: ${chunkTraceId.length}`);
    
    /**
     * Processes management applications and updates related account transactions and balances.
     *
     * This function queries the management applications using `uniService.queryManageApplication` with the provided `taskIds`.
     * It processes each application, and if the application's status is `COMPLETED`, it performs the following actions:
     * 1. Finds the corresponding account balance transaction from the database using Prisma.
     * 2. Updates the status of the found account balance transaction to `ADDED`.
     * 3. Increments the balance of the related advertising account by the transaction amount.
     *
     * @param {Object} taskIds - The task IDs used for querying management applications.
     * @returns {Promise<void>} - A promise that resolves when all operations are complete.
     */
    const syncWithUNI = async (taskIds: Array<string> = []): Promise<void> => {
      const respone = await this.uniService.queryManageApplication({ taskIds }, {});
      const manageApplications = respone.data?.data?.manageApplications || [];
      for (let i = 0; i < manageApplications.length; i++) {
        const el = manageApplications[i];
        if (el.status === UniMediaAccountApplicationStatus.COMPLETED) {
          const tran = await this.primsa.accountBalanceTransaction.findFirst({ where: { traceLv1Id: el.taskId }, include: { advertisingAccount: true } });
          await this.primsa.accountBalanceTransaction.update({ where: { id: tran.id }, data: { status: AccountBalanceTransactionStatus.ADDED } });
          await this.primsa.advertisingAccount.update({
            where: { id: tran.advertisingAccount.id },
            data: { balance: { increment: tran.amount } },
          });
        }
      }
    };

    for (let i = 0; i < chunkTraceId.length; i++) {
      const element = chunkTraceId[i];
      await syncWithUNI(element);
    }
  }
}
