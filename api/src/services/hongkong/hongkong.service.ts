import { Injectable } from '@nestjs/common';
import axios from 'axios';
import { BackendConfigService } from '../backend-config.service';
import moment from 'moment';

export interface IAdHKDepoistSubmit {
  args: Arg[]
}

export interface Arg {
  list: List[]
  platform: number
}

export interface List {
  faceAdId: number | string
  money: number
}

@Injectable()
export class HKService {
  constructor(private readonly backendConfigService: BackendConfigService) {}

  HONGKONG_HOST = this.backendConfigService.getEnv('HONGKONG_HOST');
  HONGKONG_USERNAME = this.backendConfigService.getEnv('HONGKONG_USERNAME');
  HONGKONG_PASSWORD = this.backendConfigService.getEnv('HONGKONG_PASSWORD');
  HONGKONG_GRANT_TYPE = this.backendConfigService.getEnv('HONGKONG_GRANT_TYPE');
  HONGKONG_CLIENT_ID = this.backendConfigService.getEnv('HONGKONG_CLIENT_ID');
  HONGKONG_CLIENT_SECRET = this.backendConfigService.getEnv(
    'HONGKONG_CLIENT_SECRET',
  );
  HONGKONG_SCOPE = this.backendConfigService.getEnv('HONGKONG_SCOPE');

  TOKEN = '';
  TOKEN_EXPIRED = '';

  axiosInstance = axios.create({
    baseURL: this.HONGKONG_HOST,
    headers: {
      'Content-Type': 'application/json',
    },
  });

  async loginHK() {
    if (moment().isSameOrAfter(moment(this.TOKEN_EXPIRED)) || !this.TOKEN) {
      try {
        const requestData = {
          username: this.HONGKONG_USERNAME,
          password: this.HONGKONG_PASSWORD,
          grant_type: this.HONGKONG_GRANT_TYPE,
          client_id: this.HONGKONG_CLIENT_ID,
          client_secret: this.HONGKONG_CLIENT_SECRET,
          scope: this.HONGKONG_SCOPE,
        };
        // Convert request data to URL encoded format
        const params = new URLSearchParams();
        for (const key in requestData) {
          params.append(key, requestData[key]);
        }
        const responseLogin = await this.axiosInstance.post(
          'oauth/token',
          params,
          {
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded',
            },
          },
        );
        this.TOKEN = responseLogin.data.accessToken;
        this.TOKEN_EXPIRED = responseLogin.data.accessTokenExpiresAt;
        console.log(responseLogin.data);
        return responseLogin.data.accessToken;
      } catch (error) {
        return '';
      }
    } else {
      return this.TOKEN;
    }
  }

  async adDepositSubmit(body: IAdHKDepoistSubmit) {
    console.log('token', this.TOKEN, body);
    const loggedIn = await this.loginHK();
    if (!loggedIn) {
      return {data: {code: '400'}};
    }
    return this.axiosInstance.post('addeposit/submit', body, {
      headers: { Authorization: `Bearer ${this.TOKEN}` },
    });
  }
}
