import { Injectable } from '@nestjs/common';
import axios from 'axios';
import { BackendConfigService } from '../backend-config.service';

export interface IQueryManageApplication {
  data: IData
  code: string
  message: any
}

export interface IData {
  manageApplications: IManageApplication[]
  pageSize: number
  total: number
  pages: number
  pageNumber: number
}

export interface IManageApplication {
  taskNumber: string
  taskId: string
  type: number
  mediaAccountId: string
  mediaAccountName: string
  targetMediaAccountId: any
  targetMediaAccountName: any
  mediaPlatform: number
  status: number
  amount: string
  rechargeServiceFee: any
  deductServiceFee: any
  dailyBudget: number
  isMoveAllBalance: any
  currencyCode: any
  feedback: any
  value: any
  role: any
  createdTimestamp: number
  updatedTimestamp: number
}


export interface IRechargeApplication {
  taskNumber: string;
  mediaAccountId: string;
  mediaPlatform: number;
  amount: string;
  dailyBudget: number;
}

export interface IBodyManageApplication {
  taskNumber: string;
  mediaPlatform: number
  mediaAccountId: string
  value: string
  role: 10 | 20
}

export interface IQueryMediaAccount {
  mediaAccountIds?: string
}

@Injectable()
export class UniService {
  constructor(private readonly backendConfigService: BackendConfigService) {}
  UNI_HOST = this.backendConfigService.getEnv('UNI_HOST');
  UNI_AUTH = this.backendConfigService.getEnv('UNI_AUTH');
  axiosInstance = axios.create({
    baseURL: this.UNI_HOST,
    headers: {
      'Access-Token': this.UNI_AUTH,
      'Content-Type': 'application/json',
    },
  });

  rechargeApplication(body: IRechargeApplication) {
    return this.axiosInstance.post(
      'mediaAccount/rechargeApplication/create',
      body,
    );
  }
  manageApplication(body: IBodyManageApplication) {
    return this.axiosInstance.post(
      'mediaAccount/rechargeApplication/create',
      body,
    );
  }
  getMediaAccount(body: any, params: IQueryMediaAccount) {
    return this.axiosInstance.post(
      'mediaAccount/query',
      body,
      {params}
    );
  }
  queryManageApplication(body: any, params: any) {
    return this.axiosInstance.post<IQueryManageApplication>(
      'mediaAccount/manageApplication/query',
      body,
      {params}
    )
  }
}
