-- AlterTable
ALTER TABLE `agency_balance_logs` ADD COLUMN `agency_wallet_balance_After` DOUBLE NOT NULL DEFAULT 0,
    ADD COLUMN `agency_wallet_balance_Before` DOUBLE NOT NULL DEFAULT 0;
