-- AlterTable
ALTER TABLE `notification_logs` ADD COLUMN `bMShareRequestId` INTEGER NULL;

-- AddForeignKey
ALTER TABLE `notification_logs` ADD CONSTRAINT `notification_logs_bMShareRequestId_fkey` FOREIGN KEY (`bMShareRequestId`) REFERENCES `bm_share_request`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;
