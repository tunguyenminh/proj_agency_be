/*
  Warnings:

  - You are about to drop the column `deposit` on the `money_transaction_logs` table. All the data in the column will be lost.
  - You are about to drop the column `withdrawal` on the `money_transaction_logs` table. All the data in the column will be lost.
  - Added the required column `amount` to the `money_transaction_logs` table without a default value. This is not possible if the table is not empty.
  - Added the required column `userId` to the `money_transaction_logs` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE `account_balance_transaction` ADD COLUMN `recentlyBalance` DOUBLE NOT NULL DEFAULT 0;

-- AlterTable
ALTER TABLE `money_transaction_logs` DROP COLUMN `deposit`,
    DROP COLUMN `withdrawal`,
    ADD COLUMN `amount` DOUBLE NOT NULL,
    ADD COLUMN `image` VARCHAR(191) NULL,
    ADD COLUMN `note` VARCHAR(191) NULL,
    ADD COLUMN `status` ENUM('ACCEPTED', 'REJECTED', 'PENDING') NOT NULL DEFAULT 'PENDING',
    ADD COLUMN `type` ENUM('WITHDRAWAL', 'DEPOSIT') NOT NULL DEFAULT 'DEPOSIT',
    ADD COLUMN `userId` INTEGER NOT NULL;

-- CreateTable
CREATE TABLE `configuration` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `setting_code` VARCHAR(191) NOT NULL,
    `setting_value` VARCHAR(191) NOT NULL,
    `created_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    `updated_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- AddForeignKey
ALTER TABLE `money_transaction_logs` ADD CONSTRAINT `money_transaction_logs_userId_fkey` FOREIGN KEY (`userId`) REFERENCES `user`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;
