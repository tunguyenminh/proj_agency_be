/*
  Warnings:

  - You are about to alter the column `action` on the `otp` table. The data in that column could be lost. The data in that column will be cast from `VarChar(191)` to `Enum(EnumId(12))`.

*/
-- AlterTable
ALTER TABLE `otp` MODIFY `action` ENUM('LOGIN') NOT NULL;

-- AlterTable
ALTER TABLE `user` ADD COLUMN `lastAccessToken` VARCHAR(191) NULL;
