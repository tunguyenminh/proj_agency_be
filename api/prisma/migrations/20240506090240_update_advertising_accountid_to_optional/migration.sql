-- DropForeignKey
ALTER TABLE `account_balance_transaction` DROP FOREIGN KEY `account_balance_transaction_advertisingAccountId_fkey`;

-- AlterTable
ALTER TABLE `account_balance_transaction` MODIFY `advertisingAccountId` INTEGER NULL;

-- AddForeignKey
ALTER TABLE `account_balance_transaction` ADD CONSTRAINT `account_balance_transaction_advertisingAccountId_fkey` FOREIGN KEY (`advertisingAccountId`) REFERENCES `advertising_account`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;
