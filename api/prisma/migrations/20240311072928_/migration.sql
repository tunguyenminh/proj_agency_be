/*
  Warnings:

  - The values [ACCEPTED,REJECTED] on the enum `account_balance_transaction_status` will be removed. If these variants are still used in the database, this will fail.
  - The values [ACCEPTED,REJECTED] on the enum `money_transaction_logs_status` will be removed. If these variants are still used in the database, this will fail.

*/
-- AlterTable
ALTER TABLE `account_balance_transaction` MODIFY `status` ENUM('ADDED', 'CANCELED', 'PENDING') NOT NULL DEFAULT 'PENDING';

-- AlterTable
ALTER TABLE `configuration` ADD COLUMN `setting_fee` DOUBLE NOT NULL DEFAULT 0;

-- AlterTable
ALTER TABLE `money_transaction_logs` ADD COLUMN `withdrawalMethod` ENUM('BANK', 'ACCOUNT') NOT NULL DEFAULT 'ACCOUNT',
    MODIFY `status` ENUM('ADDED', 'CANCELED', 'PENDING') NOT NULL DEFAULT 'PENDING';

-- AlterTable
ALTER TABLE `user` ADD COLUMN `desposit_ads_fee` DOUBLE NOT NULL DEFAULT 0;
