-- AlterTable
ALTER TABLE `account_balance_transaction` ADD COLUMN `admin_confirm_image` VARCHAR(191) NULL,
    ADD COLUMN `method` VARCHAR(191) NULL,
    ADD COLUMN `method_detail` VARCHAR(191) NULL,
    ADD COLUMN `note` VARCHAR(191) NULL DEFAULT '',
    ADD COLUMN `recharge_wallet` BOOLEAN NOT NULL DEFAULT false,
    ADD COLUMN `total_paid` DOUBLE NOT NULL DEFAULT 0,
    ADD COLUMN `withdrawal_method` ENUM('BANK', 'ACCOUNT') NOT NULL DEFAULT 'ACCOUNT',
    MODIFY `type` ENUM('WITHDRAWAL', 'DEPOSIT', 'WALLET_DEPOSIT') NOT NULL;
