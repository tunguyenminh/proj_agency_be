/*
  Warnings:

  - You are about to drop the column `recentlyBalance` on the `account_balance_transaction` table. All the data in the column will be lost.
  - Added the required column `advertisingAccountId` to the `account_balance_transaction` table without a default value. This is not possible if the table is not empty.
  - Added the required column `type` to the `account_balance_transaction` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE `account_balance_transaction` DROP COLUMN `recentlyBalance`,
    ADD COLUMN `advertisingAccountId` INTEGER NOT NULL,
    ADD COLUMN `amount` DOUBLE NOT NULL DEFAULT 0,
    ADD COLUMN `status` ENUM('ACCEPTED', 'REJECTED', 'PENDING') NOT NULL DEFAULT 'PENDING',
    ADD COLUMN `type` ENUM('WITHDRAWAL', 'DEPOSIT') NOT NULL;

-- AlterTable
ALTER TABLE `advertising_account` ADD COLUMN `balance` DOUBLE NOT NULL DEFAULT 0;

-- AlterTable
ALTER TABLE `money_transaction_logs` ADD COLUMN `method` ENUM('BANK', 'PINGPONG', 'PAYONEER', 'USDT', 'PH_BANK', 'VN_BANK') NOT NULL DEFAULT 'BANK';

-- AddForeignKey
ALTER TABLE `account_balance_transaction` ADD CONSTRAINT `account_balance_transaction_advertisingAccountId_fkey` FOREIGN KEY (`advertisingAccountId`) REFERENCES `advertising_account`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;
