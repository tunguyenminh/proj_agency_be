-- AlterTable
ALTER TABLE `notification_logs` ADD COLUMN `accountBalanceTransactionId` INTEGER NULL,
    ADD COLUMN `targetAdminId` INTEGER NULL,
    ADD COLUMN `targetUserId` INTEGER NULL;

-- AddForeignKey
ALTER TABLE `notification_logs` ADD CONSTRAINT `notification_logs_accountBalanceTransactionId_fkey` FOREIGN KEY (`accountBalanceTransactionId`) REFERENCES `account_balance_transaction`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `notification_logs` ADD CONSTRAINT `notification_logs_targetAdminId_fkey` FOREIGN KEY (`targetAdminId`) REFERENCES `user`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `notification_logs` ADD CONSTRAINT `notification_logs_targetUserId_fkey` FOREIGN KEY (`targetUserId`) REFERENCES `user`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;
