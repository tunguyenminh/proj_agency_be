/*
  Warnings:

  - You are about to drop the column `balance` on the `advertising_account_request` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE `advertising_account_request` DROP COLUMN `balance`,
    ADD COLUMN `balance_per_account` DOUBLE NOT NULL DEFAULT 0,
    ADD COLUMN `created_fee` DOUBLE NOT NULL DEFAULT 0;
