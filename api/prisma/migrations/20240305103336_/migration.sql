-- AlterTable
ALTER TABLE `account_balance_transaction` ADD COLUMN `user_id` INTEGER NULL;

-- AddForeignKey
ALTER TABLE `account_balance_transaction` ADD CONSTRAINT `account_balance_transaction_user_id_fkey` FOREIGN KEY (`user_id`) REFERENCES `user`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;
