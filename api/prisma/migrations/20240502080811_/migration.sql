/*
  Warnings:

  - A unique constraint covering the columns `[type]` on the table `agency_balance` will be added. If there are existing duplicate values, this will fail.

*/
-- AlterTable
ALTER TABLE `agency_balance` MODIFY `name` VARCHAR(191) NULL;

-- AlterTable
ALTER TABLE `notification_logs` ADD COLUMN `agencyBalanceLogsId` INTEGER NULL;

-- CreateTable
CREATE TABLE `agency_balance_logs` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `type` ENUM('INCREASE', 'DECREASE') NOT NULL DEFAULT 'DECREASE',
    `amount` DOUBLE NOT NULL,
    `advertisingAccountId` INTEGER NULL,
    `created_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    `updated_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    `createdById` INTEGER NOT NULL,
    `updatedById` INTEGER NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateIndex
CREATE UNIQUE INDEX `agency_balance_type_key` ON `agency_balance`(`type`);

-- AddForeignKey
ALTER TABLE `notification_logs` ADD CONSTRAINT `notification_logs_agencyBalanceLogsId_fkey` FOREIGN KEY (`agencyBalanceLogsId`) REFERENCES `agency_balance_logs`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `agency_balance_logs` ADD CONSTRAINT `agency_balance_logs_advertisingAccountId_fkey` FOREIGN KEY (`advertisingAccountId`) REFERENCES `advertising_account`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `agency_balance_logs` ADD CONSTRAINT `agency_balance_logs_createdById_fkey` FOREIGN KEY (`createdById`) REFERENCES `user`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `agency_balance_logs` ADD CONSTRAINT `agency_balance_logs_updatedById_fkey` FOREIGN KEY (`updatedById`) REFERENCES `user`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;
