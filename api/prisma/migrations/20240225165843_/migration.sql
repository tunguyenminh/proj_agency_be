-- AlterTable
ALTER TABLE `advertising_account_request` MODIFY `status` ENUM('ACCEPTED', 'PENDING', 'DELETED', 'REJECTED') NOT NULL DEFAULT 'PENDING';
