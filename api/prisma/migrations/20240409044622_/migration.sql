-- AlterTable
ALTER TABLE `advertising_account_request` MODIFY `status` ENUM('ACCEPTED', 'PENDING', 'DELETED', 'REJECTED', 'SUBMITTED') NOT NULL DEFAULT 'PENDING';
