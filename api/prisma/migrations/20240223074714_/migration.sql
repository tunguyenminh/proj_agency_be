-- DropForeignKey
ALTER TABLE `account_balance_transaction` DROP FOREIGN KEY `account_balance_transaction_advertisingAccountRequestId_fkey`;

-- AlterTable
ALTER TABLE `account_balance_transaction` MODIFY `advertisingAccountRequestId` INTEGER NULL;

-- AddForeignKey
ALTER TABLE `account_balance_transaction` ADD CONSTRAINT `account_balance_transaction_advertisingAccountRequestId_fkey` FOREIGN KEY (`advertisingAccountRequestId`) REFERENCES `advertising_account_request`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;
