-- AlterTable
ALTER TABLE `account_balance_transaction` MODIFY `type` ENUM('WITHDRAWAL', 'DEPOSIT', 'WALLET_DEPOSIT', 'ACCOUNT_REQUEST') NOT NULL;
