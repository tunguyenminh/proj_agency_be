/*
  Warnings:

  - You are about to drop the column `userId` on the `bm_share_request` table. All the data in the column will be lost.

*/
-- DropForeignKey
ALTER TABLE `bm_share_request` DROP FOREIGN KEY `bm_share_request_userId_fkey`;

-- AlterTable
ALTER TABLE `bm_share_request` DROP COLUMN `userId`,
    ADD COLUMN `note` VARCHAR(191) NULL,
    ADD COLUMN `updatedById` INTEGER NULL;

-- AddForeignKey
ALTER TABLE `bm_share_request` ADD CONSTRAINT `bm_share_request_updatedById_fkey` FOREIGN KEY (`updatedById`) REFERENCES `user`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;
