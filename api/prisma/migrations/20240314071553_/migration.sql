/*
  Warnings:

  - You are about to alter the column `method` on the `money_transaction_logs` table. The data in that column could be lost. The data in that column will be cast from `Enum(EnumId(11))` to `VarChar(191)`.

*/
-- AlterTable
ALTER TABLE `money_transaction_logs` ADD COLUMN `methodDetail` VARCHAR(191) NULL,
    MODIFY `method` VARCHAR(191) NULL;
