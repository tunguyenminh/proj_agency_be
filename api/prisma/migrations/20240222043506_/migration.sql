/*
  Warnings:

  - You are about to drop the column `domain` on the `advertising_account` table. All the data in the column will be lost.
  - You are about to drop the column `link_page` on the `advertising_account` table. All the data in the column will be lost.
  - You are about to drop the column `name_page` on the `advertising_account` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE `advertising_account` DROP COLUMN `domain`,
    DROP COLUMN `link_page`,
    DROP COLUMN `name_page`;

-- CreateTable
CREATE TABLE `AdvertisingAccountPage` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `name_page` VARCHAR(191) NULL,
    `link_page` VARCHAR(191) NULL,
    `created_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    `updated_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    `advertisingAccountId` INTEGER NOT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `AdvertisingAccountDomain` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `domain` VARCHAR(191) NULL,
    `created_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    `updated_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    `advertisingAccountId` INTEGER NOT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- AddForeignKey
ALTER TABLE `AdvertisingAccountPage` ADD CONSTRAINT `AdvertisingAccountPage_advertisingAccountId_fkey` FOREIGN KEY (`advertisingAccountId`) REFERENCES `advertising_account`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `AdvertisingAccountDomain` ADD CONSTRAINT `AdvertisingAccountDomain_advertisingAccountId_fkey` FOREIGN KEY (`advertisingAccountId`) REFERENCES `advertising_account`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;
