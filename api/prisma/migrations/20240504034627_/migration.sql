/*
  Warnings:

  - The values [HK] on the enum `agency_balance_type` will be removed. If these variants are still used in the database, this will fail.

*/
-- AlterTable
ALTER TABLE `advertising_account` MODIFY `agencyService` ENUM('UNI', 'HONGKONG', 'VN') NOT NULL DEFAULT 'VN';
