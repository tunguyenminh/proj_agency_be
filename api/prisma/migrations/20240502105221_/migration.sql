-- AlterTable
ALTER TABLE `notification_logs` ADD COLUMN `agencyBalanceId` INTEGER NULL;

-- AddForeignKey
ALTER TABLE `notification_logs` ADD CONSTRAINT `notification_logs_agencyBalanceId_fkey` FOREIGN KEY (`agencyBalanceId`) REFERENCES `agency_balance`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;
