-- AlterTable
ALTER TABLE `advertising_account` ADD COLUMN `advertisingAccountRequestId` INTEGER NULL,
    ADD COLUMN `title` VARCHAR(191) NULL;

-- AddForeignKey
ALTER TABLE `advertising_account` ADD CONSTRAINT `advertising_account_advertisingAccountRequestId_fkey` FOREIGN KEY (`advertisingAccountRequestId`) REFERENCES `advertising_account_request`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;
