/*
  Warnings:

  - You are about to drop the `MoneyTransactionLog` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropTable
DROP TABLE `MoneyTransactionLog`;

-- CreateTable
CREATE TABLE `money_transaction_logs` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `withdrawal` DOUBLE NOT NULL DEFAULT 0,
    `deposit` DOUBLE NOT NULL DEFAULT 0,
    `created_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    `updated_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `advertising_account_request` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `type` ENUM('TIKTOK', 'GOOGLE', 'FACEBOOK', 'INSTAGRAM') NOT NULL DEFAULT 'GOOGLE',
    `name_page` VARCHAR(191) NULL,
    `link_page` VARCHAR(191) NULL,
    `domain` VARCHAR(191) NULL,
    `number_of_account` INTEGER NOT NULL,
    `balance` DOUBLE NOT NULL DEFAULT 0,
    `user_id` INTEGER NOT NULL,
    `created_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    `updated_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- AddForeignKey
ALTER TABLE `advertising_account_request` ADD CONSTRAINT `advertising_account_request_user_id_fkey` FOREIGN KEY (`user_id`) REFERENCES `user`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;
