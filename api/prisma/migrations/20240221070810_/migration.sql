-- AlterTable
ALTER TABLE `advertising_account_request` ADD COLUMN `status` ENUM('ACCEPTED', 'PENDING', 'DELETED') NOT NULL DEFAULT 'PENDING';
