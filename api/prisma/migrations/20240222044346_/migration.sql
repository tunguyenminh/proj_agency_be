/*
  Warnings:

  - You are about to drop the column `domain` on the `advertising_account_request` table. All the data in the column will be lost.
  - You are about to drop the column `link_page` on the `advertising_account_request` table. All the data in the column will be lost.
  - You are about to drop the column `name_page` on the `advertising_account_request` table. All the data in the column will be lost.
  - You are about to drop the `AdvertisingAccountDomain` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `AdvertisingAccountPage` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropForeignKey
ALTER TABLE `AdvertisingAccountDomain` DROP FOREIGN KEY `AdvertisingAccountDomain_advertisingAccountId_fkey`;

-- DropForeignKey
ALTER TABLE `AdvertisingAccountPage` DROP FOREIGN KEY `AdvertisingAccountPage_advertisingAccountId_fkey`;

-- AlterTable
ALTER TABLE `advertising_account_request` DROP COLUMN `domain`,
    DROP COLUMN `link_page`,
    DROP COLUMN `name_page`;

-- DropTable
DROP TABLE `AdvertisingAccountDomain`;

-- DropTable
DROP TABLE `AdvertisingAccountPage`;

-- CreateTable
CREATE TABLE `advertising_account_page` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `name_page` VARCHAR(191) NULL,
    `link_page` VARCHAR(191) NULL,
    `created_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    `updated_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    `advertisingAccountId` INTEGER NOT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `advertising_account_domain` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `domain` VARCHAR(191) NULL,
    `created_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    `updated_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    `advertisingAccountId` INTEGER NOT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `advertising_account_request_page` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `name_page` VARCHAR(191) NULL,
    `link_page` VARCHAR(191) NULL,
    `created_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    `updated_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    `advertisingAccountRequestId` INTEGER NOT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `advertising_account_request_domain` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `domain` VARCHAR(191) NULL,
    `created_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    `updated_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    `advertisingAccountRequestId` INTEGER NOT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- AddForeignKey
ALTER TABLE `advertising_account_page` ADD CONSTRAINT `advertising_account_page_advertisingAccountId_fkey` FOREIGN KEY (`advertisingAccountId`) REFERENCES `advertising_account`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `advertising_account_domain` ADD CONSTRAINT `advertising_account_domain_advertisingAccountId_fkey` FOREIGN KEY (`advertisingAccountId`) REFERENCES `advertising_account`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `advertising_account_request_page` ADD CONSTRAINT `advertising_account_request_page_advertisingAccountRequestI_fkey` FOREIGN KEY (`advertisingAccountRequestId`) REFERENCES `advertising_account_request`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `advertising_account_request_domain` ADD CONSTRAINT `advertising_account_request_domain_advertisingAccountReques_fkey` FOREIGN KEY (`advertisingAccountRequestId`) REFERENCES `advertising_account_request`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;
