/*
  Warnings:

  - Added the required column `updatedById` to the `money_transaction_logs` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE `money_transaction_logs` ADD COLUMN `updatedById` INTEGER NOT NULL;

-- AddForeignKey
ALTER TABLE `money_transaction_logs` ADD CONSTRAINT `money_transaction_logs_updatedById_fkey` FOREIGN KEY (`updatedById`) REFERENCES `user`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;
