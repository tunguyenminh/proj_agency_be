-- AlterTable
ALTER TABLE `advertising_account` ADD COLUMN `timezone` VARCHAR(191) NULL;

-- AlterTable
ALTER TABLE `advertising_account_request` ADD COLUMN `timezone` VARCHAR(191) NULL;

-- AlterTable
ALTER TABLE `user` ADD COLUMN `balance` DOUBLE NOT NULL DEFAULT 0;

-- CreateTable
CREATE TABLE `account_balance_transaction` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `created_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    `updated_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    `advertisingAccountRequestId` INTEGER NOT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- AddForeignKey
ALTER TABLE `account_balance_transaction` ADD CONSTRAINT `account_balance_transaction_advertisingAccountRequestId_fkey` FOREIGN KEY (`advertisingAccountRequestId`) REFERENCES `advertising_account_request`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;
