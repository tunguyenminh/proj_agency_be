-- AlterTable
ALTER TABLE `account_balance_transaction` ADD COLUMN `updated_by_id` INTEGER NULL;

-- AlterTable
ALTER TABLE `advertising_account` MODIFY `status` ENUM('ACTIVE', 'PENDING', 'DEACTIVE', 'DELETED', 'REFUNDED') NOT NULL DEFAULT 'PENDING',
    MODIFY `updated_by_id` INTEGER NULL DEFAULT 1;

-- AlterTable
ALTER TABLE `advertising_account_request` ADD COLUMN `updated_by_id` INTEGER NULL;

-- AddForeignKey
ALTER TABLE `advertising_account_request` ADD CONSTRAINT `advertising_account_request_updated_by_id_fkey` FOREIGN KEY (`updated_by_id`) REFERENCES `user`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `account_balance_transaction` ADD CONSTRAINT `account_balance_transaction_updated_by_id_fkey` FOREIGN KEY (`updated_by_id`) REFERENCES `user`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;
