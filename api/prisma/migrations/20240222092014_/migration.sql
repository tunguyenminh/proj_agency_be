-- DropForeignKey
ALTER TABLE `money_transaction_logs` DROP FOREIGN KEY `money_transaction_logs_updatedById_fkey`;

-- AlterTable
ALTER TABLE `money_transaction_logs` MODIFY `updatedById` INTEGER NULL;

-- AlterTable
ALTER TABLE `notification_logs` ADD COLUMN `advertisingAccountRequestId` INTEGER NULL,
    ADD COLUMN `moneyTransactionLogId` INTEGER NULL;

-- AddForeignKey
ALTER TABLE `money_transaction_logs` ADD CONSTRAINT `money_transaction_logs_updatedById_fkey` FOREIGN KEY (`updatedById`) REFERENCES `user`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `notification_logs` ADD CONSTRAINT `notification_logs_moneyTransactionLogId_fkey` FOREIGN KEY (`moneyTransactionLogId`) REFERENCES `money_transaction_logs`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `notification_logs` ADD CONSTRAINT `notification_logs_advertisingAccountRequestId_fkey` FOREIGN KEY (`advertisingAccountRequestId`) REFERENCES `advertising_account_request`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;
